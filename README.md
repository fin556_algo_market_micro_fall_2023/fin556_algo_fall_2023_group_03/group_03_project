# Arbitragedy

## FIN 556, Fall 2023, Group 3

#### Sayar Ghosh Roy, Tingyin Ding, Blaine Hill

`{sayar3, tingyin4, blaineh2}@illinois.edu`

#### — Advised by Prof. David Lariviere

---

In this project, we primarily focus on developing a Venue Arbitrage Strategy considering two markets, namely `IEX` and `NASDAQ`. We study the effect of latency on the profitability and performance of our strategy. Within our strategy, we explicitly track our current exposure through a custom state machine, dynamically update our stop loss & take profit levels, and decide on specific actions (including the order execution strategy) based on the current market state and our current holdings. We also compare our Venue Arbitrage Strategy with two simple baseline strategies based on the Simple Moving Average (SMA) and Relative Strength Index (RSI). We showcase our analysis of intraday market data and present a clustering algorithm to aggregate tickers showing well-correlated intraday movements into appropriately sized clusters. Within a trading strategy, one could use signals (market events) from other within-cluster tickers to speculate about the movements of a primary ticker under consideration. We develop a customized test bench for evaluating and visualizing the performance of various predictive models including simple models such as Linear Regression, Ensemble-based models such as Random Forest Regressor, and Deep Neural Networks. Our test bench relies on a market event generator which in turn is used to create custom data point plus data batch generators. We show various examples of how to use this setup for various training paradigms including an online learning system and an event-driven training with an early stopping scheme. A notable example that we include in this report is predicting the future Best Bids and Best Offers for instruments along with confidence intervals around the prediction. Lastly, we analyze the performance of a multivariate Kalman Filter for predicting the next BBO within our event-driven prediction setup.

---

#### Our project report is available [here](https://gitlab.engr.illinois.edu/fin556_algo_market_micro_fall_2023/fin556_algo_fall_2023_group_03/group_03_project/-/blob/main/FinalReport.md?ref_type=heads).

---