# Pseudo-code for maintaining current position

class trade_state:
    
    def __init__(self):
        self.reset_state()

    def view_state(self):
        if self.state_type == 'reset':
            print('In Reset State')
        
        else:
            print('Instrument:', self.instrument)
            print('Size:', self.size)
            print('Side:', self.side)
            print('TP Level:', self.take_profit_level)
            print('SL Level:', self.curr_stop_loss_level)

        return

    def reset_state(self):
        self.state_type = 'reset'
        # state_type is either 'reset' or 'hold'
        self.instrument = None
        self.size = None
        self.side = None
        # Either 'long' or 'short'
        
        self.price_entered = None
        self.take_profit_level = None
        self.curr_stop_loss_level = None
        self.stop_loss_fraction = None

    def set_stop_loss(self, curr_instrument_price):        
        loss_level = curr_instrument_price - curr_instrument_price * self.stop_loss_fraction

        if self.curr_stop_loss_level is None:
            self.curr_stop_loss_level = loss_level
        
        self.curr_stop_loss_level = max(self.curr_stop_loss_level, loss_level)

    def is_at_profit(self, curr_instrument_price):
        return curr_instrument_price >= self.take_profit_level

    def get_into_pos(self, price, instrument = 'SPY', 
                     size = 100, side = 'long', profit = 0.01, 
                     stop_loss_fraction = 0.05):
        
        self.instrument = instrument
        self.size = size
        self.side = side

        self.price_entered = price 
        self.take_profit_level = price + profit

        self.stop_loss_fraction = stop_loss_fraction
        self.set_stop_loss(self.price_entered)
        self.state_type = 'hold'

        return

    def exit_position(self):
        self.reset_state()
