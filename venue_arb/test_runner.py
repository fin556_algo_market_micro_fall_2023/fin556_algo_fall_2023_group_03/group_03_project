import trade_state_multi as sm

BBAs = [[20, 21], [21, 22], [23, 24], [23, 24], [23, 24], [22, 24], [22, 23], [21, 22], [19, 21]]
m = sm.trade_state_multi()

def execute_action(m, action):
    if action[0] == 'BID':
        return [action[1], action[2], 1]
    
    if action[0] == 'ASK':
        return [action[1], action[2], -1]
    
    return [-1, 0, 0]
    
    # Marks that we got filled at that price and volume


trial_style = 'long'

for BBA in BBAs:
    print('Event: ', BBA)
    best_bid = BBA[0]
    best_ask = BBA[1]

    if m.size == 0:
        # Will include the (Is it possible to get into venue arbitrage?) condition here
        # Do not enter new positions towards the end of the trading day

        if trial_style == 'long':
            print(['BID', best_ask, 10])
            m.update_pos(best_ask, 10)
            m.process_event(best_bid, best_ask)
        else:
            print(['ASK', best_bid, 10])
            m.update_pos(best_bid, -10)
            m.process_event(best_bid, best_ask)

    else:
        # Already in a position
        m.process_event(best_bid, best_ask)
        action = m.get_action(best_bid, best_ask)
        
        if action[0] != '':
            print('Action: ', action)

            price, volume, direction = execute_action(m, action)

            if volume > 0:
                # if filled at some volume
                m.update_pos(price, volume * direction)
                m.process_event(best_bid, best_ask)
            
    m.view_state()
    print('...')

    # Call process_event() after every update_pos()
