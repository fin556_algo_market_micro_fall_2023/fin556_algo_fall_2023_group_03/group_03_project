# Pseudo-codes for functions required for maintaining current position during trading
# And for making decisions on when to send what kind of orders to which exchange

import pandas as pd
import trade_state_multi as trade_sm

class VenueArbStrategy():
    
    def reset_sm_state(self):
        self.sm = trade_sm()
        self.stop_trading_time = '15:00:00' # Timezone: Eastern Time

    def get_ET_time(self, timestamp):
        # Convert UTC datetime string to ET time
        utc_datetime = pd.to_datetime(timestamp, format = '%Y-%m-%d %H:%M:%S', utc = True)

        # Convert to Eastern Time (ET)
        et_datetime = utc_datetime.tz_convert('America/New_York')
        et_time = et_datetime.strftime('%H:%M:%S')
        
        return et_time

    def proc_order_update(self, fill_price, order_type, volume):
        # order_type is either 'ASK' or 'BID' (the type of order that was filled)
        
        if order_type == 'BID':
            direction = 1

        elif order_type == 'ASK':
            direction = -1

        self.sm.update_pos(fill_price, volume * direction)
        # Call self.sm.process_event(best_bid, best_ask) after every call to self.proc_order_update()
        
    def execute_action(self, action, IEX_info, NASDAQ_info):
        # Actions to exit from the current position (Either a Take Profit (TP) or a Stop Loss (SL))
        # Note: The choice of sending orders first to IEX and then to NASDAQ is arbitrary
        # IEX_info: [[Best Bid Price, Best Bid Volume], [Best Ask Price, Best Ask Volume]] on IEX
        # Similarly, NASDAQ_info: [[Best Bid Price, Best Bid Volume], [Best Ask Price, Best Ask Volume]] on NASDAQ
    
        price = action[1]
        size = action[2]
        side = action[0]

        if side == 'BID':
            if IEX_info[1][0] == price and NASDAQ_info[1][0] == price:
                # Send Order to IEX first, if volume remains, send another order to NASDAQ
                orders = [['BID', price, min(size, IEX_info[1][1]), 'IEX']]
                size -= min(size, IEX_info[1][1])
                if size > 0:
                    orders.append(['BID', price, min(size, NASDAQ_info[1][1]), 'NASDAQ'])

            elif IEX_info[1][0] == price:
                # Send Order to IEX
                orders = [['BID', price, min(size, IEX_info[1][1]), 'IEX']]

            elif NASDAQ_info[1][0] == price:
                # Send Order to NASDAQ
                orders = [['BID', price, min(size, NASDAQ_info[1][1]), 'NASDAQ']]

        elif side == 'ASK':
            if IEX_info[0][0] == price and NASDAQ_info[0][0] == price:
                # Send Order to IEX first, if volume remains, send another order to NASDAQ
                orders = [['ASK', price, min(size, IEX_info[0][1]), 'IEX']]
                size -= min(size, IEX_info[0][1])
                if size > 0:
                    orders.append(['ASK', price, min(size, NASDAQ_info[0][1]), 'NASDAQ'])

            elif IEX_info[0][0] == price:
                # Send Order to IEX
                orders = [['ASK', price, min(size, IEX_info[0][1]), 'IEX']]

            elif NASDAQ_info[0][0] == price:
                # Send Order to NASDAQ
                orders = [['ASK', price, min(size, NASDAQ_info[0][1]), 'NASDAQ']]
    
        # for order in orders:
        #   Send order to exchange
    
        return

    
    def check_trade_and_place_orders(self, best_bid, best_ask, IEX_info, NASDAQ_info, timestamp):
        # Checks for possible trades and create appropriate orders

        NASDAQ_width = NASDAQ_info[1][0] - NASDAQ_info[0][0]
        IEX_width = IEX_info[1][0] - IEX_info[0][0]
        one_tick = 0.01

        if self.get_ET_time(timestamp) < self.stop_trading_time:
            return False
    
        elif best_bid > best_ask and IEX_info[0][0] == best_bid and NASDAQ_info[1][0] == best_ask:
            size = min(IEX_info[0][1], NASDAQ_info[1][1])
            order_A = ['BID', best_ask, size, 'NASDAQ']
            order_B = ['ASK', best_bid, size, 'IEX']
            # Send order_A and order_B
            return True

        elif best_bid > best_ask and NASDAQ_info[0][0] == best_bid and IEX_info[1][0] == best_ask:
            size = min(IEX_info[1][1], NASDAQ_info[0][1])
            order_A = ['BID', best_ask, size, 'IEX']
            order_B = ['ASK', best_bid, size, 'NASDAQ']
            # Send order_A and order_B
            return True
    
        elif NASDAQ_width == one_tick and IEX_width == one_tick and NASDAQ_info[0][0] > IEX_info[0][0]:
            # Go Long on IEX
            order = ['BID', IEX_info[1][0], IEX_info[1][1], 'IEX']
            # Send order
            return True

        elif NASDAQ_width == one_tick and IEX_width == one_tick and NASDAQ_info[0][0] < IEX_info[0][0]:
            # Go Short on IEX
            order = ['ASK', IEX_info[0][0], IEX_info[0][1], 'IEX']
            # Send order
            return True
    
        else:
            return False

    
    def proc_event(self, IEX_info, NASDAQ_info, timestamp):
    # IEX_info: [[Best Bid Price, Best Bid Volume], [Best Ask Price, Best Ask Volume]] on IEX
    # Similarly, NASDAQ_info: [[Best Bid Price, Best Bid Volume], [Best Ask Price, Best Ask Volume]] on NASDAQ
  
        best_bid = max(IEX_info[0][0], NASDAQ_info[0][0])
        best_ask = min(IEX_info[1][0], NASDAQ_info[1][0])

        self.sm.process_event(best_bid, best_ask)

        if self.sm.size != 0:
            # Already in a position
            self.sm.process_event(best_bid, best_ask)
            action = self.sm.get_action(best_bid, best_ask)
        
            if action[0] != '':
                print('Action: ', action)
                self.execute_action(action, IEX_info, NASDAQ_info)
    
        # Should we get into a position based on our Strategy?
        self.check_trade_and_place_orders(best_bid, best_ask, IEX_info, NASDAQ_info, timestamp)
        return
