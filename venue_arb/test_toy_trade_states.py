import trade_states as sm

BBAs = [[20, 21], [21, 22], [23, 24], [23, 24], [23, 24], [22, 24], [22, 23], [21, 22], [19, 21]]

m = sm.trade_state()

trial_style = 'short'

for BBA in BBAs:
    print('Event: ', BBA)
    best_bid = BBA[0]
    best_ask = BBA[1]

    if m.state_type == 'reset':
        if trial_style == 'long':
            m.get_into_pos(best_ask)

        else:
            m.get_into_pos(best_bid)

    else:
        if trial_style == 'long':
            print('Is at profit?', m.is_at_profit(best_bid))
        else:
            print('Is at profit?', m.is_at_profit(best_ask))

        m.exit_position()

    m.view_state()
    print('...')

        