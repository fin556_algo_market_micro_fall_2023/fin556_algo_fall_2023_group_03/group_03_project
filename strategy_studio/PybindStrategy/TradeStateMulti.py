# Maintain current position tracking order fills both on the bid and the ask side

class trade_state_multi:
    
    def __init__(self, instrument = 'SPY', stop_loss_fraction = 0.05, per_unit_profit = 0.01):
        self.reset_state()
        self.instrument = instrument
        self.stop_loss_fraction = stop_loss_fraction
        self.per_unit_profit = per_unit_profit

    def view_state(self):
        print('Size:', self.size)
        print('Exposure:', self.exposure)
        print('Absolute Exposure per unit:', self.absolute_exposure_per_unit)
        
        direction = self.get_direction()

        if direction == 'long':
            print('Direction: Long')
            print('Take Profit Level:', self.absolute_exposure_per_unit + self.per_unit_profit)
            print('Stop Loss Level:', self.long_curr_stop_loss_level)

        elif direction == 'short':
            print('Direction: Short')
            print('Take Profit Level:', self.absolute_exposure_per_unit - self.per_unit_profit)
            print('Stop Loss Level:', self.short_curr_stop_loss_level)

        return

    def set_exposure_per_unit(self):
        if self.size == 0:
            self.exposure = 0
            self.absolute_exposure_per_unit = 0
            return
        
        self.absolute_exposure_per_unit = abs(self.exposure / self.size)
        return
    
    def reset_state(self):
        self.exposure = 0
        self.size = 0
        self.absolute_exposure_per_unit = 0

        self.reset_long_side()
        self.reset_short_side()
        return

    def reset_long_side(self):
        self.long_take_profit_level = None
        self.long_curr_stop_loss_level = None
        return

    def reset_short_side(self):
        self.short_take_profit_level = None
        self.short_curr_stop_loss_level = None
        return

    def get_direction(self):
        # Are we net long or net short?
        if self.size > 0:
            return 'long'
        elif self.size < 0:
            return 'short'
        else:
            return 'reset'

    def set_stop_loss(self, best_bid, best_ask):

        direction = self.get_direction()

        if direction == 'none':
            return

        elif direction == 'long':

            if self.long_curr_stop_loss_level is None:
                loss_level = self.absolute_exposure_per_unit - self.absolute_exposure_per_unit * self.stop_loss_fraction
                self.long_curr_stop_loss_level = loss_level
            
            else:
                loss_level = best_bid - best_bid * self.stop_loss_fraction
                self.long_curr_stop_loss_level = max(self.long_curr_stop_loss_level, loss_level)

        elif direction == 'short':

            if self.short_curr_stop_loss_level is None:
                loss_level = self.absolute_exposure_per_unit + self.absolute_exposure_per_unit * self.stop_loss_fraction
                self.short_curr_stop_loss_level = loss_level
            
            else:
                loss_level = best_ask + best_ask * self.stop_loss_fraction
                self.short_curr_stop_loss_level = min(self.short_curr_stop_loss_level, loss_level)

    def is_at_profit(self, best_bid, best_ask):

        direction = self.get_direction()

        if direction == 'none':
            return

        if direction == 'long':
            take_profit_level = self.absolute_exposure_per_unit + self.per_unit_profit
            return (best_bid >= take_profit_level)

        elif direction == 'short':
            take_profit_level = self.absolute_exposure_per_unit - self.per_unit_profit
            return (best_ask <= take_profit_level)
        
    def is_at_stop_loss(self, best_bid, best_ask):

        direction = self.get_direction()

        if direction == 'none':
            return

        elif direction == 'long':
            return (best_bid < self.long_curr_stop_loss_level)
            
        elif direction == 'short':
            return (best_ask > self.short_curr_stop_loss_level)

    def update_pos(self, price, size):
        # size is positive for filled on long side
        # size is negative for filled on short side

        self.exposure += (price * size)
        self.size += size

        self.set_exposure_per_unit()

        direction = self.get_direction()

        if direction == 'none':
            self.reset_state()

        elif direction == 'long':
            self.reset_short_side()
            
        elif direction == 'short':
            self.reset_long_side()
        
        return
    
    def process_event(self, best_bid, best_ask):
        self.set_stop_loss(best_bid, best_ask)

    def get_action(self, best_bid, best_ask):
        direction = self.get_direction()

        if direction == 'long':
            if self.is_at_stop_loss(best_bid, best_ask):
                return ['ASK', best_bid, abs(self.size), 'SL']
            
            if self.is_at_profit(best_bid, best_ask):
                return ['ASK', best_bid, abs(self.size), 'TP']
            
            else:
                return ['', '', '', '']

        elif direction == 'short':
            if self.is_at_stop_loss(best_bid, best_ask):
                return ['BID', best_ask, abs(self.size), 'SL']
            
            if self.is_at_profit(best_bid, best_ask):
                return ['BID', best_ask, abs(self.size), 'TP']
            
            else:
                return ['', '', '', '']
            