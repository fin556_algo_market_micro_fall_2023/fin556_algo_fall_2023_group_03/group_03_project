# #!/usr/bin/env python
# # coding: utf-8

from strategy import *
from BaseStrategy import BaseStrategy
# import pandas as pd
from TradeStateMulti import trade_state_multi as trade_sm

class MyStrategy(BaseStrategy):
    def __init__(self, strategy_actions):
        self.reset_sm_state()
        super().__init__(strategy_actions)

    def OnResetStrategyState(self):
        # print("OnResetStrategyState(): resetting strategy state", flush=True)
        self.reset_sm_state()
        super().__init__()
        
        
    # Set flags before the strategy is started
    def SetFlags(self):
        pass

    def OnTrade(self, msg):
        self.call_proc_event(msg)
        

    def OnTopQuote(self, msg):
        # print("OnTopQuote event timestamp", msg.source_time())
        # self.call_proc_event(msg)
        pass
        
    def OnQuote(self, msg): 
        # print("OnQuote event timestamp", msg.source_time())
        self.call_proc_event(msg)
        pass
        
        
    def OnBar(self, msg): 
        # print("OnBar event timestamp", msg.source_time())
        # self.call_proc_event(msg)
        pass


    def OnDepth(self, msg):
        # self.counter += 1
        # print("self.counter:", self.counter)
        # print("OnDepth event timestamp", msg.source_time(), flush=True)
        self.call_proc_event(msg)
        pass
    
    def OnOrderUpdate(self, msg): 
        # print("OnOrderUpdate event timestamp", msg.source_time(), flush=True)
        order = msg.order()
        self.proc_order_update(order)
        pass
        
    
    def OnNews(self, msg):
        # print("OnNews event timestamp", msg.source_time())
        # self.call_proc_event(msg)
        pass
  


    def RegisterForStrategyEvents(self, eventRegister, currDate):
        pass
    
    
    # helper functions
    
    def call_proc_event(self, msg):
        timestamp = msg.source_time()
        # print("call_proc_event(): timestamp", timestamp, flush=True)
        instrument = msg.instrument()
        markets = instrument.markets()
        iex_market_info = self.get_market_info(markets, MARKET_CENTER_ID_IEX)
        nasdaq_market_info = self.get_market_info(markets, MARKET_CENTER_ID_NASDAQ)
        if iex_market_info[0][0] != 0 and iex_market_info[1][0] != 0 and nasdaq_market_info[0][0] != 0 and nasdaq_market_info[1][0] != 0:
            self.proc_event(instrument, iex_market_info, nasdaq_market_info, timestamp)
        
    def getInfoAtBestBidLevel(self, markets, market_center_id):
        # find the size at the best bid level
        market = markets.market_center(market_center_id)
        book = market.order_book()
        book_level = book.BestBidLevel()
        size = book_level.size()
        price = book_level.price()
        return size, price
    
    def getInfoAtBestAskLevel(self, markets, market_center_id):
        # find the size and the price at the best ask level
        market = markets.market_center(market_center_id)
        book = market.order_book()
        book_level = book.BestAskLevel()
        size = book_level.size()
        price = book_level.price()
        return size, price

    
    def SendOrder(self, instrument, action, price, size, market_center): 
        # print("SendOrder(): action", action, "price", price, "size", size, "market_center", market_center, flush=True)
        market_center_id = MARKET_CENTER_ID_NASDAQ if market_center == "NASDAQ" else MARKET_CENTER_ID_IEX
        params = OrderParams(instrument, abs(size), price, market_center_id, (ORDER_SIDE_BUY if action == "BID" else ORDER_SIDE_SELL), ORDER_TIF_DAY, ORDER_TYPE_LIMIT)

        print("SendTradeOrder(): about to send new order to market" + str(market_center_id) + " for size " + str(size) + " at $" + str(price) + " to " + str(action) + " " + instrument.symbol(), flush=True)
        
        self.trade_actions().SendNewOrder(params)
        pass
    
    ## Venue Arbitrage Strategy

    def reset_sm_state(self):
        self.sm = trade_sm()
        self.stop_trading_time = '15:00:00' # Timezone: Eastern Time

    # def get_ET_time(self, timestamp):
    #     # Convert UTC datetime string to ET time
    #     utc_datetime = pd.to_datetime(timestamp, format = '%Y-%m-%d %H:%M:%S', utc = True)

    #     # Convert to Eastern Time (ET)
    #     et_datetime = utc_datetime.tz_convert('America/New_York')
    #     et_time = et_datetime.strftime('%H:%M:%S')

    #     return et_time

    def proc_order_update(self, order):
        # This number will be signed, negative for sales.
        size_completed = order.size_completed()
        # print("OnOrderUpdate(): size_completed", size_completed)
        avg_fill_price = order.avg_fill_price()
        # print("proc_order_update(): avg_fill_price", avg_fill_price, "size_completed", size_completed, flush=True)
        
        # print("OnOrderUpdate(): avg_fill_price", avg_fill_price)
        self.sm.update_pos(avg_fill_price, size_completed)

    def trade_condition(self, best_bid, best_ask, timestamp):
        diff = best_ask - best_bid
        if diff == 0:
        # if diff < 0.01 and self.get_ET_time(timestamp) < self.stop_trading_time:
            # print("trade_condition() True: best_bid", best_bid, "best_ask", best_ask, "timestamp", timestamp)
            return True
        else:
            # print("trade_condition() False: best_bid", best_bid, "best_ask", best_ask, "timestamp", timestamp)
            return False
        
    def execute_action(self, instrument, action, IEX_info, NASDAQ_info):
        # Actions to exit from current position (Either a Take Profit (TP) or a Stop Loss (SL))
        # Note: Choice of sending orders first to IEX and then to NASDAQ is arbitrary
        price = action[1]
        size = action[2]
        side = action[0]

        if side == 'BID':
            if IEX_info[1][0] == price and NASDAQ_info[1][0] == price:
                # Send Order to IEX first, if volume remains, send another order to NASDAQ
                orders = [['BID', price, min(size, IEX_info[1][1]), 'IEX']]
                size -= min(size, IEX_info[1][1])
                if size > 0:
                    orders.append(['BID', price, min(size, NASDAQ_info[1][1]), 'NASDAQ'])

            elif IEX_info[1][0] == price:
                # Send Order to IEX
                orders = [['BID', price, min(size, IEX_info[1][1]), 'IEX']]

            elif NASDAQ_info[1][0] == price:
                # Send Order to NASDAQ
                orders = [['BID', price, min(size, NASDAQ_info[1][1]), 'NASDAQ']]

        elif side == 'ASK':
            if IEX_info[0][0] == price and NASDAQ_info[0][0] == price:
                # Send Order to IEX first, if volume remains, send another order to NASDAQ
                orders = [['ASK', price, min(size, IEX_info[0][1]), 'IEX']]
                size -= min(size, IEX_info[0][1])
                if size > 0:
                    orders.append(['ASK', price, min(size, NASDAQ_info[0][1]), 'NASDAQ'])

            elif IEX_info[0][0] == price:
                # Send Order to IEX
                orders = [['ASK', price, min(size, IEX_info[0][1]), 'IEX']]

            elif NASDAQ_info[0][0] == price:
                # Send Order to NASDAQ
                orders = [['ASK', price, min(size, NASDAQ_info[0][1]), 'NASDAQ']]
        
        # for order in orders:
        #   Add: Send order to exchange
        for order in orders:
            self.SendOrder(instrument, order[0], order[1], order[2], order[3])
        
        return
    
    def get_market_info(self, markets, market_center_id):
        try:
            bid_size, bid_price = self.getInfoAtBestBidLevel(markets, market_center_id)
            ask_size, ask_price = self.getInfoAtBestAskLevel(markets, market_center_id)
            return [[bid_price, bid_size], [ask_price, ask_size]]
        except Exception as e:
            print("get_market_info(): Error in getting market info", e)
            return [[0, 0], [0, 0]]
        
    def check_trade_and_place_orders(self, instrument, best_bid, best_ask, IEX_info, NASDAQ_info, timestamp):
        # print("check_trade_and_place_orders(): best_bid", best_bid, "best_ask", best_ask, "timestamp", timestamp, flush=True)
        # Checks for possible trades
        # Create appropriate orders

        NASDAQ_width = NASDAQ_info[1][0] - NASDAQ_info[0][0]
        IEX_width = IEX_info[1][0] - IEX_info[0][0]
        one_tick = 0.01

        # if self.get_ET_time(timestamp) < self.stop_trading_time:
        #     return False
        
        if best_bid > best_ask and IEX_info[0][0] == best_bid and NASDAQ_info[1][0] == best_ask:
            print("check_trade_and_place_orders(): cond1",timestamp, flush=True)
            size = min(IEX_info[0][1], NASDAQ_info[1][1])
            order_A = ['BID', best_ask, size, 'NASDAQ']
            order_B = ['ASK', best_bid, size, 'IEX']

            # Add: Send order_A and order_B
            self.SendOrder(instrument, order_A[0], order_A[1], order_A[2], order_A[3])
            self.SendOrder(instrument, order_B[0], order_B[1], order_B[2], order_B[3])
            return True

        elif best_bid > best_ask and NASDAQ_info[0][0] == best_bid and IEX_info[1][0] == best_ask:
            print("check_trade_and_place_orders(): cond2",timestamp, flush=True)
            
            size = min(IEX_info[1][1], NASDAQ_info[0][1])
            order_A = ['BID', best_ask, size, 'IEX']
            order_B = ['ASK', best_bid, size, 'NASDAQ']

            # Add: Send order_A and order_B
            self.SendOrder(instrument, order_A[0], order_A[1], order_A[2], order_A[3])
            self.SendOrder(instrument, order_B[0], order_B[1], order_B[2], order_B[3])
            return True
        
        elif NASDAQ_width == one_tick and IEX_width == one_tick and NASDAQ_info[0][0] > IEX_info[0][0]:
        # elif NASDAQ_info[0][0] > IEX_info[0][0]:
            print("check_trade_and_place_orders(): cond3",timestamp, flush=True)
            
            # Go Long on IEX
            order = ['BID', IEX_info[1][0], IEX_info[1][1], 'IEX']

            # Add: Send order
            self.SendOrder(instrument, order[0], order[1], order[2], order[3])
            return True

        elif NASDAQ_width == one_tick and IEX_width == one_tick and NASDAQ_info[0][0] < IEX_info[0][0]:
        # elif NASDAQ_info[0][0] < IEX_info[0][0]:
            print("check_trade_and_place_orders(): cond4",timestamp, flush=True)
            
            # Go Short on IEX
            order = ['ASK', IEX_info[0][0], IEX_info[0][1], 'IEX']

            # Add: Send order
            self.SendOrder(instrument, order[0], order[1], order[2], order[3])
            return True
        
        else:
            return False

    
   
        
    def proc_event(self, instrument, IEX_info, NASDAQ_info, timestamp):
        # market_info is formatted as: [[Best Bid Price, Best Bid Volume], [Best Ask Price, Best Ask Volume]]
        # print("proc_event(): IEX_info", IEX_info, "NASDAQ_info", NASDAQ_info, "timestamp", timestamp, flush=True)
        
        best_bid = max(IEX_info[0][0], NASDAQ_info[0][0])
        best_ask = min(IEX_info[1][0], NASDAQ_info[1][0])

        self.sm.process_event(best_bid, best_ask)

        if self.sm.size != 0:
            # Already in a position
            self.sm.process_event(best_bid, best_ask)
            action = self.sm.get_action(best_bid, best_ask)
            
            if action[0] != '':
                # print('Action: ', action, flush=True)
                self.execute_action(instrument, action, IEX_info, NASDAQ_info)

        trade_occurred = self.check_trade_and_place_orders(instrument, best_bid, best_ask, IEX_info, NASDAQ_info, timestamp)
        # if trade_occurred:
        #     print('Trade Condition: True', flush=True)
        return
