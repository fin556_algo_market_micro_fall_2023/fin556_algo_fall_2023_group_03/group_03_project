#include <MarketModels/Trade.h>
#include <MarketModels/Markets.h>
#include <MarketModels/MarketCenter.h>
#include <MarketModels/NBBOQuote.h>
#include <MarketModels/IAggrOrderBook.h>
#include <MarketModels/IOrderBook.h>

using namespace RCM::StrategyStudio;
using namespace RCM::StrategyStudio::MarketModels;
using namespace RCM::StrategyStudio::Utilities;
namespace py = pybind11;

namespace bindings {
    void bind_instrument(py::module_ &m) {
        py::class_ <Trade>(m, "Trade")
                .def("price", &Trade::price)
                .def("set_price", &Trade::set_price)
                .def("size", &Trade::size)
                .def("set_size", &Trade::set_size)
                .def(py::init<double, int, MarketCenterID>(), "Base Constructor");

        py::class_ <NBBOQuote>(m, "NBBOQuote")
            .def("bid_exchange", &NBBOQuote::bid_exchange)
            .def("set_bid_exchange", &NBBOQuote::set_bid_exchange)
            .def("ask_exchange", &NBBOQuote::ask_exchange)
            .def("set_ask_exchange", &NBBOQuote::set_ask_exchange)
            .def(py::init<double, int, MarketCenterID, double, int, MarketCenterID>(), "Base Constructor");

        py::class_ <Markets>(m, "Markets")
            .def("market_center", overload_cast_<MarketCenterID>()(&Markets::market_center, py::const_), py::return_value_policy::reference)
            .def("aggregate_order_book", overload_cast_<>()(&Markets::aggregate_order_book, py::const_), py::return_value_policy::reference)
            .def("top_quote", &Markets::top_quote, py::return_value_policy::reference)
            .def("last_trade", &Markets::last_trade, py::return_value_policy::reference)
            .def("num_depth_updates_processed", &Markets::num_depth_updates_processed)
            .def("set_top_quote", &Markets::set_top_quote)
            .def("MinExchangeBidSize", &Markets::MinExchangeBidSize)
            .def("MinExchangeAskSize", &Markets::MinExchangeAskSize)
            .def("UpdateTopQuoteFromNBBO", &Markets::UpdateTopQuoteFromNBBO);

        py::class_<MarketCenter>(m, "MarketCenter")
            .def("order_book", overload_cast_<>()(&MarketCenter::order_book, py::const_), py::return_value_policy::reference);

        py::class_<IOrderBook>(m, "IOrderBook")
            .def("BestBidLevel", &IOrderBook::BestBidLevel, py::return_value_policy::reference)
            .def("BestAskLevel", &IOrderBook::BestAskLevel, py::return_value_policy::reference)
            .def("WorstBidLevel", &IOrderBook::WorstBidLevel, py::return_value_policy::reference)
            .def("WorstAskLevel", &IOrderBook::WorstAskLevel, py::return_value_policy::reference)
            // Add bindings for other member functions here
            .def("FindBidLevel", &IOrderBook::FindBidLevel, py::return_value_policy::reference)
            .def("FindAskLevel", &IOrderBook::FindAskLevel, py::return_value_policy::reference)
            .def("BidPriceLevelAtLevel", &IOrderBook::BidPriceLevelAtLevel, py::return_value_policy::reference)
            .def("AskPriceLevelAtLevel", &IOrderBook::AskPriceLevelAtLevel, py::return_value_policy::reference)
            .def("market_center", &IOrderBook::market_center);

        py::class_ <IPriceLevel>(m, "IPriceLevel")
            .def("num_orders", &IPriceLevel::num_orders)
            .def("price", &IPriceLevel::price)
            .def("size", &IPriceLevel::size);


        py::class_ <IAggrOrderBook>(m, "IAggrOrderBook")
            .def("BestBidLevel", &IAggrOrderBook::BestBidLevel, py::return_value_policy::reference)
            .def("BestAskLevel", &IAggrOrderBook::BestAskLevel, py::return_value_policy::reference)
            .def("WorstBidLevel", &IAggrOrderBook::WorstBidLevel, py::return_value_policy::reference)
            .def("WorstAskLevel", &IAggrOrderBook::WorstAskLevel, py::return_value_policy::reference)
            .def("NumBidMarketCentersAtPrice", &IAggrOrderBook::NumBidMarketCentersAtPrice, py::return_value_policy::reference)
            .def("NumAskMarketCentersAtPrice", &IAggrOrderBook::NumAskMarketCentersAtPrice, py::return_value_policy::reference)
            .def("NumBidMarketCentersAtLevel", &IAggrOrderBook::NumBidMarketCentersAtLevel, py::return_value_policy::reference)
            .def("NumAskMarketCentersAtLevel", &IAggrOrderBook::NumAskMarketCentersAtLevel, py::return_value_policy::reference)
            .def("NumBidLevels", &IAggrOrderBook::NumBidLevels)
            .def("NumAskLevels", &IAggrOrderBook::NumAskLevels)
            .def("NumBidOrders", &IAggrOrderBook::NumBidOrders)
            .def("NumAskOrders", &IAggrOrderBook::NumAskOrders)
            .def("NumTotalOrders", &IAggrOrderBook::NumTotalOrders)
            .def("BidSizeAtLevel", &IAggrOrderBook::BidSizeAtLevel)
            .def("AskSizeAtLevel", &IAggrOrderBook::AskSizeAtLevel)
            .def("CheckBuyPriceLevelExists", &IAggrOrderBook::CheckBuyPriceLevelExists)
            .def("CheckSellPriceLevelExists", &IAggrOrderBook::CheckSellPriceLevelExists)
            .def("BidSizeAtPrice", &IAggrOrderBook::BidSizeAtPrice)
            .def("AskSizeAtPrice", &IAggrOrderBook::AskSizeAtPrice)
            .def("TotalBidSize", &IAggrOrderBook::TotalBidSize)
            .def("TotalAskSize", &IAggrOrderBook::TotalAskSize);

            
        py::class_ <IAggrPriceLevel>(m, "IAggrPriceLevel")
            .def("num_markets", &IAggrPriceLevel::num_markets)
            .def("IsMarketCenterParticipating", &IAggrPriceLevel::IsMarketCenterParticipating)
            .def("price", &IAggrPriceLevel::price)   
            .def("size", &IAggrPriceLevel::size)
            .def("next", &IAggrPriceLevel::next, py::return_value_policy::reference)
            .def("previous", &IAggrPriceLevel::previous, py::return_value_policy::reference);

        py::class_ <IBookEntry>(m, "IBookEntry")
            .def("price", &IBookEntry::price)
            .def("size", &IBookEntry::size);

        py::class_ <IPriceLevelBase>(m, "IPriceLevelBase")
            .def("num_orders", &IPriceLevelBase::num_orders)
            .def("next", &IPriceLevelBase::next, py::return_value_policy::reference)
            .def("previous", &IPriceLevelBase::previous, py::return_value_policy::reference);

        py::class_<Instrument>(m, "Instrument")
            .def("symbol", &Instrument::symbol)
            .def("underlier", &Instrument::underlier, py::return_value_policy::take_ownership) // Returns a Pointer -> Please Delete
            .def("aggregate_order_book", overload_cast_<>()(&Instrument::aggregate_order_book, py::const_), py::return_value_policy::reference) // Returns a Pointer -> Please Delete
            .def("markets", overload_cast_<>()(&Instrument::markets, py::const_), py::return_value_policy::reference) // Do this
            .def("nbbo", overload_cast_<>()(&Instrument::nbbo, py::const_), py::return_value_policy::reference) // Do this
            .def("last_trade", overload_cast_<>()(&Instrument::last_trade, py::const_), py::return_value_policy::reference)// Do this
            .def("is_first_trade_received", &Instrument::is_first_trade_received)
            .def("AddChildInstrument", &Instrument::AddChildInstrument) // Takes in a parameter of Instrument -> pointer
            .def("HasChildInstrument", &Instrument::HasChildInstrument)
            .def("IsCalcTopQuoteBetter", &Instrument::IsCalcTopQuoteBetter)
            .def("top_quote", &Instrument::top_quote, py::return_value_policy::reference);
            
        py::class_<Quote>(m, "Quote")
            .def("bid", &Quote::bid)
            .def("ask", &Quote::ask)
            .def("bid_size", &Quote::bid_size)
            .def("ask_size", &Quote::ask_size);
        
        // NBBOQuote 
        // InstrumentType
        // IAggrOrderBook
        // Markets
        // Trade, TradeStats
        
    }

} // namespace bindings


