## Running Pybind Strategy Studio on VM

To run the Pybind Strategy Studio on your VM, follow these steps:

1. **Copy Directory to VM:**
   Copy the entire directory to the VM, ensuring that the VM is set up with the required software, especially related to the strategy studio. There is a copy of this directory in `/groupstorage/group03/PybindSSTest`.
2. **Edit MyStrategy.py:**
   Navigate to the `PybindStrategy` directory and edit the `MyStrategy.py` file. If importing any scripts/libraries, make sure to edit the Makefile.
3. **Modify go.sh:**
   Edit the `go.sh` script to adjust the `symbols` according to your requirements.
   Edit the `scripts/run_backtest.sh` script to set the cpu time and memory limits.
4. **Run the Script:**
   Execute the following command in the terminal:

   ```shell
   ./go.sh --instance <instanceName> --dates <dates> --latency <latencies> --data-dir <dataDir>
   ```

   Replace `<instanceName>`, `<dates>`, `<latencies>`, and `<dataDir>` with your specific values. For example:

   ```shell
   ./go.sh --instance PybindTest --dates 2023-01-04,2023-01-05 --latency 0,10,100 --data-dir /groupstorage/iexdownloaderparser/data/text_tick_data
   ```

## Backtest on Multi Exchange

1. Edit `ss/bt/preferred_feeds.csv` to include all data sources.
2. Combine different text tick files into one, with the filename convention `tick_symbol_yyyymmdd.txt.gz`. The data should also be in acending order of the time.

## Notes:

- The IEX data is located in `/groupstorage/iexdownloaderparser/data`.
- Merged data can be found in `/groupstorage/group03/data/merged_text_tick_data`.
- Avoid excessive prints in the strategy code to maintain optimal performance.

## Technical Details:

### Pybind:

Our implementation is based on IE421 Spring23 group02's [PybindStrategy](https://gitlab-beta.engr.illinois.edu/ie421_high_frequency_trading_spring_2023/ie421_hft_spring_2023_group_02/group_02_project/-/tree/main/PybindStrategy?ref_type=heads). We extended this by adding essential bindings, specifically enhancing the capability to retrieve the single Best Bid and Offer (BBO) for one market center in `PybindStrategy/includes/instrument.h`.

### Pipeline:

To streamline the backtesting process, we created bash scripts that enable users to run backtests on different dates and latencies using a single command line. Additionally, users have the flexibility to link various data directories as sources for tick text data.