#!/bin/bash

# usage ./rename_output.sh <strategy_name>
cd /groupstorage/group03/PybindSSTest

strategy_name=$1
latency=$2

for file in output/BACK_*.csv; do
  # Extract the start and end dates from the file name
  start_date=$(echo $file | grep -oP 'start_\K[0-9-]+')
  end_date=$(echo $file | grep -oP 'end_\K[0-9-]+')
  
  # Extract the type of file (fill, order, pnl) by matching it before the .csv extension
  file_type=$(echo $file | grep -oE '(fill|order|pnl).csv' | sed 's/.csv//')
  
  # Construct the new file name
  new_file_name="output/${strategy_name}_SPY_${start_date}_${end_date}_${latency}_${file_type}.csv"
  
  # Rename the file
  mv "$file" "$new_file_name"
done
