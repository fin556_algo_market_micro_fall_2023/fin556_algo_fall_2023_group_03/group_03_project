#!/bin/bash
# usage: ./build_strategy.sh <dataDir>
# compile pybind strategy and copy to strategy studio

dataDir=$1

cd /groupstorage/group03/PybindSSTest

echo "Compressing PybindStrategy"
tar -czf PybindStrategy.tar.gz -C PybindStrategy .

echo "Building strategy"
mkdir -p /home/vagrant/ss/sdk/RCM/StrategyStudio/examples/strategies/PybindStrategy/
tar -xzf PybindStrategy.tar.gz -C /home/vagrant/ss/sdk/RCM/StrategyStudio/examples/strategies/PybindStrategy
cd /home/vagrant/ss/sdk/RCM/StrategyStudio/examples/strategies/PybindStrategy
mkdir -p /home/vagrant/ss/bt/strategies_dlls
rm /home/vagrant/ss/bt/strategies_dlls/*

echo "Make copy strategy"
make copy_strategy

