#!/bin/bash

instanceName=$1

cd /groupstorage/group03/PybindSSTest
rm PybindStrategy.tar.gz

cd /home/vagrant/ss/bt/utilities/
echo "Terminating strategy"
./StrategyCommandLine cmd terminate $instanceName

sleep 2

./StrategyCommandLine cmd quit 

echo "Deleting Strategy"
rm -r /home/vagrant/ss/sdk/RCM/StrategyStudio/examples/strategies/PybindStrategy

echo "clean up strategy!"
