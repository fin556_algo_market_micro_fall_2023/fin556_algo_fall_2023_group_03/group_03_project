#!/bin/bash

# Set CPU time limit to 1 hour
ulimit -t 3600

# Set memory limit to 6GB
ulimit -v $((6 * 1024 * 1024))

instanceName=$1
startDate=$2
endDate=$2
symbols=$3
latency=$4


outputDir='/home/vagrant/ss/bt/backtesting-results/csv_files'
workDir=$PWD

echo "Starting strategy server"
cd /home/vagrant/ss/bt/  
./StrategyServerBacktesting &
sleep 1


echo "Creating instance"
cd /home/vagrant/ss/bt/utilities

echo "Terminate instance if exists"
./StrategyCommandLine cmd terminate $instanceName
./StrategyCommandLine cmd create_instance $instanceName PybindStrategy UIUC SIM-1001-101 dlariviere 1000000 -symbols $symbols

./StrategyCommandLine cmd set_simulator_params -latency_to_exchange $latency

# see if create successfully
./StrategyCommandLine cmd strategy_instance_list

cd /groupstorage/group03/PybindSSTest


echo "Starting backtest"

# Get the line number which ends with finished. 
logFileNumLines=$(cat /home/vagrant/ss/bt/logs/main_log.txt | wc -l)
foundFinishedLogFile=$(grep -nr "finished.$" /home/vagrant/ss/bt/logs/main_log.txt | gawk '{print $1}' FS=":"|tail -1)

cd /home/vagrant/ss/bt/utilities/ && ./StrategyCommandLine cmd start_backtest $startDate $endDate $instanceName 0

# DEBUGGING OUTPUT
echo "logFileNumLines:",$logFileNumLines

# If the line ending with finished. is less than the previous length of the log file, then strategyBacktesting has not finished, 
# once its greater than the previous, it means it has finished.
while ((logFileNumLines > foundFinishedLogFile))
do
    foundFinishedLogFile=$(grep -nr "finished.$" /home/vagrant/ss/bt/logs/main_log.txt | gawk '{print $1}' FS=":" | tail -1)
    echo "foundFinishedLogFile:",$foundFinishedLogFile

    # DEBUGGING OUTPUT
    echo "Waiting for strategy to finish"
    sleep 10
done


echo "Sleeping for 10 seconds..."

sleep 10

echo "Strategy Studio finished backtesting"

latestCRA=$(ls /home/vagrant/ss/bt/backtesting-results/BACK_*.cra -t | head -n1)

# Print the path of the craFile
echo "CRA file found:", $latestCRA


# Use the StrategyCommandLine utility to eport the CRA file to CSV format
# Name of file should be the latest craFile created
echo "Exporting CRA file to CSV format to $outputDir"

# Navigate to utilities directory
cd /home/vagrant/ss/bt/utilities/
./StrategyCommandLine cmd export_cra_file $latestCRA $outputDir
sleep 10

# remove cra
echo "copy backtest data to currdir" 
cd $workDir
mv $outputDir/* "$workDir/output"
rm $latestCRA
