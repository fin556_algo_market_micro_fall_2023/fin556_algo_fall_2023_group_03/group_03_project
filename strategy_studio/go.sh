#!/bin/bash
# usage: ./go.sh --instance <instanceName> --dates <dates> --latency <latencies> --data-dir <dataDir>

#!/bin/bash


# Default values
instanceName="PybindStrategy"
dataDir="/groupstorage/iexdownloaderparser/data/text_tick_data"
dates=("2019-12-30")
latencies=(0)

# Parse arguments
while [[ $# -gt 0 ]]; do
    key="$1"
    case $key in
        --instance)
            instanceName="$2"
            shift # past argument
            shift # past value
            ;;
        --dates)
            dates=($(echo "$2" | tr ',' ' '))
            shift # past argument
            shift # past value
            ;;
        --latency)
            latencies=($(echo "$2" | tr ',' ' '))
            shift # past argument
            shift # past value
            ;;
        --data-dir)
            dataDir="$2"
            shift # past argument
            shift # past value
            ;;
        *)    # unknown option
            shift # past argument
            ;;
    esac
done


symbols="SPY"
outputDir='/home/vagrant/ss/bt/backtesting-results/csv_files'
workDir=$PWD

# echo "linking data"
mv /home/vagrant/ss/bt/text_tick_data /home/vagrant/ss/bt/text_tick_data_backup
# ln -s /groupstorage/group03/data/text_tick_data /home/vagrant/ss/bt/text_tick_data
ln -s $dataDir /home/vagrant/ss/bt/text_tick_data

./scripts/build_strategy.sh $dataDir

# run strategy on latencies and dates
for latency in "${latencies[@]}"; do
    for date in "${dates[@]}"; do
        echo "Running backtest for $date, Set simulator latency to $latency"
        
        ./scripts/run_backtest.sh "$instanceName" "$date" "$symbols" $latency

        # rename output
        ./scripts/rename_output.sh $instanceName $latency

        # clean up strategy
        ./scripts/cleanup_strategy.sh $instanceName

        sleep 2
    done
done


# relink text_tick_data
echo "Relink text_tick_date"
rm -r /home/vagrant/ss/bt/text_tick_data
mv /home/vagrant/ss/bt/text_tick_data_backup /home/vagrant/ss/bt/text_tick_data 
