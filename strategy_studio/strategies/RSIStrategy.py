# #!/usr/bin/env python
# # coding: utf-8

from strategy import *
from BaseStrategy import BaseStrategy

class MyStrategy(BaseStrategy):
    def __init__(self, strategy_actions):
        self.window_size = 100
        self.window = []
        self.overbought = 70
        self.oversold = 30
        self.delta = []
        
        # track the trade info
        self.stock_size = 0
        self.revenue = 0.0
        self.capital = 0.0
        self.trade_times = 0

        super().__init__(strategy_actions)

    def OnResetStrategyState(self):
        self.window_size = 100
        self.window = []
        self.overbought = 70
        self.oversold = 30
        self.delta = []
        
        # track the trade info
        self.stock_size = 0
        self.revenue = 0.0
        self.capital = 0.0
        self.trade_times = 0

        super().__init__(strategy_actions)
        
    # Set flags before the strategy is started
    def SetFlags(self):
        pass

    def OnTrade(self, msg):
        instrument = msg.instrument()
        mid_price = (instrument.top_quote().ask() + instrument.top_quote().bid()) / 2

        # Manage the window of prices
        if len(self.window) >= self.window_size:
            self.window.pop(0)
            self.delta.pop(0)

        # Calculate price change
        price_change = mid_price - self.window[-1] if self.window else 0
        self.delta.append(price_change)
        self.window.append(mid_price)

        # Ensure we have enough data
        if len(self.window) < self.window_size:
            return

        # Compute total gains and losses in one loop
        total_gains, total_losses = 0, 0
        for change in self.delta:
            if change > 0:
                total_gains += change
            else:
                total_losses -= change

        avg_gain = total_gains / self.window_size
        avg_loss = total_losses / self.window_size

        # Calculate RSI, setting it to 100 if avg_loss is 0
        rsi = 100 if avg_loss == 0 else 100 - 100 / (1 + avg_gain / avg_loss)
        
        # print("rsi = " + str(rsi))
        if rsi < self.oversold :
            self.SendOrder(instrument, 100)    
            self.stock_size += 100
            self.revenue -= instrument.top_quote().ask() * 100
            self.trade_times += 1
        elif rsi > self.overbought and self.stock_size > 0:
            trade_size = min(100, self.stock_size)
            self.SendOrder(instrument, -trade_size)    
            self.stock_size -= trade_size
            self.revenue += instrument.top_quote().bid() * trade_size
            self.trade_times += 1
        
        self.capital = self.stock_size * instrument.top_quote().bid() + self.revenue

        # print("Capital = " + str(self.capital) + " size = " + str(self.stock_size) + " trade times = " + str(self.trade_times))
    


    def OnTopQuote(self, msg):
        pass

    def OnQuote(self, msg): 
        pass
    
    def OnBar(self, msg): 
        pass

    def OnDepth(self, msg):
        pass
    
    def OnOrderUpdate(self, msg): 
        pass
    

    def RegisterForStrategyEvents(self, eventRegister, currDate):
        pass
    
    def SendOrder(self, instrument, trade_size): 
        price = instrument.top_quote().ask() if trade_size > 0 else instrument.top_quote().bid()
        action = "buy" if trade_size > 0 else "sell"
        
        params = OrderParams(instrument, abs(trade_size), price, MARKET_CENTER_ID_IEX, (ORDER_SIDE_BUY if trade_size > 0 else ORDER_SIDE_SELL), ORDER_TIF_DAY, ORDER_TYPE_LIMIT)

        print("SendTradeOrder(): about to send new order for size " + str(trade_size) + " at $" + str(price) + " to " + str(action) + " " + instrument.symbol())
        
        self.trade_actions().SendNewOrder(params)
        pass

    def OnNews(self, msg):
        pass  
