#!/bin/bash

# usage: ./go.sh <instanceName> <date>
# TODO: customize the following variables
# currently only can be run on 1 day... 
startDate=$2
endDate=$3
# startDate='2021-11-05'
# endDate='2021-11-05'
instanceName=$1
# symbols="AAPL"
symbols="SPY"
outputDir='/home/vagrant/ss/bt/backtesting-results/csv_files'
workDir=$PWD
dataDir='/groupstorage/iexdownloaderparser/data/text_tick_data' 
# dataDir='/groupstorage/group03/PybindSSTest/text_tick_data'


# echo "linking data"
mv /home/vagrant/ss/bt/text_tick_data /home/vagrant/ss/bt/text_tick_data_backup
# ln -s /groupstorage/group03/data/text_tick_data /home/vagrant/ss/bt/text_tick_data
ln -s $dataDir /home/vagrant/ss/bt/text_tick_data


echo "Compressing PybindStrategy"
tar -czf PybindStrategy.tar.gz -C PybindStrategy .

# no sure if this is needed
# pypy3 delete_instance.py

echo "Building strategy"
mkdir -p /home/vagrant/ss/sdk/RCM/StrategyStudio/examples/strategies/PybindStrategy/
tar -xzf PybindStrategy.tar.gz -C /home/vagrant/ss/sdk/RCM/StrategyStudio/examples/strategies/PybindStrategy
cd /home/vagrant/ss/sdk/RCM/StrategyStudio/examples/strategies/PybindStrategy
mkdir -p /home/vagrant/ss/bt/strategies_dlls
rm /home/vagrant/ss/bt/strategies_dlls/*

echo "Make copy strategy"
make copy_strategy

echo "Starting strategy server"
cd /home/vagrant/ss/bt/  
./StrategyServerBacktesting &
sleep 1


echo "Creating instance"
cd /home/vagrant/ss/bt/utilities

echo "Terminate instance if exists"
./StrategyCommandLine cmd terminate $instanceName
./StrategyCommandLine cmd create_instance $instanceName PybindStrategy UIUC SIM-1001-101 dlariviere 1000000 -symbols $symbols

echo "Set simulator latency to 100ms"
./StrategyCommandLine cmd set_simulator_params -latency_to_exchange 1000 

# see if create successfully
./StrategyCommandLine cmd strategy_instance_list

echo "Starting backtest"
cd /home/vagrant/ss/bt/utilities/ && ./StrategyCommandLine cmd start_backtest $startDate $endDate $instanceName 0

# Get the line number which ends with finished. 
foundFinishedLogFile=$(grep -nr "finished.$" /home/vagrant/ss/bt/logs/main_log.txt | gawk '{print $1}' FS=":"|tail -1)
logFileNumLines=$(cat /home/vagrant/ss/bt/logs/main_log.txt | wc -l)

# DEBUGGING OUTPUT
echo "Last line found:",$foundFinishedLogFile

# If the line ending with finished. is less than the previous length of the log file, then strategyBacktesting has not finished, 
# once its greater than the previous, it means it has finished.
while ((logFileNumLines > foundFinishedLogFile))
do
    foundFinishedLogFile=$(grep -nr "finished.$" /home/vagrant/ss/bt/logs/main_log.txt | gawk '{print $1}' FS=":"|tail -1)

    #DEBUGGING OUTPUT
    echo "Waiting for strategy to finish"
    sleep 1
done

echo "Sleeping for 10 seconds..."

sleep 10

echo "Strategy Studio finished backtesting"

latestCRA=$(ls /home/vagrant/ss/bt/backtesting-results/BACK_*.cra -t | head -n1)

# Print the path of the craFile
echo "CRA file found:", $latestCRA

# Navigate to utilities directory
cd /home/vagrant/ss/bt/utilities/

# Use the StrategyCommandLine utility to eport the CRA file to CSV format
# Name of file should be the latest craFile created
echo "Exporting CRA file to CSV format to $outputDir"

./StrategyCommandLine cmd export_cra_file $latestCRA $outputDir
sleep 2

# remove cra
echo "copy backtest data to currdir" 
cd $workDir
mv $outputDir/* "$workDir/output"
rm $latestCRA
rm PybindStrategy.tar.gz

# clean up
echo "Relink text_tick_date"
rm -r /home/vagrant/ss/bt/text_tick_data
mv /home/vagrant/ss/bt/text_tick_data_backup /home/vagrant/ss/bt/text_tick_data 

cd /home/vagrant/ss/bt/utilities/
sleep 5
echo "Terminating strategy"
./StrategyCommandLine cmd terminate $instanceName
./StrategyCommandLine cmd quit 

echo "Deleting PybindStrategy"
rm -r /home/vagrant/ss/sdk/RCM/StrategyStudio/examples/strategies/PybindStrategy
rm /home/vagrant/ss/bt/strategies_dlls/PybindStrategy.so

# rename log file
echo "Rename log file"
cd $workDir
./scripts/rename_output.sh $instanceName

echo "Finish Backtesting!"


