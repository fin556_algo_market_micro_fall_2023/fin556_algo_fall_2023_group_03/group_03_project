#!/bin/bash

# Usage: ./script.sh 2023-01-01 2023-01-05 2023-01-10
# Each date is passed as a separate argument to the script

# Loop over each argument
for date in "$@"; do
    # Run the Python script for the current date
    python3 /groupstorage/iexdownloaderparser/src/download_iex_pcaps.py --start-date $date --end-date $date --download-dir /groupstorage/iexdownloaderparser/data/iex_downloads
done
