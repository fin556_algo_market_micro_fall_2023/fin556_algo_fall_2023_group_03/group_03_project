#!/bin/bash

if [ -e /usr/bin/python3 ]; then
	PYTHON_INTERP=python3
fi

if [ -e /usr/bin/pypy3.7 ]; then
	PYTHON_INTERP=pypy3.7
fi

if [ -e /var/lib/snapd/snap/bin/pypy3 ]; then
	PYTHON_INTERP=pypy3
fi

# Function to convert a date to the "yyyymmdd" format
yyyymmdd_format() {
  date -j -f "%Y-%m-%d" "$1" +%Y%m%d
}

download() {
    local date="$1" # 2023-01-04
    python3 src/download_iex_pcaps.py --start-date $date --end-date $date --download-dir data/iex_downloads
}

parse() {
    local symbol="$1"
    local pcap="$2"

    # Extract the date from the pcap file
    pcap_date=$(echo "$pcap" | sed -r 's/.*data_feeds_(.*)_(.*)_IEXTP.*/\1/')

    echo "PCAP_FILE=$pcap PCAP_DATE=$pcap_date"
    start_time=$(date +"%Y-%m-%d %H:%M:%S")

    # --symbols ALL or --symbols symb1,symb2,symb3
    # add --output-deep-books-too to output book snapshots
    command="gunzip -d -c $pcap | tcpdump -r - -w - -s 0 | $PYTHON_INTERP src/parse_iex_pcap.py /dev/stdin --symbols $symbol --trade-date $pcap_date"
    echo "$command"

    gunzip -d -c $pcap | tcpdump -r - -w - -s 0 | $PYTHON_INTERP src/parse_iex_pcap.py /dev/stdin --symbols $symbol --trade-date $pcap_date

    end_time=$(date +"%Y-%m-%d %H:%M:%S")
    echo "Start time: $start_time"
    echo "End time: $end_time"
 
}


# Start and end dates provided as arguments (in yyyy-mm-dd format)
start_date="$1"
end_date="$2"

# Convert start and end dates to seconds since epoch
start_seconds=$(date -j -f "%Y-%m-%d" "$start_date" +%s)
end_seconds=$(date -j -f "%Y-%m-%d" "$end_date" +%s)

# Iterate through the dates and echo them in both formats
current_seconds="$start_seconds"
while [ "$current_seconds" -le "$end_seconds" ]; do
  current_date=$(date -j -f "%s" "$current_seconds" "+%Y-%m-%d")
  yyyymmdd_data=$(yyyymmdd_format "$current_date")

  # use batch
  # ./parse_batch.sh "$current_date" $(yyyymmdd_format "$current_date")

  # download
  download "$current_date"

  # parse 
  symbol="XOP,NOG,SOI,USOI,DBO,DRIP,OILD,OIS,OILK,BOIL,IEZ,UAL,USL,NRT,SNCY,NRGD,CRUZ,FTXN,MRO,USO,MGY,VIST,OILU,AAL,BATL,MVO,CRAK,MUR,LUV,ODC,XES,PXJ,IEO,NRGU,JETU,BNO,SAVE,GUSH,IMO,OIH"

  parse $symbol "data/iex_downloads/DEEP/data_feeds_${yyyymmdd_data}_${yyyymmdd_data}_IEXTP1_DEEP1.0.pcap.gz" 

  # gunzip the files
  gunzip data/text_tick_data/*$yyyymmdd_data.txt.gz 

  # zip the txt files 
  zip text_tick_$yyyymmdd_data.zip data/text_tick_data/tick_*_$yyyymmdd_data.txt

  # rm all the txt files and the pcap file 
  rm data/text_tick_data/*_$yyyymmdd_data.txt
  rm data/iex_downloads/DEEP/*gz

  current_seconds=$((current_seconds + 86400)) # 86400 seconds in a day
done
