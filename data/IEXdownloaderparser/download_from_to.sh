#!/bin/bash

python3 /groupstorage/iexdownloaderparser/src/download_iex_pcaps.py --start-date $1 --end-date $2 --download-dir /groupstorage/iexdownloaderparser/data/iex_downloads