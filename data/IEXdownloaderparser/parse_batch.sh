#!/bin/bash

# Set the Python interpreter
if [ -e /usr/bin/python3 ]; then
    PYTHON_INTERP=python3
elif [ -e /usr/bin/pypy3.7 ]; then
    PYTHON_INTERP=pypy3.7
elif [ -e /var/lib/snapd/snap/bin/pypy3 ]; then
    PYTHON_INTERP=pypy3
else
    echo "Python interpreter not found."
    exit 1
fi


download() {
    local date="$1" # 2023-01-04
    python3 src/download_iex_pcaps.py --start-date $date --end-date $date --download-dir data/iex_downloads
}

process_batch() {
    local batch="$1"
    local pcap="$2"

    # Extract the date from the pcap file
    pcap_date=$(echo "$pcap" | sed -r 's/.*data_feeds_(.*)_(.*)_IEXTP.*/\1/')

    if [ "$pcap_date" == "$3" ]; then
        echo "PCAP_FILE=$pcap PCAP_DATE=$pcap_date"
        start_time=$(date +"%Y-%m-%d %H:%M:%S")

        # Replace "--symbols ALL" with the batch of symbols
        command="gunzip -d -c $pcap | tcpdump -r - -w - -s 0 | $PYTHON_INTERP src/parse_iex_pcap.py /dev/stdin --symbols $batch --trade-date $pcap_date 1>/dev/null"
        echo "$command"
        eval "$command"  # Execute the command

        end_time=$(date +"%Y-%m-%d %H:%M:%S")
        echo "Start time: $start_time"
        echo "End time: $end_time"
    fi
}

# Set the number of lines in each batch (n)
batch_size=1000

# Input file with one symbol per line
input_file="all_tickers.txt"

# Initialize an empty variable to store the batch
batch=""

first_batch_processed="false"

# download 
download $1

# Read the input file line by line
while IFS= read -r line; do
    # Append the current line to the batch with a comma
    if [ -n "$batch" ]; then
        batch="$batch,$line"
    else
        batch="$line"
    fi

    # Increment the line count
    ((line_count++))

    # Check if the batch size is reached or if it's the last line
    if [ $line_count -ge $batch_size ] || [ -z "$line" ]; then
        # Output the batch to the output file
        for pcap in $(ls data/iex_downloads/DEEP/*gz); do
            if [ "$first_batch_processed" == "false" ]; then
                process_batch "$batch" "$pcap" "$2"
            fi
        done
        # Clear the batch and reset the line count
        # first_batch_processed="true"
        batch=""
        line_count=0
    fi
done < "$input_file"

# gunzip the files
gunzip data/text_tick_data/*.gz 

# zip the txt files 
zip text_tick_all_symbols_$2.zip data/text_tick_data/*.txt

# rm all the txt files and the pcap file 
rm data/text_tick_data/tick_*
rm data/iex_downloads/DEEP/*gz