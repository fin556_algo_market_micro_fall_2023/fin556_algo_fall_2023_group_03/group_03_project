# [IEX downloader parser](https://gitlab.engr.illinois.edu/shared_code/iexdownloaderparser/-/tree/main?ref_type=heads)

To fetch and parse trading data from IEX. 

Usage:

```shell
# clone the repo
git clone git@gitlab.engr.illinois.edu:shared_code/iexdownloaderparser.git
cd iexdownloaderparser 

# Usage example:
# run the downloader
python3 src/download_iex_pcaps.py --start-date yyyy-mm-dd --end-date yyyy-mm-dd --download-dir data/iex_downloads

# run the parser 
./parse_all.sh

# Process day by day to save disk space... 
# copy the download_and_parse.sh to the iexdownloaderparser
chmod +x download_and_parse.sh 
./download_and_parse.sh 2023-01-01 2023-01-15 


```
