#!/bin/bash

# Set the Python interpreter based on availability
if [ -e /usr/bin/python3 ]; then
    PYTHON_INTERP=python3
elif [ -e /usr/bin/pypy3.7 ]; then
    PYTHON_INTERP=pypy3.7
elif [ -e /var/lib/snapd/snap/bin/pypy3 ]; then
    PYTHON_INTERP=pypy3
fi

# Function to convert a date to the "yyyymmdd" format
yyyymmdd_format() {
  date -f "%Y-%m-%d" "$1" +%Y%m%d
}

parse() {
    local symbol="$1"
    local pcap="$2"

    # Extract the date from the pcap file
    pcap_date=$(echo "$pcap" | sed -r 's/.*data_feeds_(.*)_(.*)_IEXTP.*/\1/')

    echo "PCAP_FILE=$pcap PCAP_DATE=$pcap_date"
    start_time=$(date +"%Y-%m-%d %H:%M:%S")

    # Form the command with the provided symbol and date
    command="gunzip -d -c $pcap | tcpdump -r - -w - -s 0 | $PYTHON_INTERP src/parse_iex_pcap.py /dev/stdin --symbols $symbol --trade-date $pcap_date --output-deep-books-too"
    echo "$command"

    # Execute the command
    eval "$command"

    end_time=$(date +"%Y-%m-%d %H:%M:%S")
    echo "Start time: $start_time"
    echo "End time: $end_time"
}

# Start and end dates provided as arguments (in yyyy-mm-dd format)
start_date="$1"
end_date="$2"

# Convert start and end dates to seconds since epoch
start_seconds=$(date -f "%Y-%m-%d" "$start_date" +%s)
end_seconds=$(date -f "%Y-%m-%d" "$end_date" +%s)

# Iterate through the dates
current_seconds="$start_seconds"
while [ "$current_seconds" -le "$end_seconds" ]; do
  current_date=$(date -f "%s" -d "@$current_seconds" "+%Y-%m-%d")
  yyyymmdd_data=$(yyyymmdd_format "$current_date")

  # Parse function call
  symbol="SPY"
  parse "$symbol" "data/iex_downloads/DEEP/data_feeds_${yyyymmdd_data}_${yyyymmdd_data}_IEXTP1_DEEP1.0.pcap.gz"

  # Increment the date by one day (86400 seconds)
  current_seconds=$((current_seconds + 86400))
done
