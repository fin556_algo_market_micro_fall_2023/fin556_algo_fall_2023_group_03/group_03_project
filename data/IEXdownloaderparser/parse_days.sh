#!/bin/bash

# Usage: ./parse_days.sh 20230101 20230105 20230110
# Each date is passed as a separate argument to the script
symbol="SPY"


# Set the Python interpreter based on availability
if [ -e /usr/bin/python3 ]; then
    PYTHON_INTERP=python3
elif [ -e /usr/bin/pypy3.7 ]; then
    PYTHON_INTERP=pypy3.7
elif [ -e /var/lib/snapd/snap/bin/pypy3 ]; then
    PYTHON_INTERP=pypy3
fi

parse() {
    local symbol="$1"
    local pcap="$2"

    # Extract the date from the pcap file
    pcap_date=$(echo "$pcap" | sed -r 's/.*data_feeds_(.*)_(.*)_IEXTP.*/\1/')

    echo "PCAP_FILE=$pcap PCAP_DATE=$pcap_date"
    start_time=$(date +"%Y-%m-%d %H:%M:%S")

    # Form the command with the provided symbol and date
    command="gunzip -d -c $pcap | tcpdump -r - -w - -s 0 | $PYTHON_INTERP src/parse_iex_pcap.py /dev/stdin --symbols $symbol --trade-date $pcap_date --output-deep-books-too"
    echo "$command"

    # Execute the command
    eval "$command"

    end_time=$(date +"%Y-%m-%d %H:%M:%S")
    echo "Start time: $start_time"
    echo "End time: $end_time"
}




# Loop over each argument
for date in "$@"; do
  # Run the Python script for the current date
  parse "$symbol" "data/iex_downloads/DEEP/data_feeds_${date}_${date}_IEXTP1_DEEP1.0.pcap.gz"

done
