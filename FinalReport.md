# Arbitragedy

## Project Report

## FIN 556, Fall 2023, Group 3

#### Sayar Ghosh Roy, Tingyin Ding, Blaine Hill

`{sayar3, tingyin4, blaineh2}@illinois.edu`

#### — Advised by Prof. David Lariviere

---

### The Team

---

##### Sayar Ghosh Roy (Team Leader)

I am a fully funded MS CS student at UIUC and a Graduate Researcher in the Data Mining Group (DMG), Data and Information Systems (DAIS) Research Lab, advised by Prof. Jiawei Han. My thesis is on Evidence-grounded Interpretable Local Citation Recommendation.

In July 2022, I graduated top of my class from IIIT Hyderabad with a Bachelor of Technology in CS (with Honors) and an MS in Computational Linguistics. At IIIT-H, I was a Student Researcher in the Information Retrieval and Extraction Lab (iREL), where I was advised by Prof. Vasudeva Varma and Dr. Manish Gupta. I also worked part-time as an Applied Research Engineer with Apple, primarily designing Machine Learning models to solve specific Natural Language Processing and Information Extraction problems within Maps.

You can find me on [LinkedIn](https://www.linkedin.com/in/sayarghoshroy/) and [GitHub](https://github.com/sayarghoshroy). Some of my publicly available work can be found [here](https://sayarghoshroy.github.io/work). I'll graduate from UIUC in May 2024 and look forward to starting a full-time position in the industry.

---

##### Tingyin Ding

I'm Tingyin Ding, a recent graduate with a Master's in Computer Science from the University of Illinois at Urbana-Champaign (December 2023). My academic interests span machine learning, data mining, algorithms, web development, systems, and cloud computing.

Bringing a blend of academic knowledge and industry experience, I've worked as a full-time data engineer and front-end engineer intern. My skill set includes proficiency in Python, C++, Go, JavaScript, and SQL.

Excitingly, I'll be embarking on a new journey post-graduation, joining TikTok as a frontend engineer. If you're interested in connecting, let's stay in touch on [LinkedIn](https://www.linkedin.com/in/tingyin-ding-a1726417a/).

---

##### Blaine Hill

I'm Blaine Hill, a MS CS grad student at UIUC and researcher advised by Dr. Hanghang Tong at the iDEA Lab. My thesis is on reinforcement learning (RL) with diffusion applied towards knowledge graph reasoning. Previously, I was a part of UNC-Chapel Hill's LUPA Lab under Dr. Junier Oliva, where I worked on RL w/Active Feature Acquiring surrogate models to optimize decision policies from features. I graduated with both a Bachelor's of Science in Computer Science and in Statistics and Analytics from UNC-Chapel Hill in 2022.

I am interested in both internships and full-time roles starting for the Summer of 2024. If you'd like to contact me, please message me via [my LinkedIn](https://www.linkedin.com/in/blainehill2001/).

---
---

### 1. Conspectus

In this project, we primarily focus on developing a Venue Arbitrage Strategy considering two markets, namely `IEX` and `NASDAQ`. We study the effect of latency on the profitability and performance of our strategy. Within our strategy, we explicitly track our current exposure through a custom state machine, dynamically update our stop loss & take profit levels, and decide on specific actions (including the order execution strategy) based on the current market state and our current holdings. We also compare our Venue Arbitrage Strategy with two simple baseline strategies based on the Simple Moving Average (SMA) and Relative Strength Index (RSI). We showcase our analysis of intraday market data and present a clustering algorithm to aggregate tickers showing well-correlated intraday movements into appropriately sized clusters. Within a trading strategy, one could use signals (market events) from other within-cluster tickers to speculate about the movements of a primary ticker under consideration. We develop a customized test bench for evaluating and visualizing the performance of various predictive models including simple models such as Linear Regression, Ensemble-based models such as Random Forest Regressor, and Deep Neural Networks. Our test bench relies on a market event generator which in turn is used to create custom data point plus data batch generators. We show various examples of how to use this setup for various training paradigms including an online learning system and an event-driven training with an early stopping scheme. A notable example that we include in this report is predicting the future Best Bids and Best Offers for instruments along with confidence intervals around the prediction. Lastly, we analyze the performance of a multivariate Kalman Filter for predicting the next BBO within our event-driven prediction setup. Throughout this report, we will point the reader to the relevant source code files.

---

### 2. Venue Arbitrage Strategy

At its core, a venue arbitrage strategy aims to exploit market inefficiencies by taking advantage of the price difference of a particular instrument trading at different venues. For example, if `SPY` is trading at $250.64 on `IEX` and $250.68 on `NASDAQ`, one could simultaneously buy `SPY` on `IEX` and sell on `NASDAQ` making a profit of $0.04 per unit (ignoring order fees). The above example simplifies the best bid and best ask for `SPY` on the two exchanges to a single 'price' (usually the arithmetic mean of the best bid and the best ask, sometimes referred to as the mid-price). From the lens of market microstructure, a venue arbitrage opportunity would arise when the best bid on one exchange would exceed the best ask on another by at least one cent. Note that such opportunities are rare and momentary. They typically last for the order of a few nanoseconds. To take advantage of such an opportunity, a firm needs to first identify that such an opportunity exists (before the National Best Bid and Offer (NBBO) is updated) and secondly, send appropriate orders to the two exchanges before all other competitors. The orders sent would account for the total available volume for such a trade.

For any robust strategy, we need to explicitly maintain our current holdings in a finite state machine to make decisions on when to exit our positions. Note that we might witness an arbitrage opportunity and subsequently create and send appropriate orders. However, these orders may not all be filled (completely). Thus, we track all of our fills to maintain our current exposure (whether we are net long or net short, how much volume we are currently holding, at what average value did we purchase our net volume, etc.). Based on our maintained position, we calculate and set our dynamic stop loss and a price threshold at which we try to exit our trades and take profit. We create a state machine capable of maintaining such positions and generating appropriate decisions (what kind of orders to send as well as an order execution strategy). We discuss this further in the subsection on Finite State Machine.

### 2.1. Arbitrage Opportunity

After every market event, we compare the order books for `SPY` on `NASDAQ` and `IEX`. If the best bid on `IEX` exceeds the best ask on `NASDAQ` or vice-versa, we create appropriate orders and send them to the appropriate exchanges. We have the following pseudo-code for the same.

```python
best_bid = max(best bid on NASDAQ, best bid on IEX)
best_ask = min(best ask on NASDAQ, best ask on IEX)

if(best_bid > best_ask and best bid on IEX == best_bid and best ask on NASDAQ == best_ask)
then
{
    size = min(best bid volume on IEX, best ask volume on NASDAQ)
    order_A = ['BID', best_ask, size, 'NASDAQ']
    order_B = ['ASK', best_bid, size, 'IEX']
    Send order_A and order_B
}

else if(best_bid > best_ask and best bid on NASDAQ == best_bid and best ask on IEX == best_ask)
then
{
    size = min(best ask volume on IEX, best bid volume on NASDAQ)
    order_A = ['BID', best_ask, size, 'IEX']
    order_B = ['ASK', best_bid, size, 'NASDAQ']
    Send order_A and order_B
}
```

Refer to `./venue_arb/strategy_funcs.py` for a working version of the same.

### 2.2. Exploiting the lead-lag relationship between IEX and NASDAQ

We also add trading conditions based on the following hypothesis. Suppose `SPY` is trading at the same Best Bid and Best Offer on both `IEX` and `NASDAQ` and the Best Offer exceeds the Best Bid by 1 cent (trading at 1 tick wide). If within a few nanoseconds, both the Best Bid and the Best Offer on `NASDAQ` go up by at least one cent (while still trading at one tick wide on `NASDAQ`) while there is no change on `IEX`, we expect `IEX` to catch up to this change on `NASDAQ`'s BBO and buy `SPY` (go long) on `IEX`. Similarly, if the BBO drops on `NASDAQ`, we short it on `IEX`.

We have the following pseudo-code for the same. For details, refer to `./venue_arb/strategy_funcs.py`.

```python
NASDAQ_width = best ask on NASDAQ - best bid on NASDAQ
IEX_width = best ask on IEX - best bid on IEX
one_tick = 0.01

if(NASDAQ_width == one_tick and IEX_width == one_tick and best bid on NASDAQ > best bid on IEX)
{
    order = ['BID', best ask on IEX, best ask volume on IEX, 'IEX']
    // Go Long on IEX
    Send order
}

else if(NASDAQ_width == one_tick and IEX_width == one_tick and best bid on NASDAQ < best bid on IEX)
then
{
    order = ['ASK', best bid on IEX, best bid volume on IEX, 'IEX']
    // Go Short on IEX
    Send order
}
```

### 2.3. Finite State Machine

In this section, we describe how we maintain a Finite State machine to track our current position in the market. We utilize this state machine and the current market information to appropriately set the price level for a dynamic stop loss. Note that within our Venue Arbitrage strategy, we set the take profit level to be 1 cent above (or below if we are in a short position) the per unit price at which we entered the trade. However, our state machine design is independent of the strategy and is capable of handling any take profit value. Using this setup, we decide on when to exit trades (if we hit a stop loss or a take profit) and appropriately create an order execution strategy to achieve the same.

#### 2.3.1. Toy State Machine for a single position

We first experiment with a simple state machine that only lets one be in one position at a time. The position could either be long or short. Here, we primarily track the instrument being traded, the side (whether our position is long or short), the size (how much volume do we own), and the price at which we entered the trade. We also maintain parameters for a stop loss fraction (to set a dynamic stop loss) and a take profit level. Here, the state machine has two basic types: `reset` and `hold`. We initially start at the `reset` and get into the `hold` state once we get into a position. This is restrictive since it does not inherently allow us to get into multiple positions at once. However, note that one could maintain multiple such state machines to track all of their positions in the market individually.

We have an action for `get_into_pos` with parameters for the price, instrument, size, side, net profit, and stop loss percentage. After every event that updates either the Best Bid or the Best Offer, we have an action to set the stop loss: `set_stop_loss`, and an action to check whether we can take a profit: `is_at_profit`. Lastly, we have an action to `exit_position` which moves the state machine into the `reset` state and updates all of its internal parameters appropriately.

For the exact operation of this toy state machine setup, look at `./venue_arb/trade_states.py`. As an example of how the state machine operates, refer to the testing script `./venue_arb/test_toy_trade_states.py`. One can set the trial style to be either `long` or `short`. The trial reads in an input sequence of the Best Bids and Best Asks. If the state is `reset`, it gets into a position while if the state is `hold`, it simply exits the current position after checking whether it is at a profit or a loss. One could utilize this testing script to check whether the state transition is taking place as expected.

#### 2.3.2. State Machine Tracking Multiple Positions

We create a more robust state machine capable of capturing our current aggregated position in the market in a scene where we have entered into multiple long and short positions of varying sizes. Such a state machine is specific to the instrument being traded (in our case, `SPY`). We also set the stop loss percentage to dynamically update the stop loss level. We also set the net profit at which we'd attempt to exit the position and take profit. Here, we don't explicitly keep track of named states (`reset` or `hold`). Intuitively, the `reset` state corresponds to the situation when we are not holding any assets (neither net long nor net short). The state machine captures states for the `size`: the volume of instruments we are currently holding with a sign to indicate whether we are net long or net short), the `exposure`: which tracks the total amount we paid (not considering trading fees currently) to get into such a position, and the `absolute_exposure_per_unit`: which is the average price for a single instrument in our holdings. Tracking these variables is important for multiple reasons. First, our placed orders might be filled at a different price than what we posted, or at multiple different price levels. Also, we might be in a situation where we have gone long at multiple price levels with varying sizes or we have gotten ourselves into multiple long and short positions. Accurately tracking our current exposure, whether we are net long or net short, and the absolute per unit price for the instrument in our holdings is crucial for precisely setting stop losses and take profit levels.

We have the following condition `get_direction` to check whether we are net long or net short.

```python
if size > 0:
    We are 'long'
else if size < 0:
    We are 'short'
else:
    We are in a 'reset' state
```

We maintain parameters for `long_take_profit_level` and `long_curr_stop_loss_level` when we are net long, and similarly for `short_take_profit_level`, `short_curr_stop_loss_level` when we are net short. Based on these parameters, which are updated after every market event, we have the following function for setting stop losses.

```python
def set_stop_loss(self, best_bid, best_ask):
    direction = self.get_direction()
    if direction == 'none':
        return
    elif direction == 'long':
        if self.long_curr_stop_loss_level is None:
            loss_level = self.absolute_exposure_per_unit - self.absolute_exposure_per_unit * self.stop_loss_fraction
            self.long_curr_stop_loss_level = loss_level
        else:
            loss_level = best_bid - best_bid * self.stop_loss_fraction
            self.long_curr_stop_loss_level = max(self.long_curr_stop_loss_level, loss_level)
    elif direction == 'short':
        if self.short_curr_stop_loss_level is None:
            loss_level = self.absolute_exposure_per_unit + self.absolute_exposure_per_unit * self.stop_loss_fraction
            self.short_curr_stop_loss_level = loss_level
        else:
            loss_level = best_ask + best_ask * self.stop_loss_fraction
            self.short_curr_stop_loss_level = min(self.short_curr_stop_loss_level, loss_level)
```

Based on the currently set stop loss level and conditioned on whether we are currently net long or net short, we check whether we have hit a stop loss as follows.

```python
def is_at_stop_loss(self, best_bid, best_ask):
    direction = self.get_direction()
    if direction == 'none':
        return
    elif direction == 'long':
        return (best_bid < self.long_curr_stop_loss_level)
    elif direction == 'short':
        return (best_ask > self.short_curr_stop_loss_level)
```

Similarly, after every market event, we compute our take profit level conditioned on whether we are net long or net short and appropriately check whether we should try to exit our position and take a profit.

```python
def is_at_profit(self, best_bid, best_ask):
        direction = self.get_direction()
        if direction == 'none':
            return
        if direction == 'long':
            take_profit_level = self.absolute_exposure_per_unit + self.per_unit_profit
            return (best_bid >= take_profit_level)
        elif direction == 'short':
            take_profit_level = self.absolute_exposure_per_unit - self.per_unit_profit
            return (best_ask <= take_profit_level)
```

To accurately update our position, we look at the order messages to check whether our order was filled or not, and if yes, then at what price and volume. These correspond to the on-order update messages on Strategy Studio. We appropriately encode this information into two variables: `price`: the price at which our order was filled, and the `size`: the volume at which our order was filled with a sign telling whether the fill was on the ask or the bid size. Based on these values, we update our state machine as follows.

```python
def update_pos(self, price, size):
    # size is positive for filled on long side and negative for filled on short side
    self.exposure += (price * size)
    self.size += size
    self.set_exposure_per_unit()

    direction = self.get_direction()
    if direction == 'none':
        self.reset_state()
    elif direction == 'long':
        self.reset_short_side()
    elif direction == 'short':
        self.reset_long_side()
    return
```

In the above function, we make a call to update the current absolute exposure per unit of instrument which is computed within the following function.

```python
def set_exposure_per_unit(self):
    if self.size == 0:
        self.exposure = 0
        self.absolute_exposure_per_unit = 0
        return
  
    self.absolute_exposure_per_unit = abs(self.exposure / self.size)
    return
```

Lastly, if we are already in a position (either net long or net short) and we have hit either a stop loss or a take profit level, we need to appropriately create market actions (orders) that would allow us to exit our current position. We achieve this with our state machine as well making use of the following function to generate the appropriate market actions.

```python
def get_action(self, best_bid, best_ask):
    direction = self.get_direction()

    if direction == 'long':
        if self.is_at_stop_loss(best_bid, best_ask):
            return ['ASK', best_bid, abs(self.size), 'SL']
        elif self.is_at_profit(best_bid, best_ask):
            return ['ASK', best_bid, abs(self.size), 'TP']
        else:
            return ['', '', '', '']

    elif direction == 'short':
        if self.is_at_stop_loss(best_bid, best_ask):
            return ['BID', best_ask, abs(self.size), 'SL']
       elif self.is_at_profit(best_bid, best_ask):
            return ['BID', best_ask, abs(self.size), 'TP']
        else:
            return ['', '', '', '']
```

In the encoding for the order, '`SL`' indicates a Stop Loss, and '`TP`' indicates a Take Profit. Note that we only send orders at either the current best bid or the best ask and not at price levels that we compute internally. This ensures that (1) we take advantage of the best price available on the publicly displayed order book, and (2) that we do not inadvertently send sub-penny orders (which would be judged as an error). The created order encoding is then appropriately converted into the order format required by Strategy Studio.

We refer the reader to `./venue_arb/trade_state_multi.py` for further details on how the state machine functions. We also include a test script: `./venue_arb/test_runner.py` for running small experiments based on an input sequence of best bids and best asks (assuming constant volume). For every event, we demonstrate how to call specific actions such as `process_event` to process the latest BBO update and `update_pos` to update the current position based on a filled order message. After every event, we view the current parameters within the state machine to ensure that it functions as expected. Similar to `./venue_arb/test_toy_trade_states.py`, we can set the `trial_style` to `long` or `short`. This script could be used to run quick unit tests on the state machine updates.

### 2.4. State Machine-based Event Processing within the Venue Arbitrage Strategy

We utilize the above-discussed state machine to maintain positions within our Venue Arbitrage Strategy. We regard every market event that updates the order book, and more specifically the Best Bid or the Best Offer on either NASDAQ or on IEX. After every such event, we update the internal state of our state machine using the state machine's `process_event` action. We also track the current event timestamp to make sure that we do not get into a position after a certain threshold time (say 15:00 ET). To ensure the same, we convert the UTC timestamp (used by Strategy Studio) to ET and only check whether we should get into a new position if the ET timestamp is before our selected threshold time. This may be disabled to trade past the exchanges' normal trading hours.

The following function extracts the specific fields required by the state machine and performs two key steps. First, it checks whether we are already in a position and if we are, it further checks whether we need to exit that position (because we have hit a take profit or a stop loss). Secondly, it checks whether any of the conditions for getting into trade were valid and whether the necessary orders were appropriately created and sent to the proper exchanges. In the functions within this section, `sm` denotes the created state machine object.

```python
def proc_event(self, IEX_info, NASDAQ_info, timestamp):
    # IEX_info: [[Best Bid Price, Best Bid Volume], [Best Ask Price, Best Ask Volume]] on IEX
    # Similarly, NASDAQ_info: [[Best Bid Price, Best Bid Volume], [Best Ask Price, Best Ask Volume]] on NASDAQ
  
    best_bid = max(IEX_info[0][0], NASDAQ_info[0][0])
    best_ask = min(IEX_info[1][0], NASDAQ_info[1][0])

    self.sm.process_event(best_bid, best_ask)

    if self.sm.size != 0:
        # Already in a position
        self.sm.process_event(best_bid, best_ask)
        action = self.sm.get_action(best_bid, best_ask)
      
        if action[0] != '':
            print('Action: ', action)
            self.execute_action(action, IEX_info, NASDAQ_info)
  
    # Should we get into a position based on our Strategy?
    self.check_trade_and_place_orders(best_bid, best_ask, IEX_info, NASDAQ_info, timestamp)
    return
```

Next, we look at the `check_trade_and_place_orders` function which implements the core conditions of our Venue Arbitrage Strategy described above. The pseudo-code for these conditions can be found in the subsections for 'Arbitrage Opportunity' and 'Exploiting the lead-lag relationship between IEX and NASDAQ'.

```python
def check_trade_and_place_orders(self, best_bid, best_ask, IEX_info, NASDAQ_info, timestamp):
    # Checks for possible trades and create appropriate orders

    NASDAQ_width = NASDAQ_info[1][0] - NASDAQ_info[0][0]
    IEX_width = IEX_info[1][0] - IEX_info[0][0]
    one_tick = 0.01

    if self.get_ET_time(timestamp) < self.stop_trading_time:
        return False
  
    elif best_bid > best_ask and IEX_info[0][0] == best_bid and NASDAQ_info[1][0] == best_ask:
        size = min(IEX_info[0][1], NASDAQ_info[1][1])
        order_A = ['BID', best_ask, size, 'NASDAQ']
        order_B = ['ASK', best_bid, size, 'IEX']
        # Send order_A and order_B
        return True

    elif best_bid > best_ask and NASDAQ_info[0][0] == best_bid and IEX_info[1][0] == best_ask:
        size = min(IEX_info[1][1], NASDAQ_info[0][1])
        order_A = ['BID', best_ask, size, 'IEX']
        order_B = ['ASK', best_bid, size, 'NASDAQ']
        # Send order_A and order_B
        return True
  
    elif NASDAQ_width == one_tick and IEX_width == one_tick and NASDAQ_info[0][0] > IEX_info[0][0]:
        # Go Long on IEX
        order = ['BID', IEX_info[1][0], IEX_info[1][1], 'IEX']
        # Send order
        return True

    elif NASDAQ_width == one_tick and IEX_width == one_tick and NASDAQ_info[0][0] < IEX_info[0][0]:
        # Go Short on IEX
        order = ['ASK', IEX_info[0][0], IEX_info[0][1], 'IEX']
        # Send order
        return True
  
    else:
        return False
```

We now look at the function that generates an appropriate order execution strategy based on the action suggested by the state machine `sm`'s `get_action` method. These actions are required to exit from a current position and are created when we hit either a stop loss or a take profit. Based on the action, we infer what kind of order we have to send (whether a bid or an ask). We then look at the order books on both `NASDAQ` and `IEX` and decide on the particular exchange to which we would route the order. Lastly, we consider the amount of volume that we wish to trade (either bid or ask) and the available volumes on both `IEX` and `NASDAQ`. Based on these parameters, we create appropriate BIDs and ASKs with specific volumes and route them to the appropriate exchanges.

```python
def execute_action(self, action, IEX_info, NASDAQ_info):
    # Actions to exit from the current position (Either a Take Profit (TP) or a Stop Loss (SL))
    # Note: The choice of sending orders first to IEX and then to NASDAQ is arbitrary
    # IEX_info: [[Best Bid Price, Best Bid Volume], [Best Ask Price, Best Ask Volume]] on IEX
    # Similarly, NASDAQ_info: [[Best Bid Price, Best Bid Volume], [Best Ask Price, Best Ask Volume]] on NASDAQ
  
    price = action[1]
    size = action[2]
    side = action[0]

    if side == 'BID':
        if IEX_info[1][0] == price and NASDAQ_info[1][0] == price:
            # Send Order to IEX first, if volume remains, send another order to NASDAQ
            orders = [['BID', price, min(size, IEX_info[1][1]), 'IEX']]
            size -= min(size, IEX_info[1][1])
            if size > 0:
                orders.append(['BID', price, min(size, NASDAQ_info[1][1]), 'NASDAQ'])

        elif IEX_info[1][0] == price:
            # Send Order to IEX
            orders = [['BID', price, min(size, IEX_info[1][1]), 'IEX']]

        elif NASDAQ_info[1][0] == price:
            # Send Order to NASDAQ
            orders = [['BID', price, min(size, NASDAQ_info[1][1]), 'NASDAQ']]

    elif side == 'ASK':
        if IEX_info[0][0] == price and NASDAQ_info[0][0] == price:
            # Send Order to IEX first, if volume remains, send another order to NASDAQ
            orders = [['ASK', price, min(size, IEX_info[0][1]), 'IEX']]
            size -= min(size, IEX_info[0][1])
            if size > 0:
                orders.append(['ASK', price, min(size, NASDAQ_info[0][1]), 'NASDAQ'])

        elif IEX_info[0][0] == price:
            # Send Order to IEX
            orders = [['ASK', price, min(size, IEX_info[0][1]), 'IEX']]

        elif NASDAQ_info[0][0] == price:
            # Send Order to NASDAQ
            orders = [['ASK', price, min(size, NASDAQ_info[0][1]), 'NASDAQ']]
  
    # for order in orders:
    #   Send order to exchange
  
    return
```

Finally, we have an action to process messages related to whether our placed order was filled or not. We check the price and the volume at which the order was filled, consider what kind of order it was in the first place (whether a bid or an ask) and appropriately update our state machine by calling its `update_pos` action.

```python
def proc_order_update(self, fill_price, order_type, volume):
    # order_type is either 'ASK' or 'BID' (the type of order that was filled)
    if order_type == 'BID':
        direction = 1
    elif order_type == 'ASK':
        direction = -1
    self.sm.update_pos(fill_price, volume * direction)
```

We refer the reader to `./venue_arb/strategy_funcs.py` for further details. The relevant code for our state machine that maintains our current market position can be found in `./venue_arb/trade_state_multi.py`. The working version of the strategy can be found in `./strategy_studio/strategies/VenueArbStrategy.py`.

### 2.5. Performance of the Venue Arbitrage Strategy

In this section, we analyze the performance of our Venue Arbitrage Strategy with a set latency value of 100 nanoseconds. In the following figure, we plot the change in the cumulative Net Profit (PnL) with change in time, for specific trading days.

![PnL for Venue Arbitrage Strategy at latency value of 100 nanoseconds](figs/VenueArb_PnL.svg)

Examining the above Profit and Loss (PnL) graph reveals that such arbitrage opportunities for `SPY` considering `IEX` and `NASDAQ` are indeed rare. We observe that on specific dates such as `12-30-2019`, `08-30-2019`, and `07-30-2019`, there are only a handful of such arbitrage opportunities. Due to the scarcity of opportunities, the strategy doesn't generate huge profits. However, at a latency of 100 nanoseconds, our Venue Arbitrage Strategy is highly capable of exploiting market inefficiencies. We also observe that we do not generate any losses. Our state machine explicitly tracks all fills and is aware of when to exit positions. Note here that though we send two orders, one for BID and one for ASK at the same size while performing venue arbitrage, both of them might not get filled completely. This would put us in either a net long or a net short position. Thus, it is of paramount importance to consider our position while making trading decisions.

In the following figure, we view the win rate for our strategy (with a latency of 100 nanoseconds).

<div align="center">
  <img src="figs/VenueArb_date_vs_Win%20Rate_latency_0.0001.svg" width="70%">
</div>

We observe that the win rate remains 100% for all dates. This demonstrates the effectiveness of the Venue Arbitrage strategy with the underlying State Machine. The strategy shows a reliable ability to profit when opportunities arise, albeit with relatively modest gains. Also, note that the cost of setting up and maintaining the technological infrastructure for running a venue arbitrage strategy effectively (with very low latency) is quite high. Thus, the generated daily profits from `SPY` might not offset those costs. However, we must also note that once such an infrastructure is set up, a firm would not be trading only `SPY`. They would trade multiple assets, each of which could potentially generate small profits throughout each trading day. Also, note that we solely consider the publicly visible order book. Firms would typically exploit channels to explore whether other price levels exist for an instrument within the Best Bid and Best Offer (applicable to assets that do not always trade at one tick wide) and utilize the same within an arbitrage strategy.

### 2.6. How Latency Affects Profitability of our Venue Arbitrage Strategy

#### PnL versus Latency

We conducted extensive testing across a latency spectrum ranging from 0 to 100 milliseconds, including intervals at 1e-6, 1e-3, and 0.005 milliseconds. This comprehensive approach aimed to gain insights into the performance of the strategy under varying latency conditions. Specifically, we sought to identify latencies where the strategy exhibits diminished effectiveness or ceases to perform optimally. The finer granularity in testing, particularly with the 0.005ms interval from 1e-3 to 1e-1 milliseconds, allows for a more nuanced understanding of how latency impacts the strategy's behavior.

In the following figure, we plot the change in our cumulative PnL against the underlying latency for 8 specific dates.

![PnL vs. Latency](figs/VenueArb_latency_vs_Final%20PnL_log.svg)

We had expected the losses to monotonically increase with the increase in latency. Based on the plots, however, we see that as the latency goes up, we may get to either side of the profitability spectrum. We sometimes lose a ton of money, while at other times, we gain a substantial amount. What we infer based on this, is that the unpredictability of the performance of a venue arbitrage strategy goes up with increasing latency while the determinism of the outcome decreases. Thus, the higher the latency of one's infrastructure, the riskier it is to run a venue arbitrage strategy.

Intuitively, at very high latencies such as 100 milliseconds, the arbitrage opportunity would disappear by the time our sent orders are processed by an exchange's matching engine but some of these placed orders (placed at the time of noticing the opportunity) would get filled putting us into a position. In the following figure, we plot the daily cumulative PnL at a latency of 100 ms. We see that the performance of the strategy is completely unpredictable and the outcomes are undeterministic. We notice high profits made on certain days and this might indeed be tempting. However, these profits are based on pure dumb luck and the underlying risk is exceptionally high.

![Venue Arbitrage PnL under 100ms latency](figs/VenueArb_PnL_latency_100.svg)

#### Win Rate versus Latency

In the following figure, we plot the change in Win Rate against the underlying latency.

![Win Rate vs. Latency](figs/VenueArb_latency_vs_Win%20Rate_log.svg)

Expectedly, the graph shows a negative correlation between latency and win rate. In other words, as latency increases, the win rate decreases. The lower the latency of a firm, the higher its chances of getting its orders filled at the targeted prices.

#### Varying the latency between 0.001 ms to 0.1 ms

Based on the above plots, it looks as if when the latency is increased to values close to 0.1 ms, the PnL starts to fluctuate greatly. Thus, we zoom in on the specific PnLs and Win Rates with latencies varying between 0.001 to 0.1 ms. In the first plot, we track the change in the final PnL against latency values ranging between 0.001 and 0.1 ms.

![Final PnL vs. Latency 0.001 to 0.1](figs/VenueArb_latency_vs_FinalPnL_0.1.svg)

In the next plot, we track the Win Rate against latency values (ranging between 0.001 and 0.1 ms).

![Win Rate vs. Latency 0.001 to 0.1](figs/VenueArb_latency_vs_WinRate_0.1.svg)

Based on the above figures, we observe a substantial decline in the win rate when the latency exceeds 0.01 milliseconds. We conclude that the outcomes of a venue arbitrage strategy would be prohibitively non-deterministic if the latency exceeds a few microseconds.

### 2.7. Dataset Creation

In this subsection, we briefly describe how we created various datasets, both for backtesting our strategies and for data analysis.

#### 2.7.1. Parsing IEX data into the Strategy Studio Format

`IEX`, or the Investors Exchange, is a U.S.-based stock exchange that operates as an Alternative Trading System (ATS) and aims to provide a fair and transparent marketplace. `IEX` is known for its unique features designed to address perceived issues in the existing stock exchanges. We utilized the [IEX DownloaderParser](https://gitlab.engr.illinois.edu/shared_code/iexdownloaderparser/-/tree/main?ref_type=heads), a tool provided by Professor Lariviere, to retrieve and process the `IEX` data. We wrote custom scripts for our specific use cases. These can be found in `./data/IEXdownloaderparser`. We list the functions for each of these scripts here.

- `download_and_parse.sh`: Download and parse raw data for each date and subsequently delete the `.pcap` file post-parsing to save disk space.
- `download_days.sh`: Download raw data for specific dates.
- `download_from_to.sh`: Download raw data between a start date and an end date.
- `parse_batch.sh`: Parse raw data in batches safeguarding against network errors.
- `parse_days.sh`: Parse data for specific dates.
- `parse_from_to.sh`: Parse raw data between a start date and an end date.

#### 2.7.2. Generate ticker files with both IEX and NASDAQ data

We create files containing text tick (in the format required by Strategy Studio) data for market events for the ticker `SPY` on both `IEX` and `NASDAQ`. We fetched freely available `NASDAQ` market events data for 8 days. We then adapted Professor Lariviere's `IEX` data parser to generate text tick files (and CSVs containing order book updates and trade events) containing `SPY` events for those specific dates.

In the first step, we convert `NASDAQ` market events data into the text tick format required by Strategy Studio. Since NASDAQ's data is in the market by order format, we parse raw `NASDAQ` book update events into the `D` type ticks required by Strategy Studio. The necessary conversion functions can be found under `./utils/Nasdaq Parsing/convert_messages_to_ss.ipynb`.

As an alternate technique, we also generate `P`-type (Book Update by Price) events from the `NASDAQ` data. For this, we first use a publicly available `NASDAQ` order book parser: https://github.com/martinobdl/ITCH. This reconstructs the Limit Order Book from data feed messages issued by `NASDAQ` according to the ITCH 50 data protocol. We then process the order book update lines in sequence to generate various `P` type tick lines for Strategy Studio. We explicitly capture each change between successive order book updates and appropriately create the necessary order book update by price events. In addition, we also consider the raw `NASDAQ` messages to generate the appropriate trade messages. We refer the reader to `./data_handler/NASDAQ_tick_creator.ipynb/` for specifics. However, note that this process is lossy and not recommended in general. Since `NASDAQ` by default provides market-by-order messages, it is sensible to convert them into `D` and `T`-type Strategy Studio ticks (as we have mentioned above). Converting the market by order data to book updates and then to price level updates (type `P` in Strategy Studio) would lose price levels beyond the depth of the parsed book updates (in our case, depth was `5`). But we included this conversion setup as a fallback option just in case we ran into unforeseen issues while trying to use two different order types, namely `D` and `P` (`IEX` ticks are of type `P` by default), for book updates within a single text tick file while backtesting on Strategy Studio.

In the second step, we merge the `IEX` and `NASDAQ` ticks into a single text tick file. We first sort the ticks within each tick file based on their source time. We then adopt a two-pointer approach to merge the sorted ticks based on their source time. We share a small snippet (15 lines) from a merged text tick file containing `P`-type ticks from both `IEX` and `NASDAQ`.

```python
2019-07-30 12:33:10.790477257,2019-07-30 12:33:10.790477257,63594,P,NASDAQ,2,300.32,1700,,,,
2019-07-30 12:33:10.791659008,2019-07-30 12:33:10.791652438,41754,P,IEX,1,300.260000,500,,,,0
2019-07-30 12:33:10.800877056,2019-07-30 12:33:10.800892747,41755,P,IEX,2,300.320000,500,,,,0
2019-07-30 12:33:11.041404928,2019-07-30 12:33:11.041417114,41756,P,IEX,2,300.340000,500,,,,0
2019-07-30 12:33:11.041512960,2019-07-30 12:33:11.041530489,41757,P,IEX,1,300.240000,500,,,,0
2019-07-30 12:33:11.044027493,2019-07-30 12:33:11.044027493,63595,P,NASDAQ,2,300.34,600,,,,
2019-07-30 12:33:11.044029917,2019-07-30 12:33:11.044029917,63596,P,NASDAQ,1,300.24,600,,,,
2019-07-30 12:33:11.306758918,2019-07-30 12:33:11.306758918,63597,P,NASDAQ,2,300.35,0,,,,
2019-07-30 12:33:11.306758918,2019-07-30 12:33:11.306758918,63598,P,NASDAQ,2,300.3,500,,,,
2019-07-30 12:33:11.306787716,2019-07-30 12:33:11.306787716,63599,P,NASDAQ,2,300.31,1000,,,,
2019-07-30 12:33:11.306789926,2019-07-30 12:33:11.306789926,63600,P,NASDAQ,2,300.31,500,,,,
2019-07-30 12:33:11.306808161,2019-07-30 12:33:11.306808161,63601,P,NASDAQ,2,300.32,1600,,,,
2019-07-30 12:33:11.341614080,2019-07-30 12:33:11.341623476,41758,P,IEX,1,300.220000,500,,,,0
2019-07-30 12:33:11.350802944,2019-07-30 12:33:11.350819490,41759,P,IEX,2,300.370000,500,,,,0
2019-07-30 12:33:12.699158791,2019-07-30 12:33:12.699158791,63602,P,NASDAQ,2,300.3,0,,,,
```

We refer the reader to `./data_handler/merge_venues.ipynb/` and `./data_handler/tick_maker_script.py` for details.

### 2.8. Performance of Baseline Strategies

In this section, we compare the performance of our Venue Arbitrage strategy to two naive baselines, one based on Simple Moving Averages, and another based on the Relative Strength Index. Note that these strategies are naive since they do not explicitly maintain the current holdings in a state machine. Neither do they track whether their sent orders were filled or not. We observe that such simple strategies are incapable of generating any profits on frequently traded instruments such as `SPY`.

#### 2.8.1. Simple Moving Average (SMA)

Simple Moving Average (SMA) is a basic technical analysis indicator used heavily in finance and statistics. The Simple Moving Average is calculated by adding up a set of prices for a financial instrument (e.g., stock prices) over a specified period and then dividing the same by the number of data points. Intuitively, the resulting value smooths out short-term fluctuations and highlights longer-term trends.

With multi-market datasets, we look at the BBO of the aggregated order book to calculate the Simple Moving Average. We appropriately route the order to the exchange offering the better price between `IEX` and `NASDAQ`.

This strategy is implemented at `./strategy_studio/strategies/SMAStrategy_multi.py`.

##### Result and analysis

In this section, we analyze the performance of our Simple Moving Average Strategy with a latency value of 100 nanoseconds. In the following figure, we plot the change in cumulative PnL against time for the specific dates. As expected, we observe significant losses and witness a great degree of unpredictability.

![SMAmulti PnL](figs/SMAmulti_PnL.svg)

We also look at win rates for each of the above dates. We observe that the win rate roughly ranges between 60% and 70%.

<div align="center">
  <img src="figs/SMAMulti_date_vs_Win%20Rate_latency_0.0001.svg" width="70%">
</div>

#### 2.8.2. Relative Strength Index (RSI)

The Relative Strength Index (RSI) stands as a widely employed momentum oscillator in technical analysis, designed to identify potential overbought or oversold conditions in a trading instrument. By calculating average gains and losses over a specified period, the RSI generates values within the 0 to 100 range. Readings surpassing 70 are typically interpreted as overbought, signaling a potential sell opportunity, while readings below 30 indicate oversold conditions, suggesting a potential buying opportunity.

Within our strategy, we utilize multi-market datasets and analyze the aggregated Best Bid and Offer to compute the RSI. Similar to our baseline SMA strategy, we route our orders to the exchange offering the better price between `IEX` and `NASDAQ`.

The implementation for this strategy can be found at `./strategy_studio/strategies/RSIStrategy.py`.

##### Results and analysis

In this section, we analyze the performance of our Relative Strength Index Strategy with a latency value of 100 nanoseconds. In the following figure, we plot the change in cumulative PnL against time for specific dates. Again, expectedly, we observe significant losses and a high degree of uncertainty.

![RSI PnL](figs/RSI_PnL.svg)

In the next figure, we look at win rates for each of the above dates. We observe that the win rate roughly ranges between 60% and 80%, with `01-30-2019` being the only exception.

<div align="center">
  <img src="figs/RSI_date_vs_Win%20Rate_latency_0.0001.svg" width="70%">
</div>

---

### 3. Backtesting on Strategy Studio

We leverage the power of the `RCM-X Strategy Studio` for our backtesting endeavors. All of our necessary code implementing specific strategies as well as our custom scripts can be found in the `./strategy_studio/` directory.

We implemented our primary strategy (Venue Arbitrage with a custom State Machine) and our baseline strategies in `Python`. For seamless integration with `Strategy Studio`, we employed `Pybind11`. `Pybind11` is a lightweight, header-only library designed to facilitate smooth interoperability between `C++` and `Python`. It streamlines the process of exposing `C++` code to `Python` and vice-versa.

We built upon the foundation laid within the following module: [PybindStrategy](https://gitlab-beta.engr.illinois.edu/ie421_high_frequency_trading_spring_2023/ie421_hft_spring_2023_group_02/group_02_project/-/tree/main/PybindStrategy?ref_type=heads), which was developed as part of a group project for Spring 2023's IE 421 (High-Frequency Trading) class. We extended certain capabilities of this module by introducing some essential bindings. Specifically, we enhanced the functionality to retrieve the individual Best Bid and Best Offer (BBO) for multiple market centers within `PybindStrategy/includes/instrument.h`.

To facilitate a streamlined backtesting process, we crafted `bash` scripts allowing users to execute backtests on different dates and latencies with a single command. This not only simplifies the testing procedure but also provides users with the flexibility to link various data directories as sources for tick text data. This modular approach enhances the adaptability of our strategy, allowing for thorough backtesting and optimization. Our helper scripts, including `build_strategy.sh`, `run_backtest.sh`, `cleanup_strategy.sh`, and `rename_output.sh`, can be found in `./strategy_studio/scripts/`.

---

### 4. Analysis of intraday market data

In this section, we briefly outline our data processing and analysis setup. In our initial data analysis phase, we developed a pipeline to analyze text tick data files in the format required by Strategy Studio having information on book updates and trades. Our developed `python` package for the same can be found under `./proc_analysis/proc_funcs.py`. We created a mapping database from individual tickers to the set of all trading days for which there exists data for that particular ticker in a collected text tick data repository. We analyzed both the trades with respective volumes for the ticker occurring throughout the trading day as well as the order book updates, primarily the changes to the best bid and the best offer. We processed the raw data into a customized time series format allowing easy aggregation and manipulation. We performed some initial visualizations to view the fluctuations in trade prices for an instrument. We computed simple univariate statistics such as the central tendencies and range of prices throughout specific periods. For example, the following figure shows the raw trade prices against the time elapsed for SPY on January 5, 2023. Note that in this figure and others in subsequent sections plotting the change in trade prices, the translucent circles indicate the trade size. Also, even though the x-axis shows the time elapsed scaled in seconds, all of our visualizations consider the nanosecond-level timestamp. We refer the reader to `./proc_analysis/time_series_builder.ipynb` for details.

<div align="center">
  <img src="https://gitlab.engr.illinois.edu/fin556_algo_market_micro_fall_2023/fin556_algo_fall_2023_group_03/group_03_project/-/raw/main/figs/SPY_trade_vis.svg?ref_type=heads" width="80%">
</div>

---

### 5. Spectral Ticker Clustering

We focused on developing algorithms to cluster tickers based on the correlation in their movements. Having groups of correlated tickers could be used downstream for developing trading strategies where the model would speculate on the movement of a certain ticker based on the movements of other tickers within the same cluster.

In this section, we outline our methodology for the creation of optimally sized clusters of tickers with well-correlated movements. We first look at our setup to compute correlations between pairs of tickers. We then outline our spectral clustering algorithm for creating clusters of closely related tickers. Lastly, we apply our setup to a set of oil-related tickers.

### 5.1. Computing correlations between pairs of tickers

Given a pair of tickers and a particular date, we create two separate time series. We perform a post-processing step to the individual raw time series to aggregate all events occurring at the same nanosecond. For example, if multiple trades were recorded for the same ticker at the same nanosecond, we consider the weighted average (based on trade volumes) of the separate trade prices as the aggregated trade price while the total trade volume at that particular nanosecond (say t) is simply the sum of volumes of all trades at t. Given these two time series, we insert artificial tick points at timestamps (in nanoseconds) where there was an event recorded for one particular ticker but not the other. Again, taking the example of trade prices, suppose we wish to compute an estimate for the trade price of a ticker at timestamp t and we have the trade prices and trade volumes for the ticker at timestamps x (< t) and y(> t), we perform a linear interpolation to estimate the trade price and volume for the ticker at timestamp t. We refer the reader to the `interpolate()` function within `./proc_analysis/proc_funcs.py` for details. We construct and insert interpolated tick points for the chosen tickers, thereby constructing a unified time series with tick information for both tickers for every available timestamp. We adopt a two-pointer approach and create the unified time series in O(`a` + `b`), where `a` and `b` are the lengths of the 2 time series (refer to `make_bivariate_data()` within `./proc_analysis/proc_funcs.py` for details). We then compute various bivariate statistics considering the unified time series.

Lastly, we developed functions to visualize the change in the price level from a base price (set to the price of the instrument when trading activity began on that particular day). As a simple example, let's consider the tickers `AAPL` (for Apple) and `AMZN` (for Amazon) on October 10, 2019. The visualizations for their un-normalized trade prices are as follows.

<div align="center">
  <img src="https://gitlab.engr.illinois.edu/fin556_algo_market_micro_fall_2023/fin556_algo_fall_2023_group_03/group_03_project/-/raw/main/figs/AAPL_trades_A.svg?ref_type=heads" width="80%">
</div>

<div align="center">
  <img src="https://gitlab.engr.illinois.edu/fin556_algo_market_micro_fall_2023/fin556_algo_fall_2023_group_03/group_03_project/-/raw/main/figs/AMAZ_trades_B.svg?ref_type=heads" width="80%">
</div>

Note that single stocks of `AAPL` and `AMZN` trade at varying prices. In this example, the mean recorded price for `AAPL` stock was $229.5894 while that for `AMZN` was at $1723.9990. Now, we visualize the change in their prices from their respective 'base' prices.

<div align="center">
  <img src="https://gitlab.engr.illinois.edu/fin556_algo_market_micro_fall_2023/fin556_algo_fall_2023_group_03/group_03_project/-/raw/main/figs/AAPL_AMZN_unscaled.svg?ref_type=heads" width="80%">
</div>

From the above visualization, it seems that the price of `AMZN` fluctuated quite a bit while `AAPL` remained largely stationary. In reality, though, there were pronounced fluctuations in the prices of `AAPL` throughout the day. However, the net price changes were much smaller compared to the fluctuations in the price of `AMZN` stock. To focus on visualizing trends instead, we appropriately scale the change in prices (from respective base prices) for the tickers that we are visualizing. The plot after scaling is as follows. In this example, the pair had a small positive correlation of 0.2303.

<div align="center">
  <img src="https://gitlab.engr.illinois.edu/fin556_algo_market_micro_fall_2023/fin556_algo_fall_2023_group_03/group_03_project/-/raw/main/figs/AAPL_AMZN_scaled.svg?ref_type=heads" width="80%">
</div>

For more details on the specifics of these visualizations, the reader could refer to `./proc_analysis/bivariate_analysis.ipynb`. `./proc_analysis/get_pairwise_correlations.ipynb` contains a complete script to calculate correlations and covariances between all possible pairs of tickers within a text ticker data dump. It explicitly tracks all dates for which tick data is available for a particular pair of tickers. Considering a small dataset for roughly two months of text tick data from 2019, we found the following pairs of highly correlated tickers.

Top 5 Ticker Pairs with the highest positive correlations:

- `GOOG, GOOGL: 0.9822`
- `IVV, SPY: 0.9218`
- `QQQ, SPY: 0.8941`
- `SPY, VOO: 0.8641`
- `SPXL, SPY: 0.8635`

Top 5 Ticker Pairs with the highest negative correlations:

- `SDS, SPY: -0.9233`
- `IVV, SDS: -0.9133`
- `SDS, SPXL: -0.9019`
- `SDS, VOO: -0.8742`
- `OILD, USO: -0.8703`

In the next step, we discuss a spectral clustering approach to cluster tickers with highly correlated movements into individual clusters.

### 5.2. Clustering tickers based on mutual correlations

We adopt a spectral clustering approach where the distance between two points (in this case, tickers) is a function of the average absolute correlation between their movements. In that, we assign two tickers with absolute correlation above a certain threshold (a modifiable parameter) into the same cluster, creating a set of ticker clusters. In the first step, we build a graph where individual tickers serve as the graph nodes. We add an edge between two nodes (tickers) if the absolute correlation between them is above the chosen threshold. Lastly, the connected components within this graph serve as the cluster of tickers. The necessary functions for ticker clustering can be found within `./proc_analysis/proc_funcs.py`. An example of their usage can be found within `./proc_analysis/get_pairwise_correlations.ipynb`. We implement a standard Depth First Search (DFS) algorithm to identify the ticker clusters.

We initially ran our ticker clustering algorithm with a default threshold of `0.8`, i.e., if the correlation between two tickers is above 0.8 or below -0.8, we make sure that they end up in the same cluster (note that we later introduced a dynamic thresholding scheme to select appropriate clusters based on a range for desirable cluster sizes). Based on our initial sample data, we found the following 4 ticker clusters.

- Size: 7, Cluster: `{'IVV', 'SDS', 'SPY', 'DIA', 'VOO', 'QQQ', 'SPXL'}`
- Size: 2, Cluster: `{'CVX', 'XOM'}`
- Size: 2, Cluster: `{'GOOGL', 'GOOG'}`
- Size: 2, Cluster: `{'OILD', 'USO'}`

Using custom visualization functions, namely, `visualize_day_multitickers()` and `build_plot()` under `./proc_analysis/build_plot/get_pairwise_correlations.ipynb`, we visualize the price movement for tickers within a cluster for a randomly selected date.

We see how the movement of tickers such as `IVV`, `DIA`, and `QQQ` are closely related to that of `SPY`.

<div align="center">
  <img src="https://gitlab.engr.illinois.edu/fin556_algo_market_micro_fall_2023/fin556_algo_fall_2023_group_03/group_03_project/-/raw/main/figs/cluster_example_A.svg?ref_type=heads" width="80%">
</div>

From the next plot, we see how the movement of `CVX` is highly correlated to that of `XOM`.

<div align="center">
  <img src="https://gitlab.engr.illinois.edu/fin556_algo_market_micro_fall_2023/fin556_algo_fall_2023_group_03/group_03_project/-/raw/main/figs/cluster_example_B.svg?ref_type=heads" width="80%">
</div>

As a final example, we see how the two tickers for Alphabet stock (`GOOGL` and `GOOG`), expectedly, have very similar movements.

<div align="center">
  <img src="https://gitlab.engr.illinois.edu/fin556_algo_market_micro_fall_2023/fin556_algo_fall_2023_group_03/group_03_project/-/raw/main/figs/cluster_example_C.svg?ref_type=heads" width="80%">
</div>

#### 5.2.1. Picking tickers related to a concept

To identify a subset of tickers related to an industry, we adopt a keyword-based filtering approach. We first consider all tickers listed on `IEX` from this data source along with their descriptions: `https://iextrading.com/trading/eligible-symbols/`. We perform some basic text normalization steps and aggregate all hits related to a set of queried keywords. As a simple example, we consider stocks related to oil. We create a custom set of the following query keywords: `['oil', 'crude', 'airline']`. We include 'airline' because the price movements of oil stocks are well correlated with those of airline stocks. Based on the query keywords, we retrieve a set of tickers on `IEX` that are relevant to the query. For our example, we retrieved 40 tickers related to the 3 keywords.

We extract raw data for all retrieved tickers within a specified period using our IEX dataset aggregator setup (described in Section 2 above). We perform some additional data dump processing steps to identify exactly which tickers actually traded within our selected period (there might not be any data for some tickers during a chosen period). For the above example, we found a set of 39 tickers related to oil after this pruning step. For a running notebook containing these steps, refer to `./proc_analysis/ticker_picker.ipynb`. We run our pairwise ticker correlation computation algorithm on all of these ticker pairs (741 pairs) and obtain the following pairs of oil-related tickers with the highest positive and negative correlations.

Top 5 Ticker Pairs with the highest positive correlations:

- `GUSH, XOP: 0.9318`
- `MRO, XOP: 0.9097`
- `DRIP, NRGD: 0.9066`
- `CRUZ, DBO: 0.9028`
- `MGY, XOP: 0.8803`

Top 5 Ticker Pairs with the highest negative correlations:

- `DRIP, XOP: -0.9542`
- `DRIP, GUSH: -0.9389`
- `NRGD, XOP: -0.9166`
- `DRIP, NRGU: -0.8776`
- `DRIP, MRO: -0.8683`

#### 5.2.2. Finding optimal clusters

We now briefly describe our algorithm to select optimally sized clusters of closely related tickers. As input, we consider the minimum and maximum desirable ticker cluster size. We run our graph-based spectral ticker clustering algorithm with various thresholds ranging from 0.6 to 0.8 and select the maximally different ticker clusters. In that, if two clusters across two different runs of the clustering algorithm contain a common ticker, we pick the cluster with the higher correlation threshold (the more cohesive cluster). For details on the exact working of the algorithm, the `generate_optimal_clusters()` function from the notebook `./proc_analysis/analyze_oil_related_tickers.ipynb` is provided below for reference.

```python
def generate_optimal_clusters(ticker_pair_to_correls, min_size = 3, max_size = 7, view = False):
    candidate_thresholds = [0.6, 0.65, 0.7, 0.75, 0.8]
    good_clusters = []
    # Contains pairs of the form cluster (a set) and the threshold used (a float)

    for threshold in candidate_thresholds:
        proc.clustering_threshold = threshold
        proc.init_clustering()
        clusters = proc.make_clusters(ticker_pair_to_correls)

        for cluster in clusters:
            if min_size <= len(cluster) <= max_size:
                good_clusters.append((cluster, threshold))
            
    if view:
        for cluster, threshold in good_clusters:
            print('- ' + str(cluster) + ', Threshold: ' + str(threshold))
            
    good_clusters = sorted(good_clusters, key = lambda x: (len(x[0]), - x[1]))

    seen_tickers = set()
    selected_ticker_clusters = []

    for cluster, threshold in good_clusters:
        flag = 0
        for ticker in cluster:
            if ticker in seen_tickers:
                flag = 1
                break
            else:
                seen_tickers.add(ticker)

        if flag == 0:
            selected_ticker_clusters.append(cluster)
    
    return selected_ticker_clusters
```

We ran our optimal cluster generation algorithm with minimum and maximum cluster sizes of 3 and 7, respectively. For the above example related to oil tickers, we obtained the following clusters across various runs of the clustering algorithm satisfying our desired size condition. 'Threshold' refers to the absolute correlation threshold adopted during a particular run of our spectral clustering algorithm.

- `{'AAL', 'UAL', 'CRUZ', 'LUV'}`, Threshold: 0.6
- `{'OILK', 'DBO', 'BNO', 'USOI', 'USO'}`, Threshold: 0.65
- `{'AAL', 'UAL', 'LUV'}`, Threshold: 0.65
- `{'OILK', 'DBO', 'BNO', 'USOI', 'USO'}`, Threshold: 0.7
- `{'BNO', 'OILK', 'USOI', 'USO'}`, Threshold: 0.75
- `{'PXJ', 'FTXN', 'OILD'}`, Threshold: 0.75
- `{'MRO', 'NRGD', 'MGY', 'XOP', 'DRIP', 'GUSH', 'MUR'}`, Threshold: 0.8
- `{'BNO', 'OILK', 'USOI', 'USO'}`, Threshold: 0.8

After the final pruning step, we obtain the following maximally different clusters.

Cluster 1:

- `PXJ`: INVESCO OIL & GAS SERVICES E
- `FTXN`: FIRST TRUST NASDAQ OIL & GAS
- `OILD`: MICRO OG EXP PROD 3X INV LEV

Cluster 2:

- `AAL`: AMERICAN AIRLINES GROUP INC
- `UAL`: UNITED AIRLINES HOLDINGS INC
- `LUV`: SOUTHWEST AIRLINES CO

Cluster 3:

- `BNO`: UNITED STATES BRENT OIL FUND
- `OILK`: PROSHARES K-1 FREE CRUDE ETF
- `USOI`: X-LINKS CRUDE OIL COV CALL
- `USO`: UNITED STATES OIL FUND LP

Cluster 4:

- `MRO`: MARATHON OIL CORP
- `NRGD`: MICROSECTORS US BIG OIL -3X
- `MGY`: MAGNOLIA OIL & GAS CORP - A
- `XOP`: SPDR S&P OIL & GAS EXP & PR
- `DRIP`: DIREXION DAILY S&P OIL & GAS
- `GUSH`: DIREXION DAILY S&P OIL & GAS
- `MUR`: MURPHY OIL CORP

The tickers `DRIP` and `GUSH` have the same basic description on this page: https://iextrading.com/trading/eligible-symbols/. Though they track the same basic index which is the S&P Oil & Gas Exploration & Production Select Industry Index, they are fundamentally different: `GUSH` is Bull 2X (300%), while `DRIP` is Bear 2X (-300%) (Source: https://portfolioslab.com/tools/stock-comparison/GUSH/DRIP).

We perform appropriate visualizations to view the movements of tickers within these optimal clusters. For cluster 4, we observe that `XOP` trades often but not `DRIP` and `GUSH` (which are based on XOP). `MUR`, `MGR`, and `MRO` trade quite often as well.

<div align="center">
  <img src="https://gitlab.engr.illinois.edu/fin556_algo_market_micro_fall_2023/fin556_algo_fall_2023_group_03/group_03_project/-/raw/main/figs/oil_cluster_4.svg?ref_type=heads" width="80%">
</div>

Tickers in cluster 1 trade very rarely. In cluster 3, `USO` trades frequently but not `OILK`, `BNO`, and `USOI`.

Cluster 2 contains three airline stocks that trade frequently and show very well-correlated movements.

<div align="center">
  <img src="https://gitlab.engr.illinois.edu/fin556_algo_market_micro_fall_2023/fin556_algo_fall_2023_group_03/group_03_project/-/raw/main/figs/oil_cluster_2.svg?ref_type=heads" width="80%">
</div>

---

### 6. Predictive Models

In this section, we outline our methodology for building various pricing prediction models.

### 6.1. Event, Window, Datapoint, and Batch Generator

In the very first step, we create event generators, both for book update events as well as for trade events. The event generator considers a set of tickers and appropriately processes the relevant tick files (within a market data repository) to generate a sequence of market events in a custom format. To take care of events across multiple tickers, we use a (minimum) priority queue and select the event with the smallest timestamp. For details, we refer the reader to the functions: `event_generator()` and `book_update_event_generator()` in `./pred/event_based_batching.ipynb`.

At a high level, we treat a raw window as a sequence of the past n relevant events for a set of tickers grounded at a particular timestamp. To achieve this, we use a second generator to generate raw events windows of a specified length. Refer to `generate_raw_windows()` and `generate_raw_book_update_windows()` in `./pred/event_based_batching.ipynb` for details. The raw event windows are processed into data points of a specified form. Broadly, we distinguish between two types of data points, namely, (a) `X-Y` datapoint, and (b) `X-only` data point. `X-Y` data points contain both the past n previous processed events for a set of tickers and the future k processed events for the ticker whose movements we wish to predict. We generate `X-Y` data points to train our predictive models. On the other hand, `X-only` data points are generated during the backtesting phase to maintain all events up to the current timestamp within the set of source features. We include various toggles for datapoint feature encoding such as whether we wish to normalize the price series or not, whether we wish to include the size for trades or the volume at a particular price level, etc.

We have a custom `state_maintainer` class to maintain the windows and create the sequence of data points. In addition to that, `state_maintainer` also creates batches of a specified size based on an input stride. We include an option to turn off batch creation and batch tracking to ensure efficiency in situations where data point batching is not required. We refer the reader to the `state_maintainer` class and its associated helper methods in `./pred/state_process.py` for details. Note that this entire functionality was implemented from scratch. It is highly modular and allows us to run various kinds of experiments: model training, batch creation, backtesting, etc., based on input parameters for considered tickers, window length, batch size, stride, etc. We have included various usage examples for the `state_maintainer` class in `./pred/event_based_batching.ipynb` including how to create data points and batches for BBO and Trade Price prediction. We include one such case here.

To create data points and data batches for BBO prediction, one could create a `state_maintainer` object, say `X_Y_state_machine` (since it generates data points of type `X-Y`) and utilize the same for data generation as follows.

```python
window_len = 64 + 3
predict_size = 3
batch_size = 128
shift = 32

X_Y_state_machine = state_maintainer(window_len = window_len,
                                     predict_size = predict_size,
                                     ticker_to_index = ticker_to_index,
                                     batch_size = batch_size,
                                     shift = shift,
                                     event_type = 1)

count_batches = 0                                   
event_limit = None

for index, event in tqdm.tqdm(enumerate(book_update_event_generator(consider_tickers, book_update_dates)), 
                                        total = event_limit, 
                                        ncols = tqdm_cols):

    if event_limit and index == event_limit:
        break
  
    datapoint, batch = X_Y_state_machine.process_event(event)
    if batch:
        count_batches += 1
```

Here, the `predict_size` specifies the number of future BBOs that we wish to predict in our experiment. The total `window_len` includes both the number of past BBOs that are used to predict the next `predict_size` BBOs. We can specify the `batch_size` while the `shift` takes care of the stride between two batches of data. `event_type` is `0` for creating data points and batches for trade type events, while `event_type` is `1` for handling book update events. When a complete data batch is ready, `batch` would not be `None` and could be used for further processing. This is especially useful in the online learning setup to fine-tune a model based on incoming minibatches. To deactivate data batching, one could simply set `batching` to `False`.

### 6.2. Predictive Models

We train our various regression models such as linear models, random forest regression, Deep Feed-Forward Neural Networks, etc., making use of our event-driven data point and batch generation setup. We also train models providing both a price prediction as well as a probabilistic interpretation of the same. A workbench for running each of these experiments can be found in `./pred/event_driven_prediction.ipynb`. This notebook contains examples for (1) Training predictive models using pre-created train, validation, and test splits, (2) Training models based on an online learning scheme (with event-driven datapoint and batch generation), (3) Event-driven model training with early stopping, (4) Visualization of predicted prices and the surrounding confidence intervals (if applicable).

#### 6.2.1. Predicting the next Best Bid and Best Offer

We generate event windows and create data points using our event-driven data point generation setup (see examples in `./pred/event_based_batching.ipynb`). We create a generic test bench for testing out various algorithms predicting the next k Best Bids and Best Offers. As an illustration, we show how to fit a simple Gaussian Process Regressor with a default kernel on finite event windows to predict the very next BBO and generate appropriate visualizations for the predicted price and the surrounding confidence interval. For details, refer to the `./pred/event_driven_prediction.ipynb`.

We consider a window of 10,000 events from a trading day and predict the next BBO based on the past 64 BBO update events. We can then visualize the predicted BBO and a 95% confidence interval (represented by the translucent circles) around the same.

<div align="center">
  <img src="https://gitlab.engr.illinois.edu/fin556_algo_market_micro_fall_2023/fin556_algo_fall_2023_group_03/group_03_project/-/raw/main/figs/gauss_ex_1e4.svg?ref_type=heads" width="75%">
</div>

We zoom in on the final 1000 events within the above window.

<div align="center">
  <img src="https://gitlab.engr.illinois.edu/fin556_algo_market_micro_fall_2023/fin556_algo_fall_2023_group_03/group_03_project/-/raw/main/figs/gauss_ex_1e3.svg?ref_type=heads" width="80%">
</div>

And lastly, we focus on the final 100 events which fit into a 1 second period. This demonstrates how the Best Bid and Best Offer fluctuate across timestamps (at the granularity of nanoseconds) offering an understanding of the low-level market microstructure.

<div align="center">
  <img src="https://gitlab.engr.illinois.edu/fin556_algo_market_micro_fall_2023/fin556_algo_fall_2023_group_03/group_03_project/-/raw/main/figs/gauss_ex_1e2.svg?ref_type=heads" width="80%">
</div>

Note that this example expectedly struggles to accurately capture the changes in the BBO since market movements, specifically, the change in the best bid or the best offer, cannot be modeled by a naive Gaussian process conditioned on the sequence ordering of events in a finite window. Kalman filters are arguably better at capturing the dynamics of the bids and offers. We include a working example of using Kalman Filters to predict the next BBO in `./timeseries_analysis/KF.ipynb` and `./kf_svi_pred/KF_visualization.ipynb`.

#### 6.2.2. Predicting the future trades

We include an example showing how to predict the next trade price given a set of past trades using a Deep Multilayer Perceptron. In the toy example, we use two days' worth of data for model training and a single day's data for each of validation and testing. We perform data window-specific price normalization steps and achieve an RMSE of 0.2037 and an MAE of 0.1607 showing the applicability of our setup. We refer the reader to `./pred/event_driven_prediction.ipynb` for a better understanding.

### 6.3. Online Learning Setup

Using our custom `state_maintainer`, we perform data point and batch generation in an event-driven fashion. The setup allows us to fine-tune our predictive models during the backtesting phase. In addition, we use the very same setup to train a predictive model from scratch in an event-driven fashion. This ensures that there is no mismatch between the model's fine-tuning and inference environments. We have a working example of the same in `./pred/event_driven_prediction.ipynb`. We also include a separate predictive model training with early stopping feature to train a predictive model using the event-driven setup with early stopping turned on. With our implementation, during backtesting, one could choose to use either a static model or a model using online learning. For the former case, one could simply turn off data point batching.

---

### 7. Predicting the next BBO with Kalman Filters

### 7.1. Overview

The Kalman Filter (KF), also known as linear quadratic estimation, is a method to perform time series estimates by modeling a joint distribution over the variables for each timestamp. A Kalman Filter is designed to be recursive and incorporates the temporal elements of our problem. See the figure below for an overview of how a Kalman Filter operates.

<div align="center" style="display: flex; justify-content: space-between;">
  <img src="./figs/report_algo.svg" style="width: 100%; margin-bottom: 20px;">
</div>

We implement this exact system, and our code can be boiled down to the following function:

```python
def kalman_filter(x, P, F, Q, H, R, z):
    # pred next timestep estimate for x and P
    x_hat = F @ x
    P_hat = F @ P @ (F.T + Q)

    # update
    K = P_hat @ H.T @ np.linalg.inv(H @ P_hat @ H.T + R)
    x = x_hat + K @ (z - H @ x_hat)
    P = (np.eye(len(x)) - K @ H) @ P_hat

    return x, P
```

Additionally, we assume a small amount (`1e-3`) of measurement noise in the covariance matrix to incorporate uncertainty/error in our model.

### 7.2. Results on SPY for January 5, 2023

Below, is a graph of a Kalman Filter's BBO predictions error over January 5, 2023, on `SPY`. For details, we refer the reader to `./kf_svi_pred/KF_visualization.ipynb`.

<div align="center" style="display: flex; justify-content: space-between;">
  <img src="./figs/KFSPY20230501.svg" style="width: 80%; margin-bottom: 20px;">
</div>

We examine the BBO prediction as we consider fewer events (top to bottom: `1e4`, `1e3`, and `1e2` events, respectively) with the translucent circles representing the 95% confidence interval at each timestamp (we observe that the change in confidence intervals across time are negligible).
<div align="center" style="display: flex; justify-content: space-between;">
  <img src="./figs/BBOKFPred1E4.svg" style="width: 80%; margin-bottom: 20px;">
</div>
<div align="center" style="display: flex; justify-content: space-between;">
  <img src="./figs/BBOKFPred1E3.svg" style="width: 80%; margin-bottom: 20px;">
</div>
<div align="center" style="display: flex; justify-content: space-between;">
  <img src="./figs/BBOKFPred1E2.svg" style="width: 80%; margin-bottom: 20px;">
</div>

---

### 8. Postmortem

Here, we record each teammate's response to specific questions, in retrospect.

---

##### 8.1. Sayar Ghosh Roy

1. What did you specifically do individually for this project?

    * Venue Arbitrage Strategy (`./venue_arb/strategy_funcs.py`)
    * Strategy-independent State Machines for tracking current position and creating appropriate actions based on incoming market events (`./venue_arb/trade_state_multi.py`, `./venue_arb/trade_states.py`)
    * Preliminary Data Processing and Analysis pipeline for processing text tick files and book update CSVs (`./proc_analysis/time_series_builder.ipynb`, `./proc_analysis/bivariate_analysis.ipynb`)
    * Algorithm to generate optimally sized clusters of tickers with well-correlated movements based on text tick data dumps (`./proc_analysis/ticker_picker.ipynb`, `./proc_analysis/analyze_oil_related_tickers.ipynb`, `./proc_analysis/get_pairwise_correlations.ipynb`)
    * Python Package containing all necessary functions for data dump processing, data analysis, and visualizations (`./proc_analysis/proc_funcs.py`)
    * Event-driven data point and data batch generation, state maintainer object for encoding the past K market events (`./pred/state_process.py`, `./pred/event_based_batching.ipynb`)
    * Test bench for multiple styles of event-driven prediction and various example models: (1) Regular training using pre-created data splits, (2) Online learning scheme with event-driven valid batch generation (based on batch size and stride), (3) Iterative event-driven training with early stopping, (4) Visualization of predicted values and surrounding confidence intervals (`./pred/pred_utils.py`, `./pred/event_driven_prediction.ipynb`)
    * Generation of NASDAQ text tick files (in the format required by Strategy Studio) based on raw NASDAQ messages (for `T` type text ticks) and NASDAQ order book updates (for `P` type ticks) (`./data_handler/NASDAQ_tick_creator.ipynb/`), Creating merged text tick files from multiple exchange-specific tick data files (`./data_handler/merge_venues.ipynb/`, `./data_handler/tick_maker_script.py`)
    * Team Leader duties, Assisting teammates in resolving their specific code issues and bugs

2. What did you learn as a result of doing your project?

    Generating an exhaustive list would be difficult. I would say that the following are my 8 main learnings.

    1. Understood how markets work, the specifics of order books and order book updates
    2. Various market data formats, and various ways to encode market events (depth update by order, depth update by price)
    3. Importance of building fault-tolerant state machines to encode one's position in the market, dynamically updating stop losses and take profit levels, and deciding on order execution strategies
    4. As an extension of Learning #3 above: The importance of explicitly tracking one's filled orders
    5. Various intricacies related to order creation and order update processing
    6. How ETFs work, how certain ETFs might be tracking the same underlying index (`DRIP` and `GUSH`)
    7. Various ways to encode the order book information in a Best Bid and Best Ask forecasting task and various data encoding and normalization schemes associated to the same
    8. This one could be somewhat too specific and esoteric. I had assumed that the Profits from a Venue Arbitrage Strategy would monotonically decrease with increase in latency. Within such a context, one could simply run a binary search over latency values to find a latency number (in nanoseconds) at which the Profits would reduce to 0 (or a certain threshold). However, I now understand that it is the determinism or predictability of the outcome (not the realized profit) that goes down with increasing latencies. And intuitively, this makes way more sense.

3. If you had a time machine and could go back to the beginning, what would you have done differently?

    * I would have set up Strategy Studio on an Ubuntu machine with a dedicated NVIDIA GPU and requested my teammates to do the same. It is not hard to get an educational Strategy Studio license for Professor Lariviere's classes.

4. If you were to continue working on this project, what would you continue to do to improve it, how, and why?

    This would somewhat depend on the amount of time I'd be continuing. However, I would have wanted to explore the following two directions.

    * Develop a generic long-short strategy class that makes use of a predictive model that is capable of predicting both price levels as well as confidence intervals around the same (a multivariate Kalman Filter or a Gaussian Process Regressor with an appropriate kernel). Within the core trade decision logic, I'd check for whether we wish to trade (go either long or short) considering a threshold confidence interval. I'd then perform a study on how the PnL and Win Rates change based on the change in the tunable threshold confidence.
    * Utilize the ticker clustering setup for building predictive models: Typically, if we wish to predict the next BBO for `SPY`, we would encode the state of the order book for `SPY` at the past k timestamps as an input to our predictive model. Specifically within the context of Deep Neural Networks, what if we include information from not just `SPY`, but from other tickers that are within the same cluster of correlated tickers as `SPY` such as `IVV`, `DIA`, `QQQ`, `SPXL`, `VOO`, etc.? Would this significantly improve the predictive performance? What happens if we include information from non-correlated tickers instead? This could potentially be an interesting research study. P.S. I have developed the necessary data generation and model-building code for this experiment already. The current event-driven data point and batch generation setup can generate data points considering multiple tickers. One could specify the list of tickers as an input, say [`SPY`, `IVV`, `QQQ`]. The generated data points would have the past k book update events for all listed tickers on the source side and the next p BBOs for a specific ticker (say `SPY`) as the target variable. The setup also appropriately handles event timestamps across all events. I have a simple running example for this in `./pred/event_driven_prediction.ipynb` (Training a Deep Feed Forward Network to predict the next price for `AAL` based on data points containing the past 64 events for each of [`AAL`, `LUV`, `UAL`]).

5. What advice do you offer to future students taking this course and working on their semester-long project?

    - See my answer to Question 3 above. Make sure to do that right away. Ideally, as soon as you register for Professor Lariviere's class and decide to work on a trading strategy using RCM-X Strategy Studio.
    - While testing out your implementations, do not use a full text tick file. Just use a small excerpt from it and make sure that everything runs and completes within a reasonable time. Especially if you are running your scripts on a VM to which you do not have admin access, the system might be too busy executing your script and lock you out or stop responding to your commands. If the tick file is too big, you'd be waiting for the script to complete running (or hope that the admin is available to kill your job).
    - Ask the Professor for periodic feedback. This would allow you to validate all of your key steps. It would also help you to course-correct before taking a wrong direction.

---

##### 8.2. Tingyin Ding

1. What did you specifically do individually for this project?

* For this project, I took care of several tasks individually:

    - **Getting IEX Data:** Before setting up the VM, I fetched some example data to help the team choose stocks.
    - **Setting Up Strategy Studio:** Initially, I tried setting up the Strategy Studio on my Mac, but it didn't work out. Once we had the VM, I wrote pipeline scripts and configured the VM's Strategy Studio to align with our experiment.
    - **Implementing Strategies with Pybind:** I delved into the documentation to fetch the Best Bid and Offer (BBO) for each market center. I made adjustments to the original PybindStrategy by adding new bindings. My work included implementing baseline strategies like SMA multi and RSI, along with integrating the VenueArbStrategy into PybindStrategy.
    - **Running Backtesting:** I conducted backtesting on the VM, exploring different latencies and strategies.
    - **Visualization:** I authored scripts for visualizing the results obtained from the backtesting phase.

2. What did you learn as a result of doing your project?

* Through the course of the project, I gained valuable insights into the intricate world of trading and the formulation of effective strategies. Specifically, I developed a deeper understanding of analyzing strategy performance, delving into the complexities of how different strategies operate in various market conditions. One notable aspect of my learning experience was the thorough debugging process, which I acquired through collaboration with my teammates. Their guidance allowed me to enhance my coding skills and troubleshoot issues meticulously, contributing to a more robust project implementation. Additionally, I expanded my knowledge base by learning Pybind, a valuable tool for seamless interoperability between C++ and Python, enhancing my ability to work across different components of the project. Furthermore, I acquired a foundational understanding of basic Simple Moving Average, Relative Strength Index, and the Venue Arbitrage strategies, providing me with practical insights into widely-used trading techniques. Overall, the project served as a comprehensive learning journey, encompassing trading intricacies, strategy analysis, debugging proficiency, and the adoption of new tools and techniques.

3. If you had a time machine and could go back to the beginning, what would you have done
   differently?

* If I could go back to the start, I'd do a couple of things differently. First off, I'd get a new computer and set up the Strategy Studio as a backup. That way, if our VM went down, we'd have a Plan B.
* Secondly, I'd take a closer look at the code and test things out more carefully. Going through the code with a fine-tooth comb and testing it thoroughly would help catch any issues early on. It's like doing a double-check to make sure everything runs smoothly.

4. If you were to continue working on this project, what would you continue to do to improve it, how,
   and why?

* To further enhance this project, I would collaborate closely with Blaine to refine the KFStrategy. This collaboration aims to ensure its seamless and optimal operation. My focus would extend to conducting extensive testing on various latencies, encompassing the time required to retrieve data from the exchange. Additionally, I propose conducting tests incorporating transaction fees and operational costs to assess the strategy's profitability under these conditions. This multifaceted approach aims to improve the robustness and versatility of the KFStrategy, addressing potential challenges and optimizing performance across a broader range of scenarios.

5. What advice do you offer to future students taking this course and working on their semester long
   project (besides “start earlier”… everyone ALWAYS says that). Providing detailed thoughtful advice to
   future students will be weighed heavily in evaluating your responses.

* **Think Ahead with Backup Plans**: In the final week, we encountered an unexpected challenge when our VM went down due to an overflow of debug prints. The situation was exacerbated by the professor falling seriously ill, causing delays in accessing the necessary resources. To avoid such predicaments, it's advisable to have backup plans in place. Setting up and testing scripts on a local machine beforehand can mitigate risks and ensure smoother execution during critical phases.
  Independence in Code Reliability:
* **Not to rely blindly on others' code**: We identified a significant bug during backtesting that limited our ability to process a substantial dataset. After a thorough code examination, we traced the issue back to an undefined variable in the script inherited from others. This underscores the importance of scrutinizing code for correctness rather than assuming it works flawlessly. Vigilance in understanding and validating code contributes to a more robust and dependable implementation.

---

##### 8.3. Blaine Hill

1. What did you specifically do individually for this project?
   * I worked on modeling different ways to perform BBO analysis and one-step prediction
     * Kalman Filter (Spent most of my time)
       * I dealt with several issues, namely how do we ensure the Kalman Filter is estimating both the mean and standard devation correctly. Early on, both the KF and SVI had issues with this.
     * SVI
       * This model works, but requires a lot of training, which makes it impractical unless you pretrain so the model learns the local features.
     * Transformer (unable to circumvent [this bug](https://discuss.huggingface.co/t/timeseriestransformer-mat1-and-mat2-shapes-cannot-be-multiplied/62836/9) in [HuggingFace's transformers library](https://huggingface.co/docs/transformers/index))
       * At first, I attempted to use HuggingFace's TimeSeriesTransformer model since it was geared for our use case. However, the author of the package had created bugs for doing one-step predictions; I tried to create my own time series transformer, but without some tricks of this model (which require a lot more time to code out), the model boiled down to predicting a number based off of the last 20 numbers which didn't perform too well.
   * I took some brief notes
   * I added the KF Strategy which is based off of Sayar's Venue Arbitrage Strategy
   * I parsed the NASDAQ ITCH data as under `utils/Nasdaq Parsing` to create Strategy Studio format data, after using a [C++ order book reconstructor](https://github.com/martinobdl/ITCH) to preprocess.
2. What did you learn as a result of doing your project?
   * I learned lots about time series analysis and the challenges associated with it: modeling volatility/noise, the lack of causality in financial data, and handling market regime changes.
   * I learned about the whole pipeline of data handling from a market source like NASDAQ's Totalview-ITCH format. There was much more to this than simply importing a csv; we had to parse each type of message or order book level to ensure the model was receiving the correct "viewpoint" of the market in order to predict the market at the next timestep. Event driven logic was a big piece of this.
3. If you had a time machine and could go back to the beginning, what would you have done
   differently?
   * Perhaps I would have focused on created strategies with simple logic at first, (we ended up using SMA to get Strategy Studio working), but had spent time on more complex models which weren't too helpful. In fact, the Kalman Filter turned out to be the best because of its simple recursive structure.
4. If you were to continue working on this project, what would you continue to do to improve it, how,
   and why?
   * I would spend some time to finalize the SVI model. It works well and converges during training, but requires a ton of data to be useful (which isn't helpful when for a specific instrument during a specific market regime, there is limited data). I would like to explore transfer learning (obtaining pretrained parameters for SVI from a similar instrument and finetuning it on the instrument of interest).
   * Additionally, I would like to explore running our venue arbitrage strategy on more data/different instruments. If we could define how we would like to take advantage of mispricing on different exchanges *with hidden orders*, we would be able to make a lot more pnl. However, for the purposes of this project, we dealt only with SPY to avoid hidden orders.
5. What advice do you offer to future students taking this course and working on their semester long
   project (besides “start earlier”… everyone ALWAYS says that). Providing detailed thoughtful advice to
   future students will be weighed heavily in evaluating your responses.
   * Start with any working strategy. Once you know Strategy Studio inside and out and have a base strategy to make trades, explore complex models to make a profit.
   * Engage with your team and advisor often! Clear communication is difficult when working on a web of pieces that need to be sync'd up.
   * Start Backwards: instead of figuring out details as you go, ensure you have those details figured out in your project proposal.
   * Have backup plans: in our project, despite different setbacks (to name a few:
     - our parsed data wasn't being consumed by strategy studio, despite the documentation saying it was correct (there was an obscure CSV we had to toggle on)
     - our VM crashed, which left us without a way to test our strategies for a few days while the professor was traveling
     - the transformer was performing well (since we couldn't include features other than the best bid and best offer)
       )
       we advanced by moving on to other todos, and came back once we had a better perspective on what to do.

---
