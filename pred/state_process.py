import os
import sys
import heapq
import pickle
import shutil
import json
import tqdm
import random
import itertools
import numpy as np
import pandas as pd
from collections import defaultdict
from collections import deque
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches

sys.path.insert(0, '../proc_analysis/')
import proc_funcs as proc

proc.data_dir = '../trade_book_sample/'
proc.data_files = proc.get_data_files(proc.data_dir)
normalize_prices = False

def filter_ticker_to_dates(ticker_to_dates, all_tickers):
    all_tickers = set(all_tickers)
    keys = [key for key in ticker_to_dates.keys()]
    
    for key in keys:
        if key not in all_tickers:
            del ticker_to_dates[key]
            
    return ticker_to_dates

def sort_ticker_dates(ticker_to_dates):
    for key in ticker_to_dates.keys():
        ticker_to_dates[key] = sorted(ticker_to_dates[key])
        
    return

EOD_marker = ['EOD']
END_marker = ['END']
marker_size_trades = 4
marker_size_book_updates = 7

def event_generator(tickers, dates):
    global EOD_marker, END_marker, marker_size_trades
    
    for date in dates:
        ticklists = []
        for ticker in tickers:
            ticklists.append(proc.build_series(ticker, date))
        
        priority_queue = []
        for index, ticklist in enumerate(ticklists):
            unit = ticklist[0] + [index, 0]
            priority_queue.append(unit)
        
        heapq.heapify(priority_queue)
        
        while priority_queue != []:
            top = heapq.heappop(priority_queue)
            
            ticker_index = top[3]
            event_index = top[4]
            
            if (event_index + 1) < len(ticklists[ticker_index]):
                unit = ticklists[ticker_index][event_index + 1] + [ticker_index, event_index + 1]
                heapq.heappush(priority_queue, unit)
            
            yield [top[0], top[1], top[2], tickers[ticker_index]]
        
        yield EOD_marker * marker_size_trades
    
    yield END_marker * marker_size_trades
    
def book_update_event_generator(tickers, dates):
    global EOD_marker, END_marker, marker_size_book_updates
    
    for date in dates:
        book_update_series = proc.build_order_book_series(date, dates)
        if book_update_series is None:
            continue
        
        for event_index, event in enumerate(book_update_series):
            ticker = event[1]
            if ticker not in tickers:
                continue
            yield event
        
        yield EOD_marker * marker_size_book_updates
    
    yield END_marker * marker_size_book_updates

def generate_raw_windows(tickers, dates, window_len):
    
    # Maintain a sliding window of events

    # window_len equals the number of historical prices (H)
    # + the number of future prices we wish to predict (F)

    n_tickers = len(tickers)
    
    window = []
    for _ in range(n_tickers):
        window.append(deque([]))
    
    ticker_to_index = {}
    
    is_complete = [False] * len(tickers)
    # Marks if we have seen enough events for that particular ticker
    
    is_window_valid = False
    # Marks if the complete window is valid
    
    for index, ticker in enumerate(tickers):
        ticker_to_index[ticker] = index
        
    def reset_window():
        nonlocal window, is_complete, is_window_valid
        
        window = []
        for _ in range(n_tickers):
            window.append(deque([]))
        
        is_complete = [False] * len(tickers)
        is_window_valid = False
    
    for event in event_generator(consider_tickers, common_dates):
        if event[0] == 'EOD':
            reset_window()
            continue
        
        elif event[0] == 'END':
            break
        
        unit = event[0 : 3]
        ticker = event[3]
        ticker_ID = ticker_to_index[ticker]
        window[ticker_ID].append(unit)
        
        if is_complete[ticker_ID]:
            window[ticker_ID].popleft()
        
        if not is_complete[ticker_ID]:
            if len(window[ticker_ID]) == window_len:
                is_complete[ticker_ID] = True
                
                if all(is_complete):
                    is_window_valid = True
                    
        if is_window_valid:
            yield window
            
def generate_raw_book_update_windows(tickers, dates, window_len):
    
    # window_len equals the number of historical book updates
    # + the number of future book updates that we wish to predict
    
    n_tickers = len(tickers)
    
    window = None
    
    is_complete = None
    # Marks if we have seen enough events for that particular ticker
    
    is_window_valid = None
    # Marks if the complete window is valid
    
    ticker_to_index = {}
    for index, ticker in enumerate(tickers):
        ticker_to_index[ticker] = index
        
    def reset_window():
        nonlocal window, is_complete, is_window_valid
        
        window = []
        for _ in range(n_tickers):
            window.append(deque([]))
        
        is_complete = [False] * len(tickers)
        is_window_valid = False
        
    reset_window()
    
    for event in book_update_event_generator(tickers, dates):
        if event[0] == 'EOD':
            reset_window()
            continue
        
        elif event[0] == 'END':
            break
        
        unit = [event[0]] + event[2 :]
        ticker = event[1]
        ticker_ID = ticker_to_index[ticker]
        window[ticker_ID].append(unit)
        
        if is_complete[ticker_ID]:
            window[ticker_ID].popleft()
        
        if not is_complete[ticker_ID]:
            if len(window[ticker_ID]) == window_len:
                is_complete[ticker_ID] = True
                
                if all(is_complete):
                    is_window_valid = True
                    
        if is_window_valid:
            yield window
        
def normalize_timestamps(X_timestamps, Y_timestamps, last_window_stamp):
    X_seconds_elapsed = []
    
    for timestamp in X_timestamps:
        X_seconds_elapsed.append(proc.convert_to_seconds(last_window_stamp - timestamp))
    
    return np.asarray(X_seconds_elapsed), None

def get_normalized_price_change(X_prices, Y_prices):
    eps = 1e-15
    
    mean = np.mean(X_prices)
    std = np.std(X_prices) + eps
    # Y_prices is not to be utilized
    
    X_prices = (X_prices - mean) / std
    Y_prices = (Y_prices - mean) / std
    
    return X_prices, Y_prices, mean, std

def get_sizes(X_sizes, Y_sizes):
    return np.asarray(X_sizes), np.asarray(Y_sizes)

def process_book_update_window(window, predict_size, last_window_timestamp):
    X_series_len = len(window) - predict_size
    
    X_timestamps, X_bids, X_asks, X_b_szs, X_a_szs = [], [[], [], []], [[], [], []], [[], [], []], [[], [], []]
    Y_timestamps, Y_bids, Y_asks, Y_b_szs, Y_a_szs = [], [[], [], []], [[], [], []], [[], [], []], [[], [], []]
    
    for unit in list(window)[0 : X_series_len]:
        X_timestamps.append(proc.get_timestamp(unit[0]))
        
        for d, X_bid in enumerate(unit[1]):
            X_bids[d].append(X_bid)
            
        for d, X_ask in enumerate(unit[3]):
            X_asks[d].append(X_ask)
            
        for d, X_bid_size in enumerate(unit[2]):
            X_b_szs[d].append(X_bid_size)
            
        for d, X_ask_size in enumerate(unit[4]):
            X_a_szs[d].append(X_ask_size)
    
    for unit in list(window)[X_series_len :]:
        Y_timestamps.append(proc.get_timestamp(unit[0]))
        
        for d, Y_bid in enumerate(unit[1]):
            Y_bids[d].append(Y_bid)
            
        for d, Y_ask in enumerate(unit[3]):
            Y_asks[d].append(Y_ask)
            
        for d, Y_bid_size in enumerate(unit[2]):
            Y_b_szs[d].append(Y_bid_size)
            
        for d, Y_ask_size in enumerate(unit[4]):
            Y_a_szs[d].append(Y_ask_size)
        
    X_timestamps, Y_timestamps = normalize_timestamps(X_timestamps, None, last_window_timestamp)
    
    if normalize_prices:
        X_bb, Y_bb, mean_bb, std_bb = get_normalized_price_change(X_bids[0], Y_bids[0])
        X_ba, Y_ba, mean_ba, std_ba = get_normalized_price_change(X_asks[0], Y_asks[0])
    else:
        X_bb, Y_bb, mean_bb, std_bb = X_bids[0], Y_bids[0], 0, 1
        X_ba, Y_ba, mean_ba, std_ba = X_asks[0], Y_asks[0], 0, 1
    
    X = [X_timestamps, X_bb, X_ba]
    Y = [Y_timestamps, Y_bb, Y_ba]
    
    return [X, Y, mean_bb, mean_ba, std_bb, std_ba, last_window_timestamp]

def process_ticker_window(window, predict_size, last_window_timestamp):
    X_series_len = len(window) - predict_size
    
    X_timestamps, X_prices, X_sizes = [], [], []
    Y_timestamps, Y_prices, Y_sizes = [], [], []
    
    for unit in list(window)[0 : X_series_len]:
        X_timestamps.append(proc.get_timestamp(unit[0]))
        X_prices.append(float(unit[1]))
        X_sizes.append(int(unit[2]))
    
    for unit in list(window)[X_series_len :]:
        Y_timestamps.append(proc.get_timestamp(unit[0]))
        Y_prices.append(float(unit[1]))
        Y_sizes.append(int(unit[2]))
        
    X_timestamps, Y_timestamps = normalize_timestamps(X_timestamps, None, last_window_timestamp)
    X_prices, Y_prices, mean, std = get_normalized_price_change(X_prices, Y_prices)
    X_sizes, Y_sizes = get_sizes(X_sizes, Y_sizes)
    
    X = [X_timestamps, X_prices, X_sizes]
    Y = [Y_timestamps, Y_prices, Y_sizes]
    
    return [X, Y, mean, std]

def raw_window_to_datapoint(raw_window, predict_size, process_func = process_ticker_window):
    
    last_stamp = None
    for window in raw_window:
        if last_stamp is None:
            last_stamp = proc.get_timestamp(window[- 1 - predict_size][0])
        else:
            last_stamp = max(last_stamp, proc.get_timestamp(window[- 1 - predict_size][0]))
        
    processed_window = []
    for ticker_window in raw_window:
        processed_window.append(process_func(ticker_window, predict_size, last_stamp))
        
    return processed_window

def generate_datapoints(tickers, dates, window_len, predict_size, limit = None):
    
    for index, raw_window in enumerate(generate_raw_windows(tickers, dates, window_len)):
        if limit and index == limit:
            break
        yield raw_window_to_datapoint(raw_window, predict_size)
        
def generate_book_update_datapoints(tickers, dates, window_len, predict_size, limit = None):
    
    for index, raw_window in enumerate(generate_raw_book_update_windows(tickers, dates, window_len)):
        if limit and index == limit:
            break
        yield raw_window_to_datapoint(raw_window, predict_size, process_func = process_book_update_window)
        
class state_maintainer:
    
    def __init__(self, window_len, predict_size, 
                 ticker_to_index, state_type = 0,
                 batching = True, batch_size = 64, 
                 shift = 16, event_type = 0):
        
        # state_type is 1 for maintaining X type windows
        # where predict_size = 0
        
        # state_type is 0 for maintaining windows containing both X and Y
        # when state_type is 0, activate data batch maintenance
        
        # event_type is 0 for creating datapoints and batches for trade type events
        # event_type is 1 for handling book update events
        
        self.state_type = state_type
        self.event_type = event_type
        self.batching = batching
        
        if state_type == 1:
            predict_size = 0
            
        if state_type == 0:
            self.batch_size = batch_size
            self.batch_shift = shift
            
            if self.batching:
                self.batch = []
        
        self.window_len = window_len
        self.predict_size = predict_size
        self.ticker_to_index = ticker_to_index
        self.n_tickers = len(ticker_to_index)
        
        self.window = None
        self.is_complete = None
        self.is_window_valid = None
        
        self.reset_window()
        
    def reset_window(self):
        self.window = []
        for _ in range(self.n_tickers):
            self.window.append(deque([]))
        
        self.is_complete = [False] * self.n_tickers
        self.is_window_valid = False
        
        if self.state_type == 0 and self.batching:
            self.batch = []
            self.curr_shifted = []
            
        return
        
    def add_event_to_window(self, event):
        if event[0] == 'EOD' or event[0] == 'END':
            self.reset_window()
            return
        
        if self.event_type == 0:
            unit = event[0 : 3]
            ticker = event[3]
            
        elif self.event_type == 1:
            unit = [event[0]] + event[2 :]
            ticker = event[1]
            
        ticker_ID = self.ticker_to_index[ticker]
        self.window[ticker_ID].append(unit)

        if self.is_complete[ticker_ID]:
            self.window[ticker_ID].popleft()

        if not self.is_complete[ticker_ID]:
            if len(self.window[ticker_ID]) == self.window_len:
                self.is_complete[ticker_ID] = True

                if all(self.is_complete):
                    self.is_window_valid = True
        
        return
    
    def check_valid_window(self):
        return self.is_window_valid
        
    def make_datapoint(self):
        if self.event_type == 0:
            return raw_window_to_datapoint(self.window, self.predict_size)
        elif self.event_type == 1:
            return raw_window_to_datapoint(self.window,
                                           self.predict_size,
                                           process_func = process_book_update_window)
    
    def update_batch(self, datapoint):
        self.batch.append(datapoint)
        
        if len(self.batch) < self.batch_size:
            return False, None
            
        if len(self.batch) == self.batch_size:
            temp_batch = self.batch
            self.batch = self.batch[self.batch_shift :]
            return True, temp_batch
    
    def process_event(self, event):
        self.add_event_to_window(event)
        if self.check_valid_window():
            datapoint = self.make_datapoint()
            batch = None
            if self.state_type == 0 and self.batching:
                is_batch_ready, batch = self.update_batch(datapoint)
            
            return datapoint, batch
            
        else:
            # No valid datapoint or data batch yet
            return None, None
        
# Usage: Define required variables

# consider_tickers = ['AAL', 'LUV', 'UAL']
# ticker_to_dates = proc.get_ticker_to_dates(proc.data_dir)
# ticker_to_dates = filter_ticker_to_dates(ticker_to_dates, consider_tickers)
# common_dates = sorted(list(proc.get_common_dates_multi(ticker_to_dates, consider_tickers)))
