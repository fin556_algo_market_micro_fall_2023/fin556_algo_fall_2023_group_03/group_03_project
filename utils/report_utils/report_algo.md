---
header-includes:
  - \usepackage[ruled,vlined,linesnumbered]{algorithm2e}
---
# Kalman Filter Algorithm

\begin{algorithm}[H]
\DontPrintSemicolon
\SetAlgoLined
\KwResult{Updated State Estimate x and Covariance Matrix P}
\SetKwInOut{Input}{Input}  
\SetKwInOut{Output}{Output}
\Input{Old Prediction x, Covariance Matrix P, System Matrix F, Process Noise Q, Measurement Matrix H, Measurement Noise R, Current BBO z}
\Output{Current Prediction x, Updated Covariance P}
\BlankLine

\tcp{Prediction}
$\hat{x} \leftarrow F * x$\;
$\hat{P} \leftarrow F * P * F^T + Q$\;

\tcp{Update}
$K \leftarrow \hat{P} \cdot H^T \cdot (H \cdot \hat{P} \cdot H^T + R)^{-1}$\;
$x \leftarrow$ $\hat{x}$ + K * (z - H * $\hat{x}$)\; 
$P \leftarrow (I - K * H) * \hat{P}$\;
\KwRet{x, P} \tcp*{Return outputs}
\caption{Kalman Filter}
\end{algorithm}