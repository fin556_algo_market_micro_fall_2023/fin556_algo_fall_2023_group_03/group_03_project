# Functions for data processing, aggregation, statistical analysis, visualizations

import os
import zipfile
import shutil
import time
import tqdm
import random
import numpy as np
import pandas as pd
from collections import defaultdict
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches

def list_all_data_files(data_dir):
    return set([filename for filename in os.listdir(data_dir) if os.path.isfile(data_dir + filename)])

def get_data_files(data_dir):
    data_files = list_all_data_files(data_dir)
    trade_tick_files = []
    
    for filename in data_files:
        if filename.endswith('.txt'):
            trade_tick_files.append(filename)
    
    return trade_tick_files

def get_book_updates_files(data_dir):
    data_files = list_all_data_files(data_dir)
    book_update_files = []
    
    for filename in data_files:
        if filename.endswith('_book_updates.csv'):
            book_update_files.append(filename)
    
    return book_update_files

def get_book_update_dates(book_update_files):
    valid_dates = []
    
    for filename in book_update_files:
        valid_dates.append(filename[0 : 8])
        
    return sorted(valid_dates)

def get_meta(ticker_filename):
    # Returns ticker, date
    try:
        fields = ticker_filename.split('_')
        return fields[1], fields[2].split('.')[0]
    except Exception as e:
        return None, None

def process_ticks(line, only_trades = True):
    units = line.split(',')
    # Format of units:
    # 0: Collection Time
    # 1: Source Time
    # 2: Sequence ID
    # 3: Tick Type (Would be 'T' for trades)
    # 4: Market Center (Would be IEX for this data source)
    # 5: Trade Price
    # 6: Number of shares traded
    
    # Return [Source Time, Trade Price, Number of shares traded]
    try:
        if only_trades and str(units[3]).strip() != 'T':
            return None
        values = [units[1], float(units[5]), int(units[6])]
        return values
    except Exception as E:
        return None

def to_float(items):
    for index, _ in enumerate(items):
        items[index] = float(items[index].strip())
    return items
    
def process_book_line(line):
    units = line.split(',')
    
    units = line.split(',')
    # Format of units:
    
    # 0: Collection Time
    # 1: Message ID
    # 2: Message type: either BID_UPDATE or ASK_UPDATE
    # 3: Ticker Symbol
    
    # 4: Bid Price 1
    # 5: Bid Size 1
    # 6: Bid Price 2
    # 7: Bid Size 2
    # 8: Bid Price 2
    # 9: Bid Size 2
    
    # 10: Ask Price 1
    # 11: Ask Size 1
    # 12: Ask Price 2
    # 13: Ask Size 2
    # 14: Ask Price 2
    # 15: Ask Size 2
    
    # Return [Collection Time, Ticker Symbol, Bid Prices, Bid Sizes, Ask Prices, Ask Sizes]
    try:
        values = [units[0], str(units[3]),
                 to_float([units[4], units[6], units[8]]),
                 to_float([units[5], units[7], units[9]]),
                 to_float([units[10], units[12], units[14]]),
                 to_float([units[11], units[13], units[15]])]
        
        return values
    except Exception as E:
        return None

def get_ticks(filename, line_processor = process_ticks, skip = False):
    global data_dir, line_limit
    processed_ticks = []
    line_count = 0
    
    with open(data_dir + filename, 'r+') as f:
        while line_count < line_limit:
            line = f.readline()
            line_count += 1
            
            if skip:
                skip = False
                continue
            
            if not line:
                break
            
            tick = line_processor(line)
            if tick is not None:
                processed_ticks.append(tick)
    
    processed_ticks = sorted(processed_ticks, key = lambda x: x[0])
    return processed_ticks

def build_series(ticker, datestring):
    global data_files
    search_for_file = 'tick_' + ticker + '_' + datestring + '.txt'
    
    if search_for_file not in data_files:
        print('Data Unavailable')
        return None
    
    ticklist = get_ticks(search_for_file)
    return ticklist

def build_order_book_series(date, book_update_dates):
    if date not in book_update_dates:
        return None
    filename = date + '_book_updates.csv'
    
    try:
        series = get_ticks(filename, line_processor = process_book_line, skip = True)
        return series
    
    except Exception as E:
        return None

def get_univariate_stats(datalist):
    stats = {}
    stats['mean'] = np.mean(datalist)
    stats['med'] = np.median(datalist)
    stats['std'] = np.std(datalist)
    stats['min'] = np.min(datalist)
    stats['max'] = np.max(datalist)
    return stats

def get_ticklist_stats(ticklist):
    prices = [tick[1] for tick in ticklist]
    trade_sizes = [tick[2] for tick in ticklist]
    stats = {'count': len(ticklist),
             'price': get_univariate_stats(prices),
             'trade_size': get_univariate_stats(trade_sizes)}
    return stats

def get_info_ticklist(ticklist, ticker, date, save_fig = None):
    ticklist_stats = proc.get_ticklist_stats(ticklist)
    print('Statistics for ' + ticker + ', Date: ' + date + ':')
    print(ticklist_stats)

    proc.visualize_trades(ticklist, ticker, date, save_fig = save_fig)

def get_timestamp(date_time_string):
    time_code = pd.to_datetime(date_time_string, format = '%Y-%m-%d %H:%M:%S.%f')
    return time_code

def convert_to_seconds(time_delta):
    return time_delta.total_seconds()

def visualize_trades(ticklist, ticker, date, save_fig = None):
    global radius_scale, display_dpi
    plt.rcParams['figure.dpi'] = display_dpi
    base_time = get_timestamp(ticklist[0][0])
    num_ticks = len(ticklist)
    
    time_diffs, prices, sizes = [], [], []
    
    for tick in ticklist:
        time_code = convert_to_seconds(get_timestamp(tick[0]) - base_time)
        time_diffs.append(time_code)
        prices.append(tick[1])
        sizes.append(tick[2])
    
    title_string = 'Trade price (with sizes) against time elapsed, Ticker: ' + ticker
    title_string += ', Date: ' + date
    fig = plt.figure(figsize = (12, 6))
    subplot = fig.add_subplot(111)
    subplot.set_title(title_string, fontsize = 14)
    subplot.set_xlabel('Time elapsed (in seconds)', fontsize = 12)
    subplot.set_ylabel('Trade Price (in $)', fontsize = 12)
    
    for index in range(num_ticks):
        subplot.plot(time_diffs[index],
                     prices[index],
                     marker = 'o',
                     alpha = 0.2,
                     color = 'blue',
                     markersize = (sizes[index]) * radius_scale)
    
    plt.plot(time_diffs, prices, color = 'blue', alpha = 0.25, linewidth = 0.5)    
    
    if save_fig:
        plt.savefig('../figs/' + save_fig + '.svg', format = 'svg', bbox_inches = 'tight')
    else:
        plt.show()
        
    return

def interpolate(tick_prev, tick_next, time):
    assert time >= tick_prev[0] and time <= tick_next[0]
    delta = convert_to_seconds(tick_next[0] - tick_prev[0])
    position = convert_to_seconds(time - tick_prev[0])
    
    price = (tick_prev[1] * (delta - position) / delta) + (tick_next[1] * position / delta)
    size = (tick_prev[2] * (delta - position) / delta) + (tick_next[2] * position / delta)
    return [time, price, size]

def conflate_ticks(ticks):
    # Multiple ticks at the exact same nanosecond
    amount = size = 0
    for tick in ticks:
        amount += tick[1] * tick[2]
        size += tick[2]
    
    return [get_timestamp(ticks[0][0]), (amount / size), size]

def conflate_trades(ticklist):
    # Conflate multiple trades occurring at the exact same nanosecond
    conflated_ticks = []
    ticks_lot = [ticklist[0]]
    
    for tick in ticklist[1:]:
        if tick[0] != ticks_lot[0][0]:
            conflated_ticks.append(conflate_ticks(ticks_lot))
            ticks_lot = [tick]
        else:
            ticks_lot.append(tick)
    
    conflated_ticks.append(conflate_ticks(ticks_lot))
    return conflated_ticks

def ticks_time_dict_to_list(ticks_time_dict):
    bivariate_data = []
    
    for time in ticks_time_dict.keys():
        unit = ticks_time_dict[time]
        bivariate_data.append([time, [unit[0][1], unit[0][2]], [unit[1][1], unit[1][2]]])
    
    bivariate_data = sorted(bivariate_data, key = lambda x: x[0])
    return bivariate_data

def make_bivariate_data(ticklist_A, ticklist_B):
    # For every timestamp in ticklist_A, we must have the corresponding interpolated price for ticker B
    # And vice-versa
    
    list_A = conflate_trades(ticklist_A)
    list_B = conflate_trades(ticklist_B)
    
    a = len(list_A)
    b = len(list_B)
    
    # Here, we use a O(a + b) algorithm
    ticks_interpolated = {}
    last_first_trade = max(list_A[0][0], list_B[0][0])
    
    index = 1
    for tick in list_A[1:]:
        time = tick[0]
        if time < last_first_trade:
            continue
        
        while index < b:
            tick_B = list_B[index]
            if tick_B[0] < time:
                index += 1
                continue
            elif tick_B[0] == time:
                ticks_interpolated[time] = [tick, tick_B]
            else:
                ticks_interpolated[time] = [tick, interpolate(list_B[index - 1], tick_B, time)]
            break
    
    index = 1
    for tick in list_B[1:]:
        time = tick[0]
        if time < last_first_trade:
            continue
        if time in ticks_interpolated.keys():
            continue
        
        while index < a:
            tick_A = list_A[index]
            if tick_A[0] < time:
                index += 1
                continue
            elif tick_A[0] == time:
                ticks_interpolated[time] = [tick_A, tick]
            else:
                ticks_interpolated[time] = [interpolate(list_A[index - 1], tick_A, time), tick]
            break
            
    bivariate_data = ticks_time_dict_to_list(ticks_interpolated)
    
    return bivariate_data

def range_scaling(values):
    value_range = np.max(values) - np.min(values)
    range_scaled = []
    for value in values:
        range_scaled.append(value / value_range)
    return range_scaled, value_range

def compute_cor_cov(bivariate_data):
    price_A_base = bivariate_data[0][1][0]
    price_B_base = bivariate_data[0][2][0]
    
    prices_A = [(unit[1][0] - price_A_base) for unit in bivariate_data]
    prices_B = [(unit[2][0] - price_B_base) for unit in bivariate_data]
    
    if np.std(prices_A) == 0 or np.std(prices_B) == 0:
        corr = None
    else:
        corr = np.corrcoef(prices_A, prices_B)[0][1]
    cov = np.cov(prices_A, prices_B)[0][1]
    return corr, cov

def proc_scale(scale):
    return '{:.2f}'.format(np.round(scale, 2))

def visualize_bivariate(ticklist_A, ticklist_B, query_info, scaled = True, save_fig = None):
    global radius_scale, display_dpi
    query_ticker_A, query_ticker_B, query_date_A, query_date_B = query_info
    price_A_base = ticklist_A[0][1]
    price_B_base = ticklist_B[0][1]
    
    plt.rcParams['figure.dpi'] = display_dpi
    base_time = min(get_timestamp(ticklist_A[0][0]), get_timestamp(ticklist_B[0][0]))
    n_A = len(ticklist_A)
    n_B = len(ticklist_B)
    
    time_diffs_A, prices_A, sizes_A = [], [], []
    time_diffs_B, prices_B, sizes_B = [], [], []
    
    for tick in ticklist_A:
        time_code = convert_to_seconds(get_timestamp(tick[0]) - base_time)
        time_diffs_A.append(time_code)
        prices_A.append(tick[1] - price_A_base)
        sizes_A.append(tick[2])
        
    for tick in ticklist_B:
        time_code = convert_to_seconds(get_timestamp(tick[0]) - base_time)
        time_diffs_B.append(time_code)
        prices_B.append(tick[1] - price_B_base)
        sizes_B.append(tick[2])
        
    if scaled:
        prices_A, scale_A = range_scaling(prices_A)
        prices_B, scale_B = range_scaling(prices_B)
    
    label_A = query_ticker_A + ', ' + query_date_A
    label_B = query_ticker_B + ', ' + query_date_B
    
    is_scaled_tag = ' '
    if scaled:
        is_scaled_tag = ' (scaled) '
        
    title_string = 'Changes in trade price' + is_scaled_tag + 'against time elapsed, Tickers: '
    title_string += '[' + label_A + '] [' + label_B + ']'
    
    fig = plt.figure(figsize = (12, 6))
    subplot = fig.add_subplot(111)
    subplot.set_title(title_string, fontsize = 14)
    subplot.set_xlabel('Time elapsed (in seconds)', fontsize = 12)
    if scaled:
        subplot.set_ylabel('Change in trade price (scaled)', fontsize = 12)
    else:
        subplot.set_ylabel('Change in trade price ($)', fontsize = 12)
    
    for index in range(n_A):
        subplot.plot(time_diffs_A[index],
                     prices_A[index],
                     marker = 'o',
                     alpha = 0.2,
                     color = 'blue',
                     markersize = (sizes_A[index]) * radius_scale)
    
    for index in range(n_B):
        subplot.plot(time_diffs_B[index],
                     prices_B[index],
                     marker = 'o',
                     alpha = 0.2,
                     color = 'green',
                     markersize = (sizes_B[index]) * radius_scale)
    
    plt.plot(time_diffs_A, prices_A, color = 'blue', alpha = 0.25, linewidth = 0.5)
    plt.plot(time_diffs_B, prices_B, color = 'green', alpha = 0.25, linewidth = 0.5)
    
    if scaled:
        label_A += ' (1 unit = $' + proc_scale(scale_A) + ')'
        label_B += ' (1 unit = $' + proc_scale(scale_B) + ')'
    
    blue_patch = mpatches.Patch(color = 'blue', label = label_A)
    green_patch = mpatches.Patch(color = 'green', label = label_B)
    plt.legend(handles = [blue_patch, green_patch])
    
    if save_fig:
        plt.savefig('../figs/' + save_fig + '.svg', format = 'svg', bbox_inches = 'tight')
    else:
        plt.show()
    
def get_ticker_to_dates(data_dir):
    data_files = get_data_files(data_dir)
    meta_infos = []
    for filename in data_files:
        meta = get_meta(filename)
        if meta[0] is not None:
            meta_infos.append(meta)
    
    ticker_to_date = defaultdict(list)
    
    for file_info in meta_infos:
        ticker_to_date[file_info[0]].append(file_info[1])
        
    ticker_to_date = dict(ticker_to_date)
    
    for key in ticker_to_date.keys():
        ticker_to_date[key] = set(ticker_to_date[key])
        
    return ticker_to_date

def get_common_dates(ticker_to_dates, ticker_A, ticker_B):
    if ticker_A not in ticker_to_dates or ticker_B not in ticker_to_dates:
        return None
    return ticker_to_dates[ticker_A].intersection(ticker_to_dates[ticker_B])

def get_common_dates_multi(ticker_to_dates, tickers):
    if tickers[0] not in ticker_to_dates:
        return None
    dates = ticker_to_dates[tickers[0]]
    
    for ticker in tickers[1:]:
        if ticker not in ticker_to_dates:
            return None
        dates = dates.intersection(ticker_to_dates[ticker])
        if len(dates) == 0:
            return None
    
    return dates

def get_ticker_date_pair_correl(ticker_A, ticker_B, date_string):
    min_points = 4
    ticklist_A = build_series(ticker_A, date_string)
    ticklist_B = build_series(ticker_B, date_string)
    n_A = len(ticklist_A)
    n_B = len(ticklist_B)
    if n_A < min_points or n_B < min_points:
        return None
    bivariate_data = make_bivariate_data(ticklist_A, ticklist_B)
    
    if len(bivariate_data) < min_points:
        return None
    corr, _ = compute_cor_cov(bivariate_data)
    return corr

def get_ticker_pair_correl(ticker_A, ticker_B, ticker_to_dates):
    min_points = 4
    common_dates = get_common_dates(ticker_to_dates, ticker_A, ticker_B)
    datewise_correls = []
    for date_str in common_dates:
        corr = get_ticker_date_pair_correl(ticker_A, ticker_B, date_str)
        if corr is not None:
            datewise_correls.append(corr)
    
    if len(datewise_correls) < min_points:
        return None, None
    return np.mean(datewise_correls), np.std(datewise_correls)

def get_all_ticker_pair_correls(ticker_to_dates, tqdm_cols = 80):
    tickers = sorted([ticker for ticker in ticker_to_dates.keys()])
    count_tickers = len(tickers)
    ticker_pairs = []
    
    for first, ticker_A in tqdm.tqdm(enumerate(tickers)):
        for second in range(first + 1, count_tickers):
            ticker_B = tickers[second]
            ticker_pairs.append((ticker_A, ticker_B))
    
    ticker_pair_to_correls = {}
    
    for ticker_pair in tqdm.tqdm(ticker_pairs, ncols = tqdm_cols):
        pair_correl, _ = get_ticker_pair_correl(ticker_pair[0], ticker_pair[1], ticker_to_dates)
        if pair_correl is not None:
            ticker_pair_to_correls[ticker_pair] = pair_correl
            
    return ticker_pair_to_correls

def build_ticker_meaning_map(data_source):
    with open(data_source, 'r+') as f:
        lines = f.read().split('\n')[1 : ]
    
    infolines = []
    for line in lines:
        units = [unit.strip() for unit in line.split(',')]
        infolines.append([units[0], units[2]])
    
    return infolines

def get_ticker_to_names(data_source):
    ticker_meaning_list = proc.build_ticker_meaning_map(data_source)
    ticker_to_names = {}
    for ticker, name in ticker_meaning_list:
        ticker_to_names[ticker] = name
    
    return ticker_to_names
    
def get_related_tickers(keywords_list, data_source):
    infolines = build_ticker_meaning_map(data_source)
    valid_tickers = set()
    
    for info_blob in infolines:
        for keyword in keywords_list:
            if keyword in info_blob[0].lower() or keyword in info_blob[1].lower():
                valid_tickers.add(info_blob[0])
                
    return valid_tickers

def filter_ticker_to_dates(ticker_to_dates, valid_tickers):
    valid_tickers = set(valid_tickers)
    validated = {}
    
    for key in ticker_to_dates.keys():
        if key in valid_tickers:
            validated[key] = ticker_to_dates[key]
            
    return validated

colors = ['blue', 'green', 'red', 'gray', 'brown',
          'orange', 'magenta', 'maroon', 'black', 'purple']

def visualize_day_multitickers(tickers, date_string, scaled = True, save_fig = None):
    global colors
    min_points = 4
    
    tickers = sorted(list(set(tickers)))
    count_tickers = min(len(tickers), len(colors))
    tickers = tickers[0 : count_tickers]
    
    ticklists, base_prices, n = [], [], []
    all_time_diffs, all_prices, all_sizes, all_scales = [], [], [], []
    
    base_time = None
    
    for ticker in tickers:
        ticklist = build_series(ticker, date_string)
        if ticklist is None:
            return None
        base_price = ticklist[0][1]
        
        ticklists.append(ticklist)
        base_prices.append(base_price)
        n.append(len(ticklist))
        
        if base_time is None:
            base_time = get_timestamp(ticklist[0][0])
        else:
            base_time = min(base_time, get_timestamp(ticklist[0][0]))
        
        time_diffs, prices, sizes = [], [], []
        
        for tick in ticklist:
            time_code = convert_to_seconds(get_timestamp(tick[0]) - base_time)
            time_diffs.append(time_code)
            prices.append(tick[1] - base_price)
            sizes.append(tick[2])
            
        all_time_diffs.append(time_diffs)
        all_prices.append(prices)
        all_sizes.append(sizes)
    
    plt.rcParams['figure.dpi'] = display_dpi
        
    if scaled:
        for ticker_ID in range(count_tickers):
            all_prices[ticker_ID], scale = range_scaling(all_prices[ticker_ID])
            all_scales.append(scale)
    
    is_scaled_tag = ' '
    if scaled:
        is_scaled_tag = ' (scaled) '
        
    title_string = 'Changes in trade price' + is_scaled_tag + 'against time elapsed'
    title_string += ', Date: ' + date_string
    
    fig = plt.figure(figsize = (14, 8))
    subplot = fig.add_subplot(111)
    subplot.set_title(title_string, fontsize = 14)
    subplot.set_xlabel('Time elapsed (in seconds)', fontsize = 12)
    if scaled:
        subplot.set_ylabel('Change in trade price (scaled)', fontsize = 12)
    else:
        subplot.set_ylabel('Change in trade price ($)', fontsize = 12)
    
    for ticker_ID, ticker in enumerate(tickers):
        for index in range(n[ticker_ID]):
            subplot.plot(all_time_diffs[ticker_ID][index],
                         all_prices[ticker_ID][index],
                         marker = 'o',
                         alpha = 0.2,
                         color = colors[ticker_ID],
                         markersize = (all_sizes[ticker_ID][index]) * radius_scale)
    
    for ticker_ID in range(count_tickers):
        plt.plot(all_time_diffs[ticker_ID],
                 all_prices[ticker_ID],
                 color = colors[ticker_ID],
                 alpha = 0.25,
                 linewidth = 0.5)
    
    if scaled:
        patches = []
        for ticker_ID in range(count_tickers):
            ticker = tickers[ticker_ID]
            label = ticker + ' (1 unit = $' + proc_scale(all_scales[ticker_ID]) + ')'
            patches.append(mpatches.Patch(color = colors[ticker_ID], label = label))
    
    plt.legend(handles = patches)
    
    if save_fig:
        plt.savefig('../figs/' + save_fig + '.svg', format = 'svg', bbox_inches = 'tight')
    else:
        plt.show()
    
def fetch_ticker_pair_correl(ticker_pair_to_correls, ticker_pair, display = False):
    ticker_pair = tuple(ticker_pair)
    if ticker_pair not in ticker_pair_to_correls:
        return None
    else:
        if display:
            print(str(ticker_pair) + ': ' + str(ticker_pair_to_correls[ticker_pair]))
        return ticker_pair_to_correls[ticker_pair]
    
def display_pair_correls(ticker_pair_correls, display_k = 10):
    print('Top ' + str(display_k) + ' Ticker Pairs with highest positive correlations:')
    for index in range(display_k):
        print('- ' + ticker_pair_correls[index][1] + ', ' +
              ticker_pair_correls[index][2] + ': ' +
              str(round(ticker_pair_correls[index][0], 4)))

    print()

    print('Top ' + str(display_k) + ' Ticker Pairs with highest negative correlations:')
    for index in range(display_k):
        print('- ' + ticker_pair_correls[- (1 + index)][1] + ', ' + 
              ticker_pair_correls[- (1 + index)][2] + ': ' +
              str(round(ticker_pair_correls[- (1 + index)][0], 4)))
        
    return
    
def build_plot(tickers, ticker_to_dates, pick_random = False, save_fig = None):
    common_dates = sorted(list(get_common_dates_multi(ticker_to_dates, tickers)))
    seed = 42
    random.seed(seed)

    if pick_random:
        random_index = random.randint(0, len(common_dates) - 1)
        date = common_dates[random_index]
    else:
        date = common_dates[0] # Specify the date index here

    # Visualize trades
    try:
        visualize_day_multitickers(tickers, date, scaled = True, save_fig = save_fig)
    except Exception as E:
        print('Missing data for the picked date')
        
# Functions to process raw zipped files containing ticker data
        
def fetch_dates(location, prefix_string):
    valid_dates = []
    start_index = len(prefix_string)
    for filename in os.listdir(location):
        if filename.startswith(prefix_string) and filename.endswith('.zip'):
            valid_dates.append(filename[start_index : start_index + 8])
    return valid_dates

def fetch_all_tickers(location, datestring, prefix_string):
    zipfile_name = location + prefix_string + datestring + '.zip'
    file_list, tickers = None, []
    
    with zipfile.ZipFile(zipfile_name, 'r') as f:
        file_list = [file for file in f.namelist() if file.startswith('data/text_tick_data/tick_')]
    
    for filename in file_list:
        tick_file_name = filename.split('/')[-1]
        ticker = tick_file_name[5 : tick_file_name.find('_', 5)]
        tickers.append(ticker)
    
    return set(tickers)

def process_zipped_files(location, ticker_symbol, datestring, prefix_string):
    zip_file_name = location + prefix_string + datestring + '.zip'
    file_inside_zip = 'data/text_tick_data/tick_' + ticker_symbol + '_' + datestring + '.txt'
    
def get_datestring_to_tickers(location, valid_dates, prefix_string):
    datestring_to_tickers = {}
    
    for datestring in valid_dates:
        tickers = fetch_all_tickers(location, datestring, prefix_string)
        datestring_to_tickers[datestring] = tickers
    
    return datestring_to_tickers

def extract_data(extract_location, datestring_to_tickers, data_location, prefix_string):
    if os.path.exists(extract_location):
        shutil.rmtree(extract_location)
    
    os.makedirs(extract_location)
    
    for datestring in tqdm.tqdm(datestring_to_tickers.keys(), ncols = tqdm_cols):
        tickers = datestring_to_tickers[datestring]
        for ticker in tickers:
            zip_file = data_location + prefix_string + datestring + '.zip'
            filename = 'data/text_tick_data/tick_' + ticker + '_' + datestring + '.txt'

            with zipfile.ZipFile(zip_file, 'r') as f:
                temp_file = f.extract(filename, extract_location)
                relocated = os.path.join(extract_location, 'tick_' + ticker + '_' + datestring + '.txt')
                os.rename(temp_file, relocated)
            
    shutil.rmtree(location + 'extracted/data/')
    
# Ticker Clustering related functions

def init_clustering():
    global nodes, adj, visited_nodes, mark, clustering_threshold
    nodes, adj, visited_nodes, mark = None, None, None, None
    
def build_graph(ticker_pair_to_correls):
    global nodes, adj, clustering_threshold
    nodes = set()
    
    for ticker_pair in ticker_pair_to_correls.keys():
        nodes.add(ticker_pair[0])
        nodes.add(ticker_pair[1])
    
    nodes = sorted(list(nodes))
    adj = defaultdict(list)
    
    # Creating a adjacency list of tickers
    
    for pair in ticker_pair_to_correls.keys():
        abs_correl = abs(ticker_pair_to_correls[pair])
        if abs_correl >= clustering_threshold:
            adj[pair[0]].append(pair[1])
            adj[pair[1]].append(pair[0])
    
    return nodes, adj

def dfs(node):
    global visited_nodes, nodes, adj, mark
    
    visited_nodes[node] = mark
    
    for neighbor in adj[node]:
        if neighbor not in visited_nodes.keys():
            dfs(neighbor)
            
    return
    
def mark_clusters():
    global nodes, visited_nodes, mark
    
    for node in nodes:
        if node not in visited_nodes.keys():
            dfs(node)
            mark += 1
    return

def aggregate_clusters():
    global nodes, visited_nodes
    
    cluster_ID_to_nodes = defaultdict(set)
    
    for node in nodes:
        cluster_ID = visited_nodes[node]
        cluster_ID_to_nodes[cluster_ID].add(node)
        
    cluster_ID_to_nodes = dict(cluster_ID_to_nodes)
    return cluster_ID_to_nodes

def make_clusters(ticker_pair_to_correls):
    global nodes, adj, visited_nodes, mark
    
    nodes, adj = build_graph(ticker_pair_to_correls)
    num_nodes = len(nodes)
    visited_nodes = {}
    mark = 1
    mark_clusters()
    cluster_ID_to_nodes = aggregate_clusters()
    clusters = []
    
    for key in cluster_ID_to_nodes.keys():
        if len(cluster_ID_to_nodes[key]) > 1:
            clusters.append([cluster_ID_to_nodes[key], len(cluster_ID_to_nodes[key])])
    
    clusters = sorted(clusters, key = lambda x: x[1], reverse = True)
    clusters = [unit[0] for unit in clusters]
    
    return clusters

def view_top_clusters(clusters):
    for cluster in clusters:
        print('- Size: ' + str(len(cluster)) + ', Cluster: ' + str(cluster))
    
    return

def generate_optimal_clusters(ticker_pair_to_correls, min_size = 3, max_size = 7, view = False):
    candidate_thresholds = [0.6, 0.65, 0.7, 0.75, 0.8]
    good_clusters = []
    # Contains pairs of the form cluster (a set) and the threshold used (a float)

    for threshold in candidate_thresholds:
        proc.clustering_threshold = threshold
        proc.init_clustering()
        clusters = proc.make_clusters(ticker_pair_to_correls)

        for cluster in clusters:
            if min_size <= len(cluster) <= max_size:
                good_clusters.append((cluster, threshold))
            
    if view:
        for cluster, threshold in good_clusters:
            print('- ' + str(cluster) + ', Threshold: ' + str(threshold))
            
    good_clusters = sorted(good_clusters, key = lambda x: (len(x[0]), - x[1]))

    seen_tickers = set()
    selected_ticker_clusters = []

    for cluster, threshold in good_clusters:
        flag = 0
        for ticker in cluster:
            if ticker in seen_tickers:
                flag = 1
                break
            else:
                seen_tickers.add(ticker)

        if flag == 0:
            selected_ticker_clusters.append(cluster)
    
    return selected_ticker_clusters

nodes, adj, visited_nodes, mark = None, None, None, None
clustering_threshold = 0.8

# Parameters with default values:

line_limit = int(1e12)
radius_scale = 0.01
display_dpi = 1200

data_dir = './sample_data/'

# Populate the data_files field as:
# proc_funcs.data_files = get_data_files(data_dir)

# Usage:

# For a single ticker:
# ticklist = proc_funcs.build_series('AAPL', '20191001')
# ticklist_stats = proc_funcs.get_ticklist_stats(ticklist)
# proc_funcs.visualize_trades(ticklist, query_ticker, query_date)

# For a pair of tickers:

# query_ticker_A = 'AAPL'
# query_date_A = '20191010'
# query_ticker_B = 'AMZN'
# query_date_B = '20191010'
# query_info = [query_ticker_A, query_ticker_B, query_date_A, query_date_B]

# query_info is required by the visualization function

# ticklist_A = proc_funcs.build_series(query_ticker_A, query_date_A)
# ticklist_B = proc_funcs.build_series(query_ticker_B, query_date_B)
# bivariate_data = proc_funcs.make_bivariate_data(ticklist_A, ticklist_B)
# corr, cov = proc_funcs.compute_cor_cov(bivariate_data)
# proc_funcs.visualize_bivariate(ticklist_A, ticklist_B, query_info, scaled = True)
