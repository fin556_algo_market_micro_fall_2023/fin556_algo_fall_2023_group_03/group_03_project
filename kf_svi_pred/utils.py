import os
import sys
import pickle
import joblib
import shutil
import json
import random
import itertools
import numpy as np
import pandas as pd
from collections import defaultdict
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import math

sys.path.insert(0, '../proc_analysis/')
sys.path.insert(0, '../pred/')
import proc_funcs as proc
import state_process as state_proc
from state_process import state_maintainer

import tqdm
tqdm_cols = 100

import warnings
warnings.filterwarnings('ignore')

from sklearn import metrics


def get_data_vector(datapoint, include_trade_sizes = False, no_target = False):
    # By Default, we'll predict the price of ticker 0
    # If no_target, y would be None
    
    if no_target:
        y = None
    else:
        y = datapoint[0][1][1]
    x = []
    
    for ticker_ID in range(len(datapoint)):
        x.append(datapoint[ticker_ID][0][0])
        x.append(datapoint[ticker_ID][0][1])
        if include_trade_sizes:
            x.append(datapoint[ticker_ID][0][2])
    
    x = np.concatenate(x)
    
    return x, y


def get_vectored_batch(batch, include_trade_sizes = False, no_target = False):
    X_vectors = []
    Y_vectors = []
    
    for datapoint in batch:
        x, y = get_data_vector(datapoint, 
                               include_trade_sizes = include_trade_sizes, 
                               no_target = no_target)
        
        X_vectors.append(x)
        Y_vectors.append(y)
        
    return X_vectors, Y_vectors


def get_datewise_split(common_dates, train_frac = 0.7, val_frac = 0.2, unit_test_mode = False):
    count_dates = len(common_dates)
    val_start = int(count_dates * train_frac)
    test_start = int(count_dates * (train_frac + val_frac))
        
    # Dates for the training, validation, and testing
    splits = [common_dates[0 : val_start], 
              common_dates[val_start : test_start],
              common_dates[test_start :]]
    
    if not unit_test_mode:
        return splits
    else:
        print(splits)
        return [[splits[0][0]], [splits[1][0]], [splits[2][0]]]
    

def train_batch(model, vectored_X, vectored_Y):
    model.partial_fit(vectored_X, vectored_Y)
    return model

def get_grouped(values):
    grouped = []
    index = 0
    l = len(values)
    
    while index + state_proc.predict_size < l:
        grouped.append(values[index : (index + state_proc.predict_size)])
        index += state_proc.predict_size
    return grouped

def get_metrics(tgt, predictions, display = False):
    rmse = metrics.mean_squared_error(tgt, predictions, squared = False)
    mae = metrics.mean_absolute_error(tgt, predictions)
    
    if display:
        tqdm.tqdm.write('RMSE: ' + str(rmse))
        tqdm.tqdm.write('MAE: ' + str(mae) + '\n')
        tqdm.tqdm.write('First values (linearized) in:')
        tqdm.tqdm.write('- True Values: ' + str(get_grouped([round(val, 3) for val in tgt[0 : 10]])))
        tqdm.tqdm.write('- Predicted Values: ' +
                        str(get_grouped([round(val, 3) for val in predictions[0 : 10]])) + '\n')

    return [rmse, mae]

def train_epoch(model, consider_tickers, train_dates, event_limit = None):
    global tqdm_cols
    
    X_Y_state_machine = state_maintainer(window_len = state_proc.window_len, 
                                         predict_size = state_proc.predict_size,
                                         ticker_to_index = state_proc.ticker_to_index,
                                         batch_size = state_proc.batch_size,
                                         shift = state_proc.shift)
    
    for index, event in tqdm.tqdm(enumerate(state_proc.event_generator(consider_tickers, train_dates)), 
                                  total = event_limit,
                                  ncols = tqdm_cols):
    
        if event_limit and index == event_limit:
            break

        datapoint, batch = X_Y_state_machine.process_event(event)
            
        if batch:
            vectored_X, vectored_Y = get_vectored_batch(batch)
            model = train_batch(model, vectored_X, vectored_Y)

    return model
    
def evaluate(model, consider_tickers, eval_dates, event_limit = None, display = False):
    global tqdm_cols
    
    X_Y_state_machine = state_maintainer(window_len = state_proc.window_len, 
                                         predict_size = state_proc.predict_size,
                                         ticker_to_index = state_proc.ticker_to_index,
                                         batch_size = state_proc.batch_size,
                                         shift = state_proc.shift)
    
    
    tgts, preds = [], []
    for index, event in tqdm.tqdm(enumerate(state_proc.event_generator(consider_tickers, eval_dates)), 
                                  total = event_limit,
                                  ncols = tqdm_cols):
    
        if event_limit and index == event_limit:
            break
        
        datapoint, _ = X_Y_state_machine.process_event(event)
        
        if datapoint:
            x, y_true = get_data_vector(datapoint)
            mean_pc = datapoint[0][2]
            std_pc = datapoint[0][3]
            
            y_pred = model.predict(x.reshape(1, -1)) * std_pc + mean_pc
            tgts += np.ndarray.tolist(y_true * std_pc + mean_pc)
            preds += np.ndarray.tolist(y_pred.reshape(y_true.shape))
    
    if len(preds) == 0:
        metrics = [-1, -1]
    else:
        metrics = get_metrics(tgts, preds, display = display)
    
    return metrics

def train_with_early_stop(model, model_name, save_location, consider_tickers, train_dates, eval_dates,
                          max_epochs = 12, train_event_limit = None, eval_event_limit = None, verbose = True):
    
    best_metric = None
    
    for epoch in range(max_epochs):
        tqdm.tqdm.write('Epoch ' + str(epoch + 1) + ':')
        
        tqdm.tqdm.write('Training Step:')
        model = train_epoch(model, state_proc.consider_tickers, train_dates, event_limit = train_event_limit)
        
        tqdm.tqdm.write('Validation Step:')
        metrics = evaluate(model, state_proc.consider_tickers, val_dates, event_limit = eval_event_limit)
        
        if verbose:
            tqdm.tqdm.write('MAE: ' + str(metrics[1]))
            if best_metric:
                tqdm.tqdm.write('Drop in MAE: ' + str(best_metric - metrics[1]) + '\n')
            
        if not best_metric or best_metric > metrics[1]:
            best_metric = metrics[1]
            # Use MAE for early stopping
            joblib.dump(model, save_location + model_name) 
        else:
            if verbose:
                tqdm.tqdm.write('Selecting Best Checkpoint')
            model = joblib.load(save_location + model_name)
            break
    
    return model

def train_test(model, model_name, save_location, consider_tickers, train_dates, eval_dates, test_dates,
               max_epochs = 12, train_event_limit = None, eval_event_limit = None, verbose = True):
    
    model = train_with_early_stop(model, model_name, save_location, consider_tickers, train_dates,
                                  eval_dates, max_epochs = max_epochs, train_event_limit = train_event_limit, 
                                  eval_event_limit = eval_event_limit)
    
    # Model has been saved
    tqdm.tqdm.write('Computing Metrics on the Test Split:')
    metrics = evaluate(model, consider_tickers, test_dates, event_limit = eval_event_limit, display = True)

def get_BBO_data_vector(datapoint, include_timestamps = False, no_target = False):
    # By default, we'll predict the BBOs for ticker 0
    # If no_target, y would be None
    
    if no_target:
        y = None
    else:
        y = np.concatenate([datapoint[0][1][1]] + [datapoint[0][1][2]])
    
    x = []
    
    for ticker_ID in range(len(datapoint)):
        if include_timestamps:
            x.append(datapoint[ticker_ID][0][0])
        x.append(datapoint[ticker_ID][0][1])
        x.append(datapoint[ticker_ID][0][2])
    
    x = np.concatenate(x)
    
    return x, y

def get_ticker_to_index(tickers):
    ticker_to_index = {}
    for index, ticker in enumerate(tickers):
        ticker_to_index[ticker] = index
        
    return ticker_to_index

def check_identity(datapoint):
    BBs = datapoint[0][0][1]
    BAs = datapoint[0][0][2]
    
    return all(map(lambda x: x == BBs[0], BBs)) and all(map(lambda x: x == BAs[0], BAs))

def generate_BBO_pred_dataset(consider_tickers, dates, event_limit = None, filter_identity = True):
    global tqdm_cols
    raw_datapoints = []
    X_vectors = []
    Y_vectors = []

    ticker_to_index = get_ticker_to_index(consider_tickers)

    X_Y_state_machine = state_maintainer(window_len = state_proc.window_len,
                                         predict_size = state_proc.predict_size,
                                         ticker_to_index = ticker_to_index,
                                         batch_size = state_proc.batch_size,
                                         shift = state_proc.shift,
                                         event_type = 1, 
                                         batching = False)
    
    for index, event in tqdm.tqdm(enumerate(state_proc.book_update_event_generator(consider_tickers, 
                                                                                   dates)), 
                                            total = event_limit, 
                                            ncols = tqdm_cols):

        if event_limit and index == event_limit:
            break

        datapoint, _ = X_Y_state_machine.process_event(event)
        
        if datapoint:
            if filter_identity:
                if check_identity(datapoint):
                    continue
            raw_datapoints.append(datapoint)
            X_vector, Y_vector = get_BBO_data_vector(datapoint)
            X_vectors.append(X_vector)
            Y_vectors.append(Y_vector)
            
    return raw_datapoints, X_vectors, Y_vectors


def train_model(model, X_vectors, Y_vectors):
    model.fit(X_vectors, Y_vectors)
    return model

def eval_model(model, X_vectors, Y_vectors):
    y_preds = model.predict(X_vectors).reshape(-1)
    y_true = np.asarray(Y_vectors).reshape(-1)
    return get_metrics(y_true, y_preds)

def save_dataset(tickers, dates, save_location, dataset_name, book_update_dates, event_limit = None):
    
    datapoints, X_vectors, Y_vectors = generate_BBO_pred_dataset(state_proc.consider_tickers, 
                                                                 book_update_dates, 
                                                                 event_limit = event_limit)
    
    with open(save_location + dataset_name + '_raw.pkl', 'wb') as f:
        pickle.dump(datapoints, f)
        
    with open(save_location + dataset_name + '_X.pkl', 'wb') as f:
        pickle.dump(X_vectors, f)
        
    with open(save_location + dataset_name + '_Y.pkl', 'wb') as f:
        pickle.dump(Y_vectors, f)
    
    return datapoints, X_vectors, Y_vectors



def visualize_BBO_pred(model_name, ticker_name, datestring, time_stamps, 
                       true_BBs, true_BAs, pred_BBs, pred_BAs, 
                       pred_BB_stds, pred_BA_stds, ignore_first = 0, radius_scale = 2):
    
    time_stamps = time_stamps[(ignore_first + 1):]
    true_BBs = true_BBs[(ignore_first + 1):]
    true_BAs = true_BAs[(ignore_first + 1):]
    pred_BBs = pred_BBs[(ignore_first + 1):]
    pred_BAs = pred_BAs[(ignore_first + 1):]
    pred_BB_stds = pred_BB_stds[(ignore_first + 1):]
    pred_BA_stds = pred_BA_stds[(ignore_first + 1):]
    

    plt.rcParams['figure.dpi'] = proc.display_dpi
    base_time = proc.get_timestamp(time_stamps[0])
    time_diffs = []
    n = len(time_stamps)
    
    for time in time_stamps:
        time_code = proc.convert_to_seconds(proc.get_timestamp(time) - base_time)
        time_diffs.append(time_code)
        
    title_string = 'BBO prediction against time elapsed, Ticker: ' + ticker_name 
    title_string += ', Date: ' + datestring + ', Model: ' + model_name
    
    fig = plt.figure(figsize = (12, 6))
    subplot = fig.add_subplot(111)
    
    subplot.set_title(title_string, fontsize = 14)
    subplot.set_xlabel('Time elapsed (in seconds)', fontsize = 12)
    
    subplot.set_ylabel('Price', fontsize = 12)
    
    for index in range(n):
        if pred_BB_stds[index] is None:
            continue
        
        if pred_BB_stds[index] == 1:
            subplot.plot(time_diffs[index],
                     pred_BBs[index],
                     marker = 'X',
                     alpha = 0.2,
                     color = 'blue',
                     markersize = radius_scale)
        else:
            subplot.plot(time_diffs[index],
                         pred_BBs[index],
                         marker = 'o',
                         alpha = 0.2,
                         color = 'blue',
                         markersize = 2 * pred_BB_stds[index] * radius_scale)
    
        if pred_BA_stds[index] == 1:
            subplot.plot(time_diffs[index],
                         pred_BAs[index],
                         marker = 'X',
                         alpha = 0.05,
                         color = 'green',
                         markersize = radius_scale)
            
        else:
            subplot.plot(time_diffs[index],
                         pred_BAs[index],
                         marker = 'o',
                         alpha = 0.05,
                         color = 'green',
                         markersize = 2 * pred_BA_stds[index] * radius_scale)
    
    plt.plot(time_diffs, pred_BBs, color = 'blue', alpha = 0.25, linewidth = 0.25)
    plt.plot(time_diffs, pred_BAs, color = 'green', alpha = 0.25, linewidth = 0.25)
    
    plt.plot(time_diffs, true_BBs, color = 'black', alpha = 0.5, linewidth = 1)
    plt.plot(time_diffs, true_BAs, color = 'red', alpha = 0.5, linewidth = 1)
    
    label_A = 'Predicted BB'
    label_B = 'Predicted BA'
    label_C = 'True BB'
    label_D = 'True BA'
    
    blue_patch = mpatches.Patch(color = 'blue', label = label_A)
    green_patch = mpatches.Patch(color = 'green', label = label_B)
    black_patch = mpatches.Patch(color = 'black', label = label_C)
    red_patch = mpatches.Patch(color = 'red', label = label_D)
    
    plt.legend(handles = [blue_patch, green_patch, black_patch, red_patch])
    plt.savefig(f'/Users/blaineh2/Desktop/FIN 556/group_03_project/kf_svi_pred/{ignore_first}.svg', format='svg')
    plt.show()
