import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from scipy.interpolate import interp1d
import datetime
import dateutil



def plotPosition():
    df = pd.read_csv("data/fills.csv")

    symbolSet = set(df.Symbol.unique())
    symbolData = dict.fromkeys(symbolSet, [[],0,0,[]]) # time, poslogs, position cnt, position
    numPerBucket = len(df) // 250  # number of fill updates before we log our position

    for index, row in df.iterrows():
        sym = row['Symbol']
        qty = row['Quantity']
        symbolData[sym][1]+=1
        symbolData[sym][2] += qty

        if (symbolData[sym][1] == numPerBucket):
            symbolData[sym][0].append(symbolData[sym][2])
            symbolData[sym][3].append(dateutil.parser.parse(row['TradeTime']))
            symbolData[sym][1] = 0



    if (len(symbolSet) > 6):
        print("We do not support more than 6 symbols for graphing")
        exit()

    symbolSet = list(symbolSet)
    numPlots = min(6,len(symbolSet))
    fig, axs = plt.subplots(numPlots,figsize=(20, 20))
    fig.autofmt_xdate()
    fig.suptitle('Position Tracking')

    xfmt = mdates.DateFormatter('%H:%M:%S')

    if ( numPlots > 1):
        for i,ax in enumerate(axs):
            sym = symbolSet[i]
            ax.set(xlabel='timestamp', ylabel='Position')
            ax.set_title(f'Daily Position for {sym}')
            ax.plot(symbolData[sym][3], symbolData[sym][0])
            ax.set_xticklabels(ax.get_xticks(), rotation=45)
            ax.xaxis.set_major_locator(mdates.MinuteLocator(interval=10))
            ax.xaxis.set_major_formatter( xfmt )
    else:
        sym = symbolSet[0]
        axs.set(xlabel='timestamp', ylabel='Position')
        axs.set_title(f'Daily Position for {sym}')
        axs.plot(symbolData[sym][3], symbolData[sym][0])
        axs.set_xticklabels(axs.get_xticks(), rotation=45)
        axs.xaxis.set_major_locator(mdates.MinuteLocator(interval=10))
        axs.xaxis.set_major_formatter( xfmt )

    plt.show()
    
    
def plotPositions(file_names=["data/fills1.csv", "data/fills2.csv", "data/fills3.csv", "data/fills4.csv", "data/fills5.csv", "data/fills6.csv", "data/fills7.csv", "data/fills8.csv"], titles=["position1", "position2", "position3", "position4", "position5", "position6", "position7", "position8"]):
    num_files = len(file_names)
    num_plots = min(8, num_files)
    rows = 4
    cols = 2

    fig, axs = plt.subplots(rows, cols, figsize=(15, 15))
    fig.autofmt_xdate()
    fig.suptitle('Position Tracking')

    xfmt = mdates.DateFormatter('%H:%M:%S')

    for i in range(num_plots):
        row = i // cols
        col = i % cols
        ax = axs[row, col] if num_plots > 1 else axs  # Determine the current subplot

        df = pd.read_csv(file_names[i])

        symbolSet = set(df.Symbol.unique())
        symbolData = dict.fromkeys(symbolSet, [[], 0, 0, []])  # time, poslogs, position cnt, position
        numPerBucket = len(df) // 250  # number of fill updates before we log our position

        for index, row in df.iterrows():
            sym = row['Symbol']
            qty = row['Quantity']
            symbolData[sym][1] += 1
            symbolData[sym][2] += qty

            if symbolData[sym][1] == numPerBucket:
                symbolData[sym][0].append(symbolData[sym][2])
                symbolData[sym][3].append(dateutil.parser.parse(row['TradeTime']))
                symbolData[sym][1] = 0

        for sym in symbolSet:
            ax.set(xlabel='timestamp', ylabel='Position')
            ax.set_title(f'Daily Position for {sym}')
            ax.plot(symbolData[sym][3], symbolData[sym][0])
            ax.set_xticklabels(ax.get_xticks(), rotation=45)
            ax.xaxis.set_major_locator(mdates.MinuteLocator(interval=10))
            ax.xaxis.set_major_formatter(xfmt)

    plt.tight_layout()
    plt.show()
