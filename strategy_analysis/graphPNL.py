import matplotlib.pyplot as plt
import csv

def graphPNL(aFileName = "data/pnl.csv", title="PNL"):
      myInstances = {}
      with open(aFileName, 'r') as file:
        csv_reader = csv.reader(file)

        header = next(csv_reader)

        for row in csv_reader:
          if (row[0] not in myInstances):
            myInstances[row[0]] = []

          myInstances[row[0]].append(float(row[2]))


      for myInstanceName, myPnls in myInstances.items():

        plt.figure(figsize=(20,20))
        plt.plot(myPnls)

        plt.title(title)
        plt.xlabel("Time Increasing")
        plt.xticks([])
        plt.ylabel("PNL")
        plt.grid(True)

        plt.show()

def graphPNLs(file_names=["data/pnl1.csv", "data/pnl2.csv", "data/pnl3.csv", "data/pnl4.csv", "data/pnl5.csv", "data/pnl6.csv", "data/pnl7.csv", "data/pnl8.csv"], titles=["PNL1", "PNL2", "PNL3", "PNL4", "PNL5", "PNL6", "PNL7", "PNL8"]):
    num_plots = len(file_names)
    rows = 4
    cols = 2

    fig, axes = plt.subplots(rows, cols, figsize=(15, 15))

    for i in range(num_plots):
        ax = axes[i // cols, i % cols]  # Calculate the subplot index
        myInstances = {}

        with open(file_names[i], 'r') as file:
            csv_reader = csv.reader(file)
            header = next(csv_reader)

            for row in csv_reader:
                if row[0] not in myInstances:
                    myInstances[row[0]] = []

                myInstances[row[0]].append(float(row[2]))

        for myInstanceName, myPnls in myInstances.items():
            ax.plot(myPnls)
            
        # Labeling the final PnL on the graph
        final_pnl = myPnls[-1]
        ax.text(0.95, 0.9, f'Final PnL: {final_pnl:.2f}', transform=ax.transAxes,
                horizontalalignment='right', verticalalignment='top', bbox=dict(facecolor='white', alpha=0.7))


        ax.set_title(titles[i])
        ax.set_xlabel("Time Increasing")
        ax.set_xticks([])
        ax.set_ylabel("PNL")
        ax.grid(True)

    plt.tight_layout()
    
    plt.savefig("PnL.svg")
    plt.show()
