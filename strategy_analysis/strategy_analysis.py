'''
    usage: python3 strategy_analysis.py <filename1> <filename2> ... <filenameN>
'''
import sys
import numpy as np 
import pandas as pd
import matplotlib.pyplot as plt

def print_matrices(cumulative_pnl_value, initial_value):
    pnl = cumulative_pnl_value[1:]
    prev_pnl = cumulative_pnl_value[:-1]
    returns = (pnl - prev_pnl) / (prev_pnl + initial_value)

    max_pnl = np.max(cumulative_pnl_value)
    print(f"Maximum Profit and Loss:\t {max_pnl}")
    min_pnl = np.min(cumulative_pnl_value)
    print(f"Minimum Profit and Loss:\t {min_pnl}")

    starting_net = df['Cumulative PnL'].iloc[0]
    ending_net = df['Cumulative PnL'].iloc[-1]
    print(f"Net Profit and Loss:\t {ending_net}")

    diff = ending_net - starting_net
    cumulative_return = diff/ initial_value
    print(f"Cumulative Return:\t {round( cumulative_return * 100, 4)}%")

    volatility = np.std(returns)
    print(f"Volatility:\t {round(volatility, 4)}")

    sharpe_ratio = np.average(returns)/ volatility
    print(f"Sharpe Ratio:\t {round(sharpe_ratio, 4)}")

    max_drawdown = (max_pnl-min_pnl)/(max_pnl+initial_value)
    print(f"Max Drawdown:\t {round( max_drawdown,4 )}")

    win_rate = np.sum(pnl - prev_pnl > 0)/(len(cumulative_pnl_value) - 1 )
    print(f"Win Rate:\t {round( win_rate * 100,4 )}%")


def strategies_analysis(files: [str], initial_value=1000000):
    """
    files: a list of csv filename. Columns includes Stragtegy name, time, and cumulative PnL.
    initial_value: initial_value when starting runing the strategy
    """
    fig = plt.figure(figsize= (15,10))
    color_map = ["green", "blue", "orange", "purple"]

    for i, file in enumerate(files):
        df = pd.read_csv(file)
        cumulative_pnl_value = df['Cumulative PnL'].to_numpy()
        time = df['Time'].to_numpy()
        cumulative_pnl_percent= cumulative_pnl_value /initial_value *100
        plt.plot(time, cumulative_pnl_percent, color=color_map[i])

        print(i, file)
        print_matrices(cumulative_pnl_value, initial_value)


    plt.title("Cumulative PnL")
    plt.xlabel('Time')
    plt.ylabel('Cumulative PnL')
    plt.savefig('fig/cumulative_pnl.jpg',bbox_inches='tight', dpi=150)
    plt.show()

if __name__ == '__main__':
    if len(sys.argv) != 2:
        print("Usage: python3 strategy_analysis.py <filename1> <filename2> ... <filenameN>")
    else:
        filenames = sys.argv[1:]

    strategies_analysis(filenames)