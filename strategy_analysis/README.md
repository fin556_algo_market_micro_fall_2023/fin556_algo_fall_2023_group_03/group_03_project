# Strategy Visualization
After Strategy Studio finished backtesting, it outputs three files, which are fills, orders, and PnL(Profit and Loss), and we are going to focus mainly on the PnL csv file, which has three columns: Strategy Name, Time and Cumulative PnL.

## Metrics

- Max PnL
- Min PnL
- Net PnL = last cumulative PnL
- Returns:
    - cumulative_return = PnL/initial_value
    - return = (PnL-prev_PnL)/(initial_value+prev_PnL)

- Volatility = std(returns)
- Sharpe ratio = avg(returns) / Volatility
- Max drawdown = (max_PnL - min_PnL)/(max_PnL+initial_value)
- Win rate = #(PnL-prev_PnL > 0) / #trades

## Visualization

- Time/date vs. cumulative PnL
- For each date,  PnL vs latency up to 10^2 and up to 10^-1
- For each date, win rate vs latency
- Plot the mean & std dev for each latency 
- For each latency, each date's PnL for each strategy
