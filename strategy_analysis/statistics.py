import os
import pandas as pd
import numpy as np
from collections import defaultdict
import matplotlib.pyplot as plt
import re
from datetime import datetime

def extract_date_and_latency(filename):
    match = re.search(r'(\d{2}-\d{2}-\d{4})_(\d+(\.\d+)?)_', filename)
    if match:
        return match.group(1), float(match.group(2))
    return None, None

def plot_date_vs_statistics_fix_latency(directories, column, latency=0.0001, dpi=300, figure_size=(10, 6), save_path=None):
    plt.figure(figsize=figure_size, dpi=dpi)
    strategies = []

    for directory in directories:
        statistics_data = defaultdict(list)
        strategy = directory.split("/")[-2].split("_")[0]

        for file in os.listdir(directory):
            if file.endswith(".csv"):
                file_prefix = file.rstrip(".csv")
                if not file_prefix.endswith("_pnl"):
                    continue
                date, datalatency = extract_date_and_latency(file_prefix)

                if date and datalatency == latency:
                    file_prefix = os.path.join(directory, file_prefix)
                    statistics = calculate_matrix(file_prefix)[column]
                    statistics_data[date].append(statistics)

        # Convert date strings to datetime objects for sorting
        dates = [datetime.strptime(date, "%m-%d-%Y") for date in statistics_data.keys()]
        print(dates)
        dates, statistics_values = zip(*sorted(zip(dates, statistics_data.values())))
        
        # Plotting
        dates, statistics_values = zip(*sorted(statistics_data.items(), key=lambda x: x[0]))
        plt.plot(dates, statistics_values, marker='o', label=f'Directory: {strategy}')

    plt.xlabel('Date')
    plt.ylabel(column)
    plt.title(f'Date vs {column} - Latency: {latency}')
    plt.legend()

        # Plotting as a bar plot
    #     plt.bar([date.strftime("%m-%d-%Y") for date in dates], [sum(values) / len(values) for values in statistics_values], label=f'Directory: {strategy}')
        
    #     strategies.append(strategy)
        
    # plt.xlabel('Date')
    # plt.ylabel(column)
    # plt.title(f'Date vs {column} - Latency: {latency}')
    # plt.legend()

    # Save the plot if save_path is provided
    if save_path:
        plt.savefig(os.path.join(save_path, f'{strategies[0]}_date_vs_{column}_latency_{latency}.svg'))

    plt.show()

def plot_date_vs_pnl(directories, dpi=300, figure_size=(10, 6), save_path=None):
    plt.figure(figsize=figure_size, dpi=dpi)

    for directory in directories:
        pnl_data = defaultdict(list)

        for file in os.listdir(directory):
            if file.endswith("_pnl.csv"):
                file_prefix = file.rstrip("_pnl.csv")
                date, _ = extract_date_and_latency(file_prefix)

                if date:
                    pnl = pd.read_csv(os.path.join(directory, file))
                    final_pnl = pnl.iloc[-1]['Cumulative PnL']
                    pnl_data[date].append(final_pnl)

        # Plotting
        dates, pnl_values = zip(*sorted(pnl_data.items(), key=lambda x: x[0]))
        plt.plot(dates, pnl_values, marker='o', label=f'Directory: {directory}')

    plt.xlabel('Date')
    plt.ylabel('Final PnL')
    plt.title('Date vs Final PnL')
    plt.legend()

    # Save the plot if save_path is provided
    if save_path:
        plt.savefig(os.path.join(save_path, 'date_vs_final_pnl.svg'))

    plt.show()

def plot_latency_vs_statistics(directory, column, lowerbound=0, upbound=100, dpi=300, figure_size=(10, 6), save_path=None, log=True):
    latency_data = defaultdict(list)

    for file in os.listdir(directory):
        if file.endswith(".csv"):
            file_prefix = file.rstrip(".csv")
            file_prefix = os.path.join(directory, file_prefix)

            date, latency = extract_date_and_latency(file_prefix)
            if date and latency is not None:
                if latency > upbound or latency < lowerbound: 
                    continue
                file_prefix = "_".join(file_prefix.split("_")[:-1])
                statistics = calculate_matrix(file_prefix)[column]
                latency_data[date].append((latency, statistics))

    # Plotting in a 4-row * 2-column grid
    num_dates = len(latency_data)
    rows = 4
    cols = 2
    num_plots = min(num_dates, rows * cols)

    fig, axs = plt.subplots(rows, cols, figsize=(15, 15))

    for i, (date, data) in enumerate(latency_data.items()):
        if i >= num_plots:
            break

        data.sort(key=lambda x: x[0])
        latency, statistics = zip(*data)

        row = i // cols
        col = i % cols

        axs[row, col].plot(latency, statistics, marker='o', linestyle='-', label=f'Date: {date}')
        if log:
            axs[row, col].set_xscale('log')  # Set x-axis to logarithmic scale
            axs[row, col].set_xlabel('Log(Latency)')
        else:
            axs[row, col].set_xlabel('Latency')
        axs[row, col].set_ylabel(column)
        axs[row, col].set_title(f'Latency vs {column} - Date: {date}')
        axs[row, col].legend()


    plt.tight_layout()
        # Save the plot if save_path is provided
    if save_path:
        plt.savefig(os.path.join(save_path, f'latency_vs_{column}.svg'))

    plt.show()
# def plot_latency_vs_statistics(directory, column, upbound=100, dpi=300, figure_size=(10, 6), save_path=None):
#     latency_data = defaultdict(list)

#     for file in os.listdir(directory):
#         if file.endswith(".csv"):
#             file_prefix = file.rstrip(".csv")
#             file_prefix = os.path.join(directory, file_prefix)

#             date, latency = extract_date_and_latency(file_prefix)
#             if date and latency is not None:
#                 if latency > upbound: 
#                     continue
#                 file_prefix = "_".join(file_prefix.split("_")[:-1])
#                 statistics = calculate_matrix(file_prefix)[column]
#                 latency_data[date].append((latency, statistics))

#     # Plotting
#     for date, data in latency_data.items():
#         data.sort(key=lambda x: x[0])
#         latency, statistics = zip(*data)

#         plt.figure(figsize=figure_size, dpi=dpi)
#         plt.plot(latency, statistics, marker='o', linestyle='-', label=f'Date: {date}')
#         plt.xscale('log')  # Set x-axis to logarithmic scale
#         plt.xlabel('Log(Latency)')
#         plt.ylabel(column)
#         plt.title(f'Latency vs {column} - Date: {date}')
#         plt.legend()

#         # Save the plot if save_path is provided
#         if save_path:
#             plt.savefig(os.path.join(save_path, f'latency_vs_{column}_{date}.svg'))

#         plt.show()



def get_latency_column(directory, column):
    latency_column = defaultdict(list)
    visited = set()
    
    for file in os.listdir(directory):
        if file.endswith(".csv"):
            try:
                file_prefix = file.rstrip(".csv")
                file_prefix = file_prefix.split("_")
                latency = file_prefix[-2]
                file_prefix = "_".join(file_prefix[:-1])
                
                if file_prefix in visited:
                    continue
                visited.add(file_prefix)
                
                file_prefix = os.path.join(directory, file_prefix)
                statistics = calculate_matrix(file_prefix)
                
                latency_column[latency].append(statistics[column])
            except Exception as e:
                print("Error processing file", file, e)
    
    # Convert defaultdict to a list of tuples
    latency_column = sorted(latency_column.items(), key=lambda x: x[0])

    latencies, column_data = zip(*latency_column)  # Unpack the tuples into separate lists
    column_data = np.array(column_data, dtype=float)

    return latencies, column_data


def plot_latency_column(directory, column, upbound=100, save_path=None):
    directory = directory.rstrip("/")
    strategy = directory.split("/")[-1].split("_")[0]
    title = strategy + ":" + column + " vs Latency"
    latency, column_data = get_latency_column(directory, column)
    
    # Convert column_data to a numpy array
    column_data = np.array(column_data, dtype=float)

    # Calculate statistics
    mean_value = np.mean(column_data, axis=1)
    std_dev = np.std(column_data, axis=1)
    max_value = np.max(column_data, axis=1)
    min_value = np.min(column_data, axis=1)

    # Plotting
    plt.figure(figsize=(10, 6))
    plt.plot(latency, mean_value, label='Mean')
    plt.fill_between(latency, mean_value - std_dev, mean_value + std_dev, alpha=0.2, label='Standard Deviation')
    plt.plot(latency, max_value, label='Max')
    plt.plot(latency, min_value, label='Min')

    plt.xlabel("Latency")
    plt.ylabel(column)
    plt.title(title)
    plt.legend()
    
    # Save the plot if save_path is provided
    if save_path:
        plt.savefig(os.path.join(save_path, f'latency_vs_{column} - {strategy}.svg'))
    
    plt.show()
    
    



def calculate_matrix(file_prefix):
    # orders = pd.read_csv(file_prefix + "_order.csv")
    # fill = pd.read_csv(file_prefix + "_fill.csv")
    pnl = pd.read_csv(file_prefix + "_pnl.csv")
    # pnl = pd.read_csv(file_prefix + ".csv")    
    # Calculate the total transaction cost
    # total_transaction_cost_fill = fill['ExecutionCost'].sum()
    # total_transaction_cost_order = orders['ExecutionCost'].sum()
    # total_transaction_cost = total_transaction_cost_fill + total_transaction_cost_order

    # Calculate periodic returns by taking the difference of cumulative PnL
    pnl['PnL Returns'] = pnl['Cumulative PnL'].diff()

    # Calculate Sharpe Ratio 
    sharpe_ratio = (pnl['PnL Returns'].mean() / pnl['PnL Returns'].std()) * np.sqrt(pnl['PnL Returns'].count())

    # Calculate Maximum Drawdown
    rolling_max = pnl['Cumulative PnL'].cummax()
    daily_drawdown = pnl['Cumulative PnL'] - rolling_max
    max_drawdown = daily_drawdown.min()

    # Calculate Win Rate
    total = pnl['PnL Returns'].count()
    win_sum = (pnl['PnL Returns'] >= 0).sum()
    win_rate = win_sum / total * 100 if total > 0 else 0
    # print(file_prefix, "has", total, "trades", "with win rate", win_rate, "%")
    if total == 0:
        print(file_prefix, "has no trades")

    # Calculate Profit Factor
    total_loss =  abs(pnl[pnl['PnL Returns'] < 0]['PnL Returns'].sum())
    total_win = pnl[pnl['PnL Returns'] > 0]['PnL Returns'].sum()
    profit_factor = total_win / total_loss if total_loss > 0 else 0
    if total_loss == 0:
        # print(file_prefix, "has no losses")
        if total_win > 0:
            profit_factor = 1
    
    # Extract the final PnL from the last row
    final_pnl = float(pnl.iloc[-1]['Cumulative PnL'])

    # put all the statistics into a dictionary
    statistics = {
        # "Total Transaction Cost": total_transaction_cost,
        "Sharpe Ratio": sharpe_ratio,
        "Maximum Drawdown": max_drawdown,
        "Win Rate": win_rate,
        "Profit Factor": profit_factor,
        "Final PnL": final_pnl,
    }
    
    return statistics

# Example usage:
# plot_latency_column('/path/to/directory', 'Sharpe Ratio')
