import os
import pandas as pd
from datetime import date
import tqdm
tqdm_cols = 100

def ET_nanos_from_midnight_to_UTC_time(date_obj, timestamp_string):
    nanos = int(timestamp_string)
    
    eastern_datetime = pd.to_datetime(date_obj) + pd.to_timedelta(nanos, unit = 'ns')
    eastern_datetime = eastern_datetime.tz_localize('US/Eastern')
    
    utc_datetime = eastern_datetime.tz_convert('UTC')
    
    return utc_datetime

def format_datetime(utc_datetime):
    nanos_only = utc_datetime.nanosecond
    utc_datetime = utc_datetime.strftime('%Y-%m-%d %H:%M:%S.%f') + '{:03d}'.format(nanos_only)
    
    return utc_datetime

def generate_csv_lines(location, data_file_name, skip = True):
    with open(location + data_file_name, 'r+') as f:
        if skip:
            _ = f.readline()
        
        while True:
            line = f.readline().strip()
            if not line:
                return None
            yield line
            
    return None
            
def book_update_line_to_info(line):
    line = line.split(',')
    timestamp = line[0]
    
    bid_price_vol = {}
    ask_price_vol = {}
    
    offset = 1
    
    for index in range(0, 5):
        start = offset + index * 4
        
        if line[start].strip() != '':
            bid_price_vol[float(line[start])] = int(line[start + 1])
        
        if line[start + 2].strip() != '':
            ask_price_vol[float(line[start + 2])] = int(line[start + 3])
        
    return [timestamp, bid_price_vol, ask_price_vol]

valid_message_types = set(['E', 'C', 'P', 'Q'])

def msg_line_to_info(line):
    global valid_message_types
    
    line = line.split(',')
    
    m_type = line[1]
    
    if m_type not in valid_message_types:
        return None
    
    timestamp = line[0]
    ID = line[2]
    side = line[3]
    size = line[4]
    price = line[5]
    canc_size = line[6]
    exec_size = line[7]
    
    return [timestamp, price, exec_size, m_type]
    
def datestring_to_date_obj(datestring):
    mon = int(datestring[0 : 2])
    day = int(datestring[2 : 4])
    year = int(datestring[4 :])
    
    return date(year, mon, day)

def book_updates_to_P_ticks(location, data_file_name, proc_limit = None):
    
    # Code to convert NASDAQ book updates and trades into
    # 'P' and 'T' type ticks required by Strategy Studio

    # Note that this process is lossy and not recommended in general
    # Since NASDAQ provides market by order messages, it is sensible to convert
    # that into 'D' and 'T' type Strategy Studio ticks

    # Market by order to book updates to price level updates (SS type 'P')
    # would lose price levels beyond the depth of the parsed book updates

    # However, we have this conversion setup as a fall back option
    # In case, we run into unforseen issues while trying to rely on
    # two different order types within Strategy Studio
    
    datestring = data_file_name[0 : 8]
    date_obj = datestring_to_date_obj(datestring)
    
    curr_bid_price_vol = {}
    curr_ask_price_vol = {}
    
    all_events = []
        
    for index, line in tqdm.tqdm(enumerate(generate_csv_lines(location, data_file_name)), 
                                  total = proc_limit,
                                  ncols = tqdm_cols):
        if not line or index == proc_limit:
            break
            
        t, bpv, apv = book_update_line_to_info(line)
        events = []
        
        for price in curr_bid_price_vol:
            if price not in bpv:
                events.append(['B', price, 0])
            
            elif price in bpv and bpv[price] == curr_bid_price_vol[price]:
                continue
            
            else:
                events.append(['B', price, bpv[price]])
                
        for price in bpv:
            if price not in curr_bid_price_vol:
                events.append(['B', price, bpv[price]])
        
                
        for price in curr_ask_price_vol:
            if price not in apv:
                events.append(['A', price, 0])
            
            elif price in apv and apv[price] == curr_ask_price_vol[price]:
                continue
            
            else:
                events.append(['A', price, apv[price]])
                
        for price in apv:
            if price not in curr_ask_price_vol:
                events.append(['A', price, apv[price]])
        
        curr_bid_price_vol = bpv
        curr_ask_price_vol = apv
        
        UTC_timestamp = ET_nanos_from_midnight_to_UTC_time(date_obj, t.strip())
        
        for event in events:
            all_events.append([UTC_timestamp] + event)
        
    return all_events

def message_to_T_ticks(location, data_file_name, proc_limit = None):
    
    datestring = data_file_name[0 : 8]
    date_obj = datestring_to_date_obj(datestring)
    
    events = []
    
    for index, line in tqdm.tqdm(enumerate(generate_csv_lines(location, data_file_name)), 
                                  total = proc_limit,
                                  ncols = tqdm_cols):
        
        if not line or index == proc_limit:
            break
            
        tick = msg_line_to_info(line)
        if not tick:
            continue
            
        UTC_timestamp = ET_nanos_from_midnight_to_UTC_time(date_obj, tick[0].strip())
        events.append([UTC_timestamp] + tick[1 :])
        
    return events
    
def get_data_files(data_dir):
    data_files = set([filename for filename in os.listdir(data_dir) if os.path.isfile(data_dir + filename)])
    book_update_files = []
    
    for filename in data_files:
        if filename.endswith('.csv'):
            book_update_files.append(filename)
    
    return book_update_files

def get_dates(book_update_files):
    valid_dates = []
    for filename in book_update_files:
        valid_dates.append(filename[0 : 8])
    
    return valid_dates

def merge_events(A, B):
    # A and B are two sets of events
    l_A, l_B = len(A), len(B)
    p_A, p_B = 0, 0
    merged = []
    
    while p_A < l_A and p_B < l_B:
        if A[p_A][0] < B[p_B][0]:
            merged.append(A[p_A])
            p_A += 1
        else:
            merged.append(B[p_B])
            p_B += 1
            
    if p_B < l_B and p_A == l_A:
        p_A = p_B
        l_A = l_B
        A = B
        
    while p_A < l_A:
        merged.append(A[p_A])
        p_A += 1
        
    return merged

def event_to_text_ticks(events, location, tickfile_name):
    
    with open(location + tickfile_name, 'w+') as f:
        f.write('')
    
    with open(location + tickfile_name, 'a+') as f:
        
        for index, event in tqdm.tqdm(enumerate(events), 
                                      total = len(events),
                                      ncols = tqdm_cols):
            
            timestamp = format_datetime(event[0])
            
            if event[1] == 'A' or event[1] == 'B':
                # Tick of Type 'P'
                
                items = ['' for _ in range(12)]
                
                items[0] = timestamp
                items[1] = timestamp
                items[2] = str(index + 1)
                items[3] = 'P'
                items[4] = 'NASDAQ'
                
                items[6] = str(event[2])
                items[7] = str(event[3])
                
                
                if event[1] == 'A':
                    items[5] = '2'
                elif event[1] == 'B':
                    items[5] = '1'
                    
            else:
                # Tick of Type 'T'
                
                items = ['' for _ in range(11)]
                
                items[0] = timestamp
                items[1] = timestamp
                items[2] = str(index + 1)
                items[3] = 'T'
                items[4] = 'NASDAQ'
                items[5] = str(event[1])
                items[6] = str(event[2])
                    
            tick_string = ','.join(items)
            f.write(tick_string + '\n')
            
    return

def create_tick_data(datestring, OB_location, msg_location, save_location):
    
    tqdm.tqdm.write('Processing date: ' + datestring + '\n')
    
    order_book_file_name = datestring + '.NASDAQ_ITCH50_SPY_book_5.csv'
    tqdm.tqdm.write('- Generating P type ticks:')
    P_events = book_updates_to_P_ticks(OB_location, order_book_file_name)
    
    message_file_name = datestring + '.NASDAQ_ITCH50_SPY_message.csv'
    tqdm.tqdm.write('- Generating T type ticks:')
    T_events = message_to_T_ticks(msg_location, message_file_name)
    
    tqdm.tqdm.write('- Merging events and creating tick file:')
    merged_events = merge_events(P_events, T_events)
    event_to_text_ticks(merged_events, save_location, datestring + '_SPY_ticks.txt')
    
    tqdm.tqdm.write('\n')
    
    return

def get_NASDAQ_data_files(data_dir):
    data_files = set([filename for filename in os.listdir(data_dir) if os.path.isfile(data_dir + filename)])
    tick_files = []
    
    for filename in data_files:
        if filename.endswith('.txt'):
            tick_files.append(filename)
    
    return tick_files

def order_n_file_date(n_datestring):
    # Order NASDAQ files' datestring in the year month date format
    mon = n_datestring[0 : 2]
    day = n_datestring[2 : 4]
    year = n_datestring[4 :]
    
    return year + mon + day

def get_dates(nasdaq_files):
    valid_dates = []
    for filename in nasdaq_files:
        valid_dates.append(order_n_file_date(filename[0 : 8]))
    
    return valid_dates

def ymd_date_to_filenames(datestring):
    year = datestring[0 : 4]
    mon = datestring[4 : 6]
    day = datestring[6 :]
    
    IEX_file = 'tick_SPY_' + datestring + '.txt'
    NASDAQ_file = mon + day + year + '_SPY_ticks.txt'
    
    return IEX_file, NASDAQ_file

def get_file_pairs(valid_dates):
    file_pairs = []
    for datestring in valid_dates:
        file_pairs.append(ymd_date_to_filenames(datestring))
        
    return file_pairs

def generate_tick_lines(location, data_file_name):
    with open(location + data_file_name, 'r+') as f:
        while True:
            line = f.readline().strip()
            if not line:
                return None
            yield line
            
    return None

def get_timestamp(tickline):
    units = tickline.split(',')
    return units[1] # Consider the source time

def generate_timed_ticklist(location, data_file_name, proc_limit = None):
    
    ticklist = [] # Format: (timestamp string, tick line)
    
    for index, line in tqdm.tqdm(enumerate(generate_tick_lines(location, data_file_name)), 
                                  total = proc_limit,
                                  ncols = tqdm_cols):
        
        if not line or index == proc_limit:
            break
            
        ticklist.append((get_timestamp(line), line))
        
    ticklist = sorted(ticklist)
    return ticklist

def merge_timed_ticks(A, B):
    # A and B are two sets of events
    l_A, l_B = len(A), len(B)
    p_A, p_B = 0, 0
    merged_ticks = []
    
    while p_A < l_A and p_B < l_B:
        if A[p_A][0] < B[p_B][0]:
            merged_ticks.append(A[p_A][1])
            p_A += 1
        else:
            merged_ticks.append(B[p_B][1])
            p_B += 1
            
    if p_A == l_A:
        while p_B < l_B:
            merged_ticks.append(B[p_B][1])
            p_B += 1
        
    if p_B == l_B:
        while p_A < l_A:
            merged_ticks.append(A[p_A][1])
            p_A += 1
        
    return merged_ticks


def write_ticks(merged_tick_lines, location, tickfile_name):
    
    with open(location + tickfile_name, 'w+') as f:
        f.write('')
    
    with open(location + tickfile_name, 'a+') as f:
        for line in merged_tick_lines:
            f.write(line + '\n')
            
    return

# To Run:

# OB_location = './NASDAQ_book/'
# msg_location = './NASDAQ_msgs/'
# save_location = './tick_files/'

# valid_dates = get_dates(get_data_files(OB_location))

# for date_string in valid_dates:
#     create_tick_data(date_string, OB_location, msg_location, save_location)
